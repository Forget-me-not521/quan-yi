FROM qy-dev-harbor.myquanyi.com/tools/tomcat-8.5.30-jre8-alpine:latest
ARG PACKAGE_NAME
RUN  mkdir /usr/local/apps \
     && wget http://qy-digital-pkg.myquanyi.com/base/skywalking-agent.tar.gz -O /tmp/skywalking-agent.tar.gz \
     && tar -xvzf /tmp/skywalking-agent.tar.gz -C /usr/local/apps/
COPY  ./target/${PACKAGE_NAME} /tmp/${PACKAGE_NAME}
RUN  tar -xvzf /tmp/${PACKAGE_NAME} -C /usr/local/apps
COPY  ./target/${PACKAGE_NAME} /usr/local/apps/${PACKAGE_NAME}

COPY  docker-entrypoint.sh /
RUN  chmod u+x docker-entrypoint.sh
