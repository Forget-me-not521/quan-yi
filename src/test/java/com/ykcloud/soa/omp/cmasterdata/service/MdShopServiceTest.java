//package com.ykcloud.soa.omp.cmasterdata.service;
//
//import com.gb.soa.omp.ccommon.util.JsonUtil;
//import com.google.common.collect.Lists;
//import com.ykcloud.soa.omp.cmasterdata.MdMain;
//import com.ykcloud.soa.omp.cmasterdata.api.model.LocImport;
//import com.ykcloud.soa.omp.cmasterdata.api.request.AllHeadSubUnitNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.DirectWayFirstReceiptStorageNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.FinalDistributionStorageNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.HeadSubUnitNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.LocDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.LocImportRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.LocInfosByPhysicalNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.LocSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.LogicStorageDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.LogicStorageSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.PayInfoByPayTypeIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.PayTypeDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.PayTypeSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.PhysicalListByPhysicalNumIdsGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.PhysicalNumIdByByStorageNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.PhysicalNumIdBySubUnitNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.PhysicalStorageByPhysicalNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.PhysicalStorageDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.PhysicalStorageSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.ShopProductDefaultReceiptStorageGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.ShopStorageListForReplenishGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.StorageInfoByStorageNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.StorageNumIdBySubUnitNumIdAndStorageDeptNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.SubUnitInfoBySubUnitNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.SubUnitInfoByUnitNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.SubUnitNumIdByStorageNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.SubUnitPriceTagCopyRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.SubUnitSaveRecstorageDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.SubUnitSaveRecstorageSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.SubUnitSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.SubcontractStorageNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.TagDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.TagGroupDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.TagGroupSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.TagItemDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.TagItemSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.TagSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.UnitNumIdBySubUnitNumIdGetRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.ZoneDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.ZoneSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.response.DirectWayFirstReceiptStorageNumIdGetResponse;
//import com.ykcloud.soa.omp.cmasterdata.api.response.FinalDistributionStorageNumIdGetResponse;
//import com.ykcloud.soa.omp.cmasterdata.api.response.PhysicalStorageByPhysicalNumIdGetResponse;
//import com.ykcloud.soa.omp.cmasterdata.api.response.PhysicalStorageByStorageNumIdGetResponse;
//import com.ykcloud.soa.omp.cmasterdata.api.response.StorageInfoByStorageNumIdGetResponse;
//import com.ykcloud.soa.omp.cmasterdata.api.response.SubUnitInfoByUnitNumIdGetResponse;
//import com.ykcloud.soa.omp.cmasterdata.api.response.SubUnitPriceTagCopyResponse;
//import com.ykcloud.soa.omp.cmasterdata.api.service.MdShopService;
//import com.ykcloud.soa.omp.cmasterdata.service.impl.MdUnitServiceImpl;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import javax.annotation.Resource;
//import java.util.Arrays;
//import java.util.List;
//
///**
// * @ClassName: MdShopServiceTest.java
// * @Description: mdShopServiceTest
// * @version: v1.0.0
// * @author: fred.zhao
// * @date: 2018年3月9日 下午4:25:26
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = MdMain.class)
//public class MdShopServiceTest {
//
//    private static Logger log = LoggerFactory.getLogger(MdUnitServiceImpl.class);
//
//    @Resource
//    private MdShopService mdShopService;
//
//
//
//
//
//    //门店档案保存
//    @Test
//    public void testSaveSubUnit() {
//        SubUnitSaveRequest request = new SubUnitSaveRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setUserNumId(666L);
//
//        //基本信息
//
//
//
//        mdShopService.saveSubUnit(request);
//    }
//
//
//
//
//
//
//
//
//
//    //库位导入
//    @Test
//    public void testImportLoc() {
//        LocImportRequest request = new LocImportRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setUserNumId(666L);
//        request.setZoneNumId(202L);
//        List<LocImport> locImportList = Lists.newArrayList();
//
//        LocImport l1 = new LocImport();
//        l1.setLocId("efeffett");
//        l1.setLocName("库位导入1");
//        l1.setEnLocName("import1");
//        l1.setTypeName("货架");
//        l1.setPhysicalNumId(10203L);
//        l1.setSubUnitNumId(100032L);
//        locImportList.add(l1);
//
//        LocImport l2 = new LocImport();
//        l2.setLocId("efefh");
//        l2.setLocName("库位导入2");
//        l2.setEnLocName("import2");
//        l2.setTypeName("库位");
//        l2.setPhysicalNumId(10301L);
//        l2.setSubUnitNumId(100032L);
//        locImportList.add(l2);
//
//        LocImport l3 = new LocImport();
//        l3.setLocId("efeffetth");
//        l3.setLocName("库位导入3");
//        l3.setEnLocName("import3");
//        l3.setPhysicalNumId(10302L);
//        l3.setSubUnitNumId(100032L);
//        locImportList.add(l3);
//
//        LocImport l4 = new LocImport();
//        l4.setLocId("efeffetth");
//        l4.setLocName("库位导入4");
//        l4.setEnLocName("import4");
//        l4.setPhysicalNumId(10303L);
//        l4.setSubUnitNumId(100032L);
//        locImportList.add(l4);
//        request.setLocImportList(locImportList);
//
//    }
//
//
//
//
//
//
//}
