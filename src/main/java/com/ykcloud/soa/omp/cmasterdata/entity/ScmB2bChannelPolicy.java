package com.ykcloud.soa.omp.cmasterdata.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 *结果
 */
@Data
public class ScmB2bChannelPolicy implements Serializable {
    private static final long serialVersionUID=1L;

    /** 行号 */
    @ApiModelProperty(value = "行号")
    private Long series;

    /** 租户 */
    @ApiModelProperty(value = "租户")
    private Long tenant_num_id;

    /** 状态标识，0正式，1测试，3删除，4作废 */
    @ApiModelProperty(value = "状态标识，0正式，1测试，3删除，4作废")
    private Long data_sign;

    /** 公司 */
    @ApiModelProperty(value = "公司")
    private String cort_num_id;

    /** 业务单元 */
    @ApiModelProperty(value = "业务单元")
    private String unit_num_id;

    /** 组织 */
    @ApiModelProperty(value = "组织")
    private String org_unit;

    /** 渠道 */
    @ApiModelProperty(value = "渠道")
    private Byte channel_num_id;

    /** 商品范围层级。1商品，2小类 */
    @ApiModelProperty(value = "商品范围层级。1商品，2小类")
    private Byte select_item_lv;

    /** 小类 */
    @ApiModelProperty(value = "小类")
    private String pty3_num_id;

    /** 商品 */
    @ApiModelProperty(value = "商品")
    private String item_num_id;

    /** 是否生效。0不生效，1生效 */
    @ApiModelProperty(value = "是否生效。0不生效，1生效")
    private Byte effect_sign;

    /** 取价模式 */
    @ApiModelProperty(value = "取价模式")
    private Byte price_model;

    /** 加价毛利率 */
    @ApiModelProperty(value = "加价毛利率")
    private BigDecimal price_add_rate;

    /** 固定加价 */
    @ApiModelProperty(value = "固定加价")
    private BigDecimal price_add_price;

    /** 来源 */
    @ApiModelProperty(value = "来源")
    private String form_type;

    /** 来源单号 */
    @ApiModelProperty(value = "来源单号")
    private String form_bill;

    /** 创建人 */
    @ApiModelProperty(value = "创建人")
    private Long create_user_id;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private Date create_dtme;

    /** 更新用户 */
    @ApiModelProperty(value = "更新用户")
    private Long update_user_id;

    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private Date update_dtme;

    /** 删除 */
    @ApiModelProperty(value = "删除")
    private String cancelsign;


}
