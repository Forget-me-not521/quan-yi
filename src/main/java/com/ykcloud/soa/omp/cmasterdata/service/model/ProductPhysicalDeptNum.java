package com.ykcloud.soa.omp.cmasterdata.service.model;

import com.gb.soa.omp.ccommon.api.request.AbstractRequest;

public class ProductPhysicalDeptNum extends AbstractRequest {
    private static final long serialVersionUID = -3386902840536992105L;
    //商品编号
    private Long itemNumId;

    public Long getItemNumId() {
        return itemNumId;
    }

    public void setItemNumId(Long itemNumId) {
        this.itemNumId = itemNumId;
    }

}
