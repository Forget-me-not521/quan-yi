package com.ykcloud.soa.omp.cmasterdata.entity;

public class SYS_POS_POWER {
    private Long SERIES;
    private Long TENANT_NUM_ID;
    private Long DATA_SIGN;
    private String ID;
    private String TEXT;
    private String URL;
    private String SUPPORT_MODEL;
    private String SPECIALAUTHORIZAITON;
    private String PARENTID;
    private Long TYPE_NUM_ID;
    private Long SEQ;

    public Long getSERIES() {
        return SERIES;
    }

    public void setSERIES(Long SERIES) {
        this.SERIES = SERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long TENANT_NUM_ID) {
        this.TENANT_NUM_ID = TENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTEXT() {
        return TEXT;
    }

    public void setTEXT(String TEXT) {
        this.TEXT = TEXT;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getSUPPORT_MODEL() {
        return SUPPORT_MODEL;
    }

    public void setSUPPORT_MODEL(String SUPPORT_MODEL) {
        this.SUPPORT_MODEL = SUPPORT_MODEL;
    }

    public String getSPECIALAUTHORIZAITON() {
        return SPECIALAUTHORIZAITON;
    }

    public void setSPECIALAUTHORIZAITON(String SPECIALAUTHORIZAITON) {
        this.SPECIALAUTHORIZAITON = SPECIALAUTHORIZAITON;
    }

    public String getPARENTID() {
        return PARENTID;
    }

    public void setPARENTID(String PARENTID) {
        this.PARENTID = PARENTID;
    }

    public Long getTYPE_NUM_ID() {
        return TYPE_NUM_ID;
    }

    public void setTYPE_NUM_ID(Long TYPE_NUM_ID) {
        this.TYPE_NUM_ID = TYPE_NUM_ID;
    }

    public Long getSEQ() {
        return SEQ;
    }

    public void setSEQ(Long SEQ) {
        this.SEQ = SEQ;
    }
}
