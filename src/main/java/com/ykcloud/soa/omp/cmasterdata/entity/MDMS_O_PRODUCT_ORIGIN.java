package com.ykcloud.soa.omp.cmasterdata.entity;

public class MDMS_O_PRODUCT_ORIGIN extends BaseEntity {

    private String PRODUCT_ORIGIN_ID;

    private Long PRODUCT_ORIGIN_NUM_ID;

    private String PRODUCT_ORIGIN_NAME;

    private Long PRV_NUM_ID;

    private Long CITY_NUM_ID;

    private Long CITY_AREA_NUM_ID;

    private Long TOWN_NUM_ID;

    private String ALREADYSEND;

    public String getPRODUCT_ORIGIN_ID() {
        return PRODUCT_ORIGIN_ID;
    }

    public void setPRODUCT_ORIGIN_ID(String pRODUCT_ORIGIN_ID) {
        PRODUCT_ORIGIN_ID = pRODUCT_ORIGIN_ID;
    }

    public Long getPRODUCT_ORIGIN_NUM_ID() {
        return PRODUCT_ORIGIN_NUM_ID;
    }

    public void setPRODUCT_ORIGIN_NUM_ID(Long pRODUCT_ORIGIN_NUM_ID) {
        PRODUCT_ORIGIN_NUM_ID = pRODUCT_ORIGIN_NUM_ID;
    }

    public String getPRODUCT_ORIGIN_NAME() {
        return PRODUCT_ORIGIN_NAME;
    }

    public void setPRODUCT_ORIGIN_NAME(String pRODUCT_ORIGIN_NAME) {
        PRODUCT_ORIGIN_NAME = pRODUCT_ORIGIN_NAME;
    }

    public Long getPRV_NUM_ID() {
        return PRV_NUM_ID;
    }

    public void setPRV_NUM_ID(Long pRV_NUM_ID) {
        PRV_NUM_ID = pRV_NUM_ID;
    }

    public Long getCITY_NUM_ID() {
        return CITY_NUM_ID;
    }

    public void setCITY_NUM_ID(Long cITY_NUM_ID) {
        CITY_NUM_ID = cITY_NUM_ID;
    }

    public Long getCITY_AREA_NUM_ID() {
        return CITY_AREA_NUM_ID;
    }

    public void setCITY_AREA_NUM_ID(Long cITY_AREA_NUM_ID) {
        CITY_AREA_NUM_ID = cITY_AREA_NUM_ID;
    }

    public Long getTOWN_NUM_ID() {
        return TOWN_NUM_ID;
    }

    public void setTOWN_NUM_ID(Long tOWN_NUM_ID) {
        TOWN_NUM_ID = tOWN_NUM_ID;
    }

    public String getALREADYSEND() {
        return ALREADYSEND;
    }

    public void setALREADYSEND(String aLREADYSEND) {
        ALREADYSEND = aLREADYSEND;
    }

}
