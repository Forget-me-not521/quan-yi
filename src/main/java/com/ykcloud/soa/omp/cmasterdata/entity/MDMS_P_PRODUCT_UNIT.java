package com.ykcloud.soa.omp.cmasterdata.entity;

import com.ykcloud.soa.erp.common.entity.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @Author stark.jiang
 * @Date 2021/09/03/10:09
 * @Description:
 * @Version 1.0
 */
@Data
public class MDMS_P_PRODUCT_UNIT extends BaseEntity {

    private String CORT_NUM_ID;
    private String ITEM_NUM_ID;
    private String PRO_CODE_OLD;
    private String PLACE;//地点',
    private String ORIGIN;//原产地',
    private Integer PATENT;//专利（下拉选择-dic）',
    private Date PATENT_BEGIN_DATE;//专利有效期',
    private Date PATENT_END_DATE;//专利有效期',
    private String PRO_LOCATION;//分部商品定位（下拉选择）',
    private Integer PRO_STATUS;
    private Integer IS_PRODUCT;//(1：商品,2非商品)
    private Integer NOVEL_CORONAVIRUS_FLAG;//是否新冠(1-是,0-否)',
    private Integer KEY_MAINTAIN_FLAG;//是否重点养护(1-是,0-否)',
    private Integer IS_SUB_STOP_USING;//分部是否停用编码(1-是,0-否)',
    private Double PROPOSAL_PRICE;//建议零售价',
    private Integer MIN_PACK_PRO_LONG;//最小包装商品长(mm)',
    private Integer MIN_PACK_PRO_WIDE;//最小包装商品宽(mm)',
    private Integer MIN_PACK_PRO_HEIGHT;//最小包装商品高(mm)',
    private Integer MEDIUM_PACK_QTY;//中包装数量',
    private Integer ITEM_PACK_QTY;//件包装数量',
    private Integer USED_DAYS;//可服用天数',
    private String INDICATIONS;//功能主治',
    private Integer IS_MEDICAL_INSURANCE;//医保(1-是,0-否)',
    private String MEDICAL_INSURANCE_NO;//医保编号(国家医保编码)',
    private String PROV_MEDICAL_INSURANCE_NO;//省医保编号(省级医保局编码)',
    private Integer TOXICOLOGY;//毒理(1-是,0-否)',
    private Integer SPIRIT;//精神(1-是,0-否)',
    private Integer EPHEDRINE;//麻黄碱(1-是,0-否)',
    private Integer ANAESTHESIA;//麻醉(1-是,0-否)',
    private Integer BIOCHEMISTRY;//生化(1-是,0-否)',
    private Integer STORAGE_CONDITIONS;//贮存条件',
    private Integer PRESCRIPTION_CLASS;//处方分类（下拉选择）',
    private Integer BID_MARK;//医院中标标识(1-是,0-否)',
    private Integer MAY_RETURN_FLAG;//是否可退换(1-是,0-否)',
    private Integer PURCHASE_ATTRIBUTE;//分部采购属性(下拉选择)',
    private Integer PURCHASE_SALE_PATTERN;//统采供销模式（下拉选择）',
    private Integer DISTRIBUTION_MODE;//配送方式（下拉选择）',
    private Integer NOT_RECEPTION_GROSS_PROFIT;//不计入前台毛利(1-是,0-否)',
    private Integer CHINESE_MEDICINE_AVOID;//中药十八反十九畏(1-是,0-否)',
    private String SEASON_PRO;//季节性商品',
    private Integer SALE_LEVEL;//销售级别（下拉选择，汉字，按子公司筛选）',
    private String REWARD_CONDITION;//奖励条件',
    private Integer PURCHASE_EMPE_ID;//采购员编号',
    private String PURCHASE_EMPE_NAME;//采购员名称',
    private Integer IS_BASE_DRUG;//是否基药(1-是,0-否)',
    private String MEDICATION_COURSE;//疗程用药',
    private Integer IS_BREAK;//是否拆零
    private Integer BREAK_UNIT;//拆零单位（下拉选择）',
    private Integer BREAK_UNIT_DENOMINATOR;//拆零单位分母',
    private Integer BREAK_UNIT_MOLECULE;//拆零单位分子',
    private String ATTRIBUTE1;//其它属性1（标记中药药典标准）',
    private String ATTRIBUTE2;//其它属性2（标记黄金单品）',
    private String ATTRIBUTE3;//其它属性3（标记流通品）',
    private String ATTRIBUTE4;//其它属性4（标记商品特别属性）',
    private Integer MAINTAIN_PERIOD;//养护周期',
    private Integer SALE_LIMIT_QTY;//限销数量',
    private Integer REBATE_CONDITION;//运输条件（下拉选择-汉字）',
    private Double WEIGHT;//重量（克）',
    private Integer BATCH_NO_MANAGE_FLAG;//是否批号管理(1-是,0-否)',
    private Integer MANAGE_SHOP_TYPE;//可经营门店类型（下拉选择,ABCD）',
    private Integer PRE_MAX_DISTRIBUTION_QTY;//单次最高配送量',
    private Integer SPARE_PARTS_AREA_MAX;//散件区上限',
    private Integer SPARE_PARTS_AREA_MIN;//散件区下限',
    private Integer DISTRIBUTION_BATCH;//配送批量',
    private String MEDICAL_INSURANCE_TYPE;//医保类型',
    private Integer QUALITY_MARK;//质量特殊标志（下拉选择-dic，数字）',
    private String QUALITY_STANDARD;//质量标准',
    private String MEDICAL_EQUIPMENT_LICENSE;//医疗器械生产/经营许可证',
    private String SALE_CODE;//经营资质代码',
    private Double POCKET_RATIO;//自付比例',
    private Integer SPECIAL_COUNTER;//专柜（下拉选择）',
    private Double COMMISSION;//提点',
    private Integer NETWORK_SALE_FLAG;//是否网上销售(1-是,0-否)' ,
    private Date APPROVAL_BEGIN_DATE;//批准文号证书效期',
    private Date APPROVAL_END_DATE;//批准文号证书效期',
    private Date PRODUCT_PROXY_BEGIN_DATE;//委托生产委托书的效期',
    private Date PRODUCT_PROXY_END_DATE;//委托生产委托书的效期',
    private Integer ESUPERVISE_FOLLOW;//电子监管码追溯',
    private Integer IS_STORAGE_ESUPERVISE;//是否仓库维护电子监管码(1-是,0-否)' ,
    private Date QUALITY_BEGIN_DATE;//质量标准效期',
    private Date QUALITY_END_DATE;//质量标准效期',
    private Integer STORE_AREA_MARK;//存储区域标识（下拉选择，按公司筛选）',
    private Integer IS_DUAL_CHANNEL;//是否双通道(1-是,0-否)',
    private Integer SALES_UNIT_NUM_ID;//销售单位',
    private Integer IS_ANALEPTIC;//兴奋剂标识(1-是,0-否)',
    private Integer IS_EASY_TASTE;//是否易串味(1-是,0-否)',
    private Integer ABC_MARK;//ABC标识（下拉选择，ABC）',
    private Double SUB_COST_TAX_RATE;
    private Double SUB_SALE_TAX_RATE;

    private Integer FIRST_PRO_LOCATION;
    private String TAX_RATE_ID;

    private Integer FORBID_PURCHASE;
    private Integer FORBID_DISTRIBUTION;
    private Integer FORBID_RETURN_STORE;
    private Integer FORBID_RETURN_FACTORY;
    private Integer FORBID_SALE;
}
