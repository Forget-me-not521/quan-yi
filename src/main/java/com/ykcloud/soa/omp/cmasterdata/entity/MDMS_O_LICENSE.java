package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MDMS_O_LICENSE extends BaseEntity {
    private String licenseNumId;

    private String licenseName;

}
