package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

/**
 * @Author stark.jiang
 * @Date 2021/10/22/9:22
 * @Description: 供应商年报确认表头
 * @Version 1.0
 */
@Data
public class MDMS_O_SUPPLY_ANNUAL_REPORT_HDR extends BaseEntity{
    private String CORT_NUM_ID;//公司编码',
    private String CORT_NAME;//公司名称',
    private String REPORT_YEAR;//年份 ',
    private String REPORT_TITLE;//标题',
    private Integer CONFIRM_QTY;//已确认数量
    private Integer TOTAL_QTY;//总数量
}
