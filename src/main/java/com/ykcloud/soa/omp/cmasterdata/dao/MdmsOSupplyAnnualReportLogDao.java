package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUPPLY_ANNUAL_REPORT_LOG;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @Author stark.jiang
 * @Date 2021/10/22/9:36
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsOSupplyAnnualReportLogDao extends Dao<MDMS_O_SUPPLY_ANNUAL_REPORT_LOG> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }
}
