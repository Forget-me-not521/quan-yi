package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsBlSupply;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_SUPPLY;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class MdmsBlSupplyDao extends Dao<MDMS_BL_SUPPLY> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_BL_SUPPLY.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_BL_SUPPLY.class, ",");
    private static final String TABLE_NAME = "MDMS_BL_SUPPLY";

    public List<MDMS_BL_SUPPLY> checkItemExist(Long tenantNumId, Long dataSign, String unifiedCreditCode, String reservedNo) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_supply ");
        sb.append(" where tenant_Num_id = ? and data_sign = ? and unified_social_credit_code = ?  and reserved_no = ? and cancelsign = 'N'");
        List<MDMS_BL_SUPPLY> mdmsBlSupplyList = jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, reservedNo, dataSign, unifiedCreditCode}, new BeanPropertyRowMapper<>(MDMS_BL_SUPPLY.class));
        return mdmsBlSupplyList;
    }

    public MDMS_BL_SUPPLY selectByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_supply ");
        sb.append(" where tenant_Num_id = ? and data_sign = ? and reserved_no = ? and cancelsign = 'N'");
        List<MDMS_BL_SUPPLY> list = jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(MDMS_BL_SUPPLY.class));
        if(CollectionUtils.isEmpty(list)){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据详情不存在!");
        }
        if(list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据详情有多条!");
        }
        return list.get(0);
    }

    public List<MdmsBlSupply> selectInfoByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_supply ");
        sb.append(" where tenant_Num_id = ? and data_sign = ? and reserved_no = ? and cancelsign = 'N'");
        List<MdmsBlSupply> list = jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(MdmsBlSupply.class));
        if(CollectionUtils.isEmpty(list)){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据详情不存在!");
        }
        if(list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据详情有多条!");
        }
        return list;
    }


    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }
}