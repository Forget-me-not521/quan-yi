package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.erp.common.exception.ErpExceptionType;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_NOTICE;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class MdmsONoticeDao extends Dao<MDMS_O_NOTICE> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_NOTICE.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_NOTICE.class, ",");
    private static final String TABLE_NAME = "MDMS_O_NOTICE";

    public MDMS_O_NOTICE getBySeries(Long tenantNumId, Integer dataSign, Long series) {
        String sql = "select * from MDMS_O_NOTICE where tenant_num_id = ? and data_sign = ? " +
                " and series = ? and cancelsign='N'";
        List<MDMS_O_NOTICE> list = jdbcTemplate.query(sql,
                new Object[]{tenantNumId, dataSign, series}, new BeanPropertyRowMapper<>(MDMS_O_NOTICE.class));
        if (CollectionUtils.isEmpty(list)) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "找不到公告 ！行号:" + series);
        }
        if (list.size() > 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "公告存在多条 ！行号:" + series);
        }
        return list.get(0);
    }

}