package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CITY_TOWN;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.*;

import javax.annotation.Resource;

@Repository
public class MdmsOCityTownDao {

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_CITY_TOWN.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_CITY_TOWN.class, ",");
    private static final String TABLE_NAME = "mdms_o_city_town";
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public Long getTownNumId(Long tenantNumId, Long dataSign, Long townNumId) {
        String sql = "select count(*) from mdms_o_city_town  " +
                "where tenant_num_id=? and data_sign=? and town_num_id = ? ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, townNumId}, Long.class);
    }

    public Long getTownNumIdMate(Long tenantNumId, Long dataSign, Long townNumId, Long cityAreaNumId) {
        String sql = "select count(*) from mdms_o_city_town  " +
                "where tenant_num_id=? and data_sign=? and town_num_id = ? and city_area_num_id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, townNumId, cityAreaNumId}, Long.class);
    }

    public boolean checkExistTwon(Long tenantNumId, Long dataSign, Long prvNumId, Long cityNumId, Long cityAreaNumId, Long townNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1)");
        sb.append(" from ");
        sb.append(" mdms_o_city_town ");
        sb.append(" where tenant_num_id=? and data_sign=? and prv_num_id=? and city_num_id = ? and city_area_num_id = ? and town_num_id = ? and cancelsign='N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, prvNumId, cityNumId, cityAreaNumId, townNumId}, Integer.class) == 0 ? true : false;
    }

    public int insertMdmsOCityTown(MDMS_O_CITY_TOWN town) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(TABLE_NAME);
        sb.append(" ( ");
        sb.append(SQL_COLS);
        sb.append(") values (");
        sb.append(WILDCARDS);
        sb.append(")");
        System.out.println(sb.toString());
        System.out.println(Arrays.asList(EntityFieldUtil.fieldSplitValue(MDMS_O_CITY_TOWN.class, town)));

        return jdbcTemplate.update(sb.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_CITY_TOWN.class, town));
    }

    public Long checkExistSeries(String series, Long tenantNumId, Long dataSign) {
        String sql = "select town_num_id from mdms_o_city_town where SERIES = ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        Long townNumId = jdbcTemplate.queryForObject(sql, Long.class, new Object[]{series, tenantNumId, dataSign});
        return townNumId;
    }

    public int updateMdmsOCityTown(Long townNumId, Long cityAreaNumId, Long cityNumId, Long prvNumId, String townName,
                                   String townSimNo, Long userNumId, String series, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city_town set town_num_id=?,city_area_num_id=?,city_num_id=?,prv_num_id=?,town_name=?,town_sim_no=?,last_update_user_id=? ,last_updtme = now(),updatedata='Y' where series =? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.update(sql, new Object[]{townNumId, cityAreaNumId, cityNumId, prvNumId, townName, townSimNo, userNumId, series, tenantNumId, dataSign});
    }

    public int deteteMdmsOCityTown(Long userNumId, String series, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city_town set cancelsign ='Y' ,last_update_user_id= ?,last_updtme=now() where series= ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.update(sql, new Object[]{userNumId, series, tenantNumId, dataSign});
    }

    public int deteteMdmsOCityTownByAreaId(Long userNumId, Long areaId, Long cityId, Long prvId, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city_town set cancelsign ='Y' ,last_update_user_id= ?,last_updtme=now() where city_area_num_id= ? and city_num_id=? and prv_num_id=? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.update(sql, new Object[]{userNumId, areaId, cityId, prvId, tenantNumId, dataSign});
    }

    public int deleteByprvId(Long userNumId, Long prvId, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city_town set cancelsign ='Y' ,last_update_user_id= ?,last_updtme=now() where prv_num_id =? and tenant_num_id=? and data_sign=? and cancelsign ='N' ";
        return jdbcTemplate.update(sql, new Object[]{userNumId, prvId, tenantNumId, dataSign});
    }

    public int checkExistTwonbyAreaIdAndcityIdAndPrvNumId(Long city_AREA_NUM_ID, Long city_NUM_ID, Long prv_NUM_ID,
                                                          Long tenantNumId, Long dataSign) {
        String sql = "select count(1) from mdms_o_city_town where city_area_num_id= ? and city_num_id=? and prv_num_id=? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{city_AREA_NUM_ID, city_NUM_ID, prv_NUM_ID, tenantNumId, dataSign});
    }






    public MDMS_O_CITY_TOWN getEntity(Long tenantNumId, Long dataSign, Long townNumId) {
        String sql = "select * from mdms_o_city_town where tenant_num_id=? and data_sign=? and town_num_id=? and cancelsign='N'";
        List<MDMS_O_CITY_TOWN> list=jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, townNumId},
                new BeanPropertyRowMapper<>(MDMS_O_CITY_TOWN.class));
        if(CollectionUtils.isNotEmpty(list)){
            return list.get(0);
        }
        return null;
    }
}
