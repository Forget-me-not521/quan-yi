package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

@Data
public class MDMS_O_LICENSE_SECOND extends BaseEntity {
    private String firstNumId;

    private String secondNumId;

    private String secondName;

    private String saleAllow;

}
