package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsBlPartner;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_PARTNER;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Repository
public class MdmsOPartnerDao extends Dao<MDMS_O_PARTNER> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_PARTNER.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_PARTNER.class, ",");
    private static final String TABLE_NAME = "MDMS_O_PARTNER";

    public List<MDMS_O_PARTNER> selectByUnifiedCreditCode(Long tenantNumId, Long dataSign, String unifiedCreditCode) {
        String sql = "select * from MDMS_O_PARTNER WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  UNIFIED_CREDIT_CODE = ? AND cancelsign = 'N'";
        List<MDMS_O_PARTNER> mdmsOPartnerList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, unifiedCreditCode}, new BeanPropertyRowMapper<>(MDMS_O_PARTNER.class));
        if (CollectionUtils.isNotEmpty(mdmsOPartnerList) && mdmsOPartnerList.size() > 1) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "存在多笔表单数据！");
        }
        return mdmsOPartnerList;
    }

    public List<MdmsBlPartner> getPartnerPage(Long tenantNumId, Long dataSign, String unifiedCreditCode, String cortNumId, String partnerNumId, String partnerName, Long createUserId, Date createDateBegin, Date createDateEnd, Integer pageNum, Integer pageSize) {
        String sql = "select c.*,s.* from mdms_o_partner c inner join mdms_o_sub_partner s on c.partner_num_id=s.partner_num_id " +
                "and c.tenant_num_id=s.tenant_num_id and c.data_sign=s.data_sign where c.tenant_num_id =? and c.data_sign =? and s.cort_num_id=? ";
        if(StringUtils.isNotEmpty(unifiedCreditCode)){
            sql+=" and c.unified_credit_code='"+unifiedCreditCode+"' ";
        }
        if(StringUtils.isNotEmpty(partnerNumId)){
            sql+=" and c.partner_num_id='"+partnerNumId+"' ";
        }
        if(StringUtils.isNotEmpty(partnerName)){
            sql+=" and c.partner_name='"+partnerName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sql+=" and c.create_user_id='"+createUserId+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateBegin)){
            sql+=" and c.create_dtme>'"+createDateBegin+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateEnd)){
            sql+=" and c.create_dtme<'"+createDateEnd+"' ";
        }
        Integer start = (pageNum-1)*pageSize;

        sql+=" limit ?,? ";
        List<MdmsBlPartner> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign,cortNumId,start,pageSize}, new BeanPropertyRowMapper<>(MdmsBlPartner.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public Integer getPartnerPageCount(Long tenantNumId, Long dataSign, String unifiedCreditCode, String cortNumId, String partnerNumId, String partnerName, Long createUserId, Date createDateBegin,Date createDateEnd) {
        String sql = "select count(1) from mdms_o_partner c inner join mdms_o_sub_partner s on c.partner_num_id=s.partner_num_id " +
                "and c.tenant_num_id=s.tenant_num_id and c.data_sign=s.data_sign  where c.tenant_num_id =? and c.data_sign =? and s.cort_num_id=? ";
        if(StringUtils.isNotEmpty(unifiedCreditCode)){
            sql+=" and c.unified_credit_code='"+unifiedCreditCode+"' ";
        }
        if(StringUtils.isNotEmpty(partnerNumId)){
            sql+=" and c.partner_num_id='"+partnerNumId+"' ";
        }
        if(StringUtils.isNotEmpty(partnerName)){
            sql+=" and c.partner_name='"+partnerName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sql+=" and c.create_user_id='"+createUserId+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateBegin)){
            sql+=" and c.create_dtme>'"+createDateBegin+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateEnd)){
            sql+=" and c.create_dtme<'"+createDateEnd+"' ";
        }
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign,cortNumId}, Integer.class);
    }

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }
}