package com.ykcloud.soa.omp.cmasterdata.enums;

public enum ProcessMasterSourceTypeEnums {
    BUSINESS(1, "业务单据"),
    FLOW(2, "流程单据");

    private int id;
    private String value;

    ProcessMasterSourceTypeEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (ProcessMasterSourceTypeEnums item : ProcessMasterSourceTypeEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
