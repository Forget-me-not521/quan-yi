package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class MDMS_O_CITY_AREA {

    /*
    当前需求不需要所有字段，需要的可以自行添加
 */
    private String SERIES;
    private Long TENANT_NUM_ID;
    private Long DATA_SIGN;
    private Long PRV_NUM_ID;
    private Long CITY_NUM_ID;
    private Long CITY_AREA_NUM_ID;
    private String CITY_AREA_SIM_NO;
    private String CITY_AREA_NAME;
    private Long EMPE_NUM_ID;
    private Date CREATE_DTME;
    private Date LAST_UPDTME;
    private Long CREATE_USER_ID;
    private Long LAST_UPDATE_USER_ID;
    private String CANCELSIGN;
    private String INSERTDATA;
    private String UPDATEDATA;
    private String SENDDATA;

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String sERIES) {
        SERIES = sERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long tENANT_NUM_ID) {
        TENANT_NUM_ID = tENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long dATA_SIGN) {
        DATA_SIGN = dATA_SIGN;
    }

    public Long getPRV_NUM_ID() {
        return PRV_NUM_ID;
    }

    public void setPRV_NUM_ID(Long pRV_NUM_ID) {
        PRV_NUM_ID = pRV_NUM_ID;
    }

    public Long getCITY_NUM_ID() {
        return CITY_NUM_ID;
    }

    public void setCITY_NUM_ID(Long cITY_NUM_ID) {
        CITY_NUM_ID = cITY_NUM_ID;
    }

    public Long getCITY_AREA_NUM_ID() {
        return CITY_AREA_NUM_ID;
    }

    public void setCITY_AREA_NUM_ID(Long cITY_AREA_NUM_ID) {
        CITY_AREA_NUM_ID = cITY_AREA_NUM_ID;
    }

    public String getCITY_AREA_SIM_NO() {
        return CITY_AREA_SIM_NO;
    }

    public void setCITY_AREA_SIM_NO(String cITY_AREA_SIM_NO) {
        CITY_AREA_SIM_NO = cITY_AREA_SIM_NO;
    }

    public String getCITY_AREA_NAME() {
        return CITY_AREA_NAME;
    }

    public void setCITY_AREA_NAME(String cITY_AREA_NAME) {
        CITY_AREA_NAME = cITY_AREA_NAME;
    }

    public Long getEMPE_NUM_ID() {
        return EMPE_NUM_ID;
    }

    public void setEMPE_NUM_ID(Long eMPE_NUM_ID) {
        EMPE_NUM_ID = eMPE_NUM_ID;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date cREATE_DTME) {
        CREATE_DTME = cREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date lAST_UPDTME) {
        LAST_UPDTME = lAST_UPDTME;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long cREATE_USER_ID) {
        CREATE_USER_ID = cREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long lAST_UPDATE_USER_ID) {
        LAST_UPDATE_USER_ID = lAST_UPDATE_USER_ID;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String cANCELSIGN) {
        CANCELSIGN = cANCELSIGN;
    }

    public String getINSERTDATA() {
        return INSERTDATA;
    }

    public void setINSERTDATA(String iNSERTDATA) {
        INSERTDATA = iNSERTDATA;
    }

    public String getUPDATEDATA() {
        return UPDATEDATA;
    }

    public void setUPDATEDATA(String uPDATEDATA) {
        UPDATEDATA = uPDATEDATA;
    }

    public String getSENDDATA() {
        return SENDDATA;
    }

    public void setSENDDATA(String sENDDATA) {
        SENDDATA = sENDDATA;
    }

}
