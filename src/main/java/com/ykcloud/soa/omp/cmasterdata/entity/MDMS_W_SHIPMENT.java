package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

@Data
public class MDMS_W_SHIPMENT extends BaseEntity{

    //公司
    private String CORT_NUM_ID;

    //门店
    private String SUB_UNIT_NUM_ID;

    //大仓编码
    private String PHYSICAL_NUM_ID;

    //大仓名称
    private String PHYSICAL_NAME;

    //装运点编号
    private String SHIPMENT_NUM_ID;

    //装运点名称
    private String SHIPMENT_NAME;

    //0:不可用，1：可用
    private Long ENABLE_SIGN;

    //备注
    private String REMARK;
}
