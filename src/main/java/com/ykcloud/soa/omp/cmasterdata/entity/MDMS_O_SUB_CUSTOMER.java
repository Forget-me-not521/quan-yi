package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Author stark.jiang
 * @Date 2021/09/14/15:06
 * @Description:
 * @Version 1.0
 */
@Data
public class MDMS_O_SUB_CUSTOMER extends BaseEntity {
    private String CUSTOMER_NUM_ID;
    private String CORT_NUM_ID;
    private String OLD_CUSTOMER_ID;//旧客户编码 ',
    private String FILE_NO;//档案号',
    private String QUALITY_LEADER;//质量负责人',
    private String PURCHASE_LEADER;
    private Integer ANNUAL_REPORT;//年度报告',
    private Date ANNUAL_REPORT_BEGIN;//年度报告效期开始',
    private Date ANNUAL_REPORT_END;//年度报告效期结束',
    private Integer PAYMENT_TERM;//付款条件',
    private String CORPORAT;//法人',
    private String CORPORAT_PROXY_NAME;//法人委托人姓名',
    private String CORPORAT_PROXY_STAT_IDCARD;//法人委托人的身份证号码',
    private Date CORPORAT_PROXY_IDCARD_BEGIN;//法人委托人的身份证有效期开始',
    private Date CORPORAT_PROXY_IDCARD_END;//法人委托人的身份证有效期结束',
    private Date CORPORAT_PROXY_STAT_BEGIN;//法人委托书效期开始',
    private Date CORPORAT_PROXY_STAT_END;//法人委托书效期结束',
    private Double SUB_CREDIT_LINE;//分部授信额度',
    private Integer SUB_CUSTOMER_STATUS;//状态ID',
    private String FINANCE_NAME;//财务联系人',
    private String FINANCE_TEL;//财务联系人电话',
    private Integer RECEIVING_BANK_NUM;//收款银行联行号',
    private String RECEIVING_BANK_NAME;//收款银行名称',
    private String RECEIVING_BANK_ACCOUNT;//收款银行账号',
    private String REMARK;//备注',
    private Integer MONEY_TYPE;//币别',
    private Integer CLOSE_DAYS;//关闭天数',
    private Integer FORBID_RETURN_FACTORY;//禁退厂(1-是,0-否)',
    private Integer FORBID_SALE;//禁售(1-是,0-否)',
    private Integer PRE_DISTRIBUTION;//送货前置期（天）',
    private Integer DIRECT_DELIVERY_FLAG;//直配标志(1-是,0-否)',
    private String STORAGE_ADR;//仓库地址',
    private String BUSINESS_CONTACT;//业务联系人',
    private String BUSINESS_CONTACT_TEL;//业务联系人电话',
    private String BUSINESS_CONTACT_IDCARD;//业务联系人身份证号码',
    private Integer SALE_LIMIT_DAYS;//限销近效期天数',
    private Integer LICENCE_WARN_DAYS;//证照预警天数',
    private String REPLENISHER;//销售员',
    private String DRUG_BUSINESS_LICENSE;//药品经营许可证',
    private Date DRUG_BUSINESS_LICENSE_BEGIN;//药品经营许可证有效期',
    private Date DRUG_BUSINESS_LICENSE_END;//药品经营许可证有效期',
    private String DRUG_PRODUCT_LICENSE;//药品生产许可证',
    private Date DRUG_PRODUCT_LICENSE_BEGIN;//药品生产许可证有效期开始',
    private Date DRUG_PRODUCT_LICENSE_END;//药品生产许可证有效期结束',
    private Date BUSINESS_LICENSE_BEGIN_DATE;//营业执照有效期
    private Date BUSINESS_LICENSE_END_DATE;//营业执照有效期
    private Date QUALITY_AGREEMENT_BEGIN_DATE;//质量协议书有效期
    private Date QUALITY_AGREEMENT_END_DATE;//质量协议书有效期
    private String MEDICAL_INSTITUTION_LICENSE;//医疗机构许可证',
    private Date MEDICAL_INSTITUTION_BEGIN;//医疗机构许可证有效期开始',
    private Date MEDICAL_INSTITUTION_END;//医疗机构许可证有效期结束',
    private String MEDICAL_DEVICE_BUS_LINCENSE;//医疗器械经营许可证',
    private String MEDICAL_DEVICE_BUS_SCOPE;//医疗器械经营范围',
    private Date MEDICAL_DEVICE_BUS_BEGIN;//医疗器械经营许可证有效期开始',
    private Date MEDICAL_DEVICE_BUS_END;//医疗器械经营许可证有效期结束',
    private String MEDICAL_DEVICE_PRO_REC2;//二类医疗器械经营备案',
    private String MEDICAL_DEVICE_PRO_REC2_SCOPE;//二类医疗器械经营备案范围',
    private String FOOD_BUS_LICENSE;//食品经营许可证',
    private String FOOD_BUS_LICENSE_SCOPE;//食品经营许可证范围',
    private Date FOOD_BUS_LICENSE_BEGIN;//食品经营许可证有效期开始',
    private Date FOOD_BUS_LICENSE_END;//食品经营许可证有效期结束',

}
