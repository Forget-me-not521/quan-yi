package com.ykcloud.soa.omp.cmasterdata.dao;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_W_PHYSICAL;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;

/**
 * fakir
 * 2018/4/28
 */
@Repository
public class MdmsWPhysicalDao extends Dao<MDMS_W_PHYSICAL> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_W_PHYSICAL.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_W_PHYSICAL.class, ",");
    private static final String TABLE_NAME = "MDMS_W_PHYSICAL";



    /**
     * 根据门店获取物理仓库列表
     *
     * @author tz.x
     * @date 2018年5月23日上午11:24:39
     */
    public List<Long> getPhysicalNumIdBySubUnitNumId(Long tenantNumId, Long dataSign, Long subUnitNumId) {
        String sql = "select physical_num_id from mdms_w_physical " +
                "where tenant_num_id = ? and data_sign = ? and sub_unit_num_id = ? and cancelsign = 'N' ";
        List<Long> physicalNumIdList = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, subUnitNumId}, Long.class);
        if (physicalNumIdList.isEmpty()) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店未查询到物理仓库！门店编号： " + subUnitNumId);
        }
        return physicalNumIdList;
    }





    public MDMS_W_PHYSICAL selectMdmsWPhysical(Long tenantNumId, Long dataSign, String physicalId) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select ");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" where TENANT_NUM_ID=? and DATA_SIGN=? and PHYSICAL_ID=?");
        return jdbcTemplate.queryForObject(stringBuffer.toString(), new Object[]{tenantNumId, dataSign, physicalId}, new BeanPropertyRowMapper<>(MDMS_W_PHYSICAL.class));
    }

    public MDMS_W_PHYSICAL selectMdmsWPhysicalByPhysicalNumId(Long tenantNumId, Long dataSign, Long physicalNumId) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select ");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" where TENANT_NUM_ID=? and DATA_SIGN=? and PHYSICAL_NUM_ID=?");
        return jdbcTemplate.queryForObject(stringBuffer.toString(), new Object[]{tenantNumId, dataSign, physicalNumId}, new BeanPropertyRowMapper<>(MDMS_W_PHYSICAL.class));
    }

    public int insertEntity(MDMS_W_PHYSICAL mdms_w_physical) {
        mdms_w_physical.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_W_PHYSICAL_SERIES));
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_W_PHYSICAL.class, mdms_w_physical));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "  数据插入失败! ");
        }
        return i;
    }

    public String getSubUnitNameByUnitNumId(Long dataSign, Long tenantNumId, Long subUnitNumId) {
        String sql = "select sub_unit_name from mdms_o_sub_unit where tenant_num_id = ? and data_sign = ? and sub_unit_num_id = ?";
        String subUnitName = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, subUnitNumId}, String.class);
        if (subUnitName == null || subUnitName == "") {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "未查到记录！门店编号： " + subUnitNumId);
        }
        return subUnitName;
    }


    public List<MDMS_W_PHYSICAL> getEntityListBySubUnitNumId(Long tenantNumId, Long dataSign, Long subUnitNumId) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select ");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" tenant_num_id = ? and data_sign = ? and sub_unit_num_id = ?");
        List<MDMS_W_PHYSICAL> list = jdbcTemplate.query(stringBuffer.toString(), new Object[]{tenantNumId, dataSign, subUnitNumId}, new BeanPropertyRowMapper<>(MDMS_W_PHYSICAL.class));
        if (CollectionUtils.isEmpty(list)) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "未查到数据！门店编号： " + subUnitNumId);
        }
        return list;
    }

    public boolean checkRecordIsExistByPhysicalNumId(Long tenantNumId, Long dataSign, Long physicalNumId) {
        String sql = "select count(1) from mdms_w_physical where tenant_num_id = ? and data_sign = ? and physical_num_id = ? and cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, physicalNumId}, Integer.class) == 0 ? false : true;
    }

    public Long getPhysicalNumIdBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "select physical_num_id from mdms_w_physical where tenant_num_id = ? and data_sign = ? and series = ? and cancelsign = 'N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, Long.class);
    }

    public Long getSubUnitNumIdBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "select sub_unit_num_id from mdms_w_physical where tenant_num_id = ? and data_sign = ? and series = ? and cancelsign = 'N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, Long.class);
    }

    public int deleteRecordBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "delete from mdms_w_physical  where tenant_num_id = ? and data_sign = ? and series = ? and cancelsign = 'N'";
        return jdbcTemplate.update(sql, tenantNumId, dataSign, series);
    }

    public void updateEntityByPhysicalNumId(Long tenantNumId, Long dataSign, Long physicalNumId, MDMS_W_PHYSICAL mdmsWPhysical) {
        String cols = EntityFieldUtil.wildCardSplitUpdate(mdmsWPhysical, ",");
        List<Object> list = new ArrayList<>(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_W_PHYSICAL.class, mdmsWPhysical)));
        list.add(tenantNumId);
        list.add(dataSign);
        list.add(physicalNumId);
        StringBuffer sb = new StringBuffer();
        sb.append("update mdms_w_physical set ");
        sb.append(cols);
        sb.append(" where  tenant_num_id = ? and data_sign = ? and physical_num_id = ? and cancelsign = 'N'");
        int row = jdbcTemplate.update(sb.toString(), list.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    String.format("跟新物理仓信息失败!物理仓编码:%d", physicalNumId));
        }
    }



    public int updateEntityByPhysicalBySeries(String series, MDMS_W_PHYSICAL mdmsWPhysical,String subUnitNumId,String cortNumId) {
        String cols = EntityFieldUtil.wildCardSplitUpdate(mdmsWPhysical, ",");
        List<Object> list = new ArrayList<>(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_W_PHYSICAL.class, mdmsWPhysical)));
        list.add(series);
        list.add(cortNumId);
        list.add(subUnitNumId);
        StringBuffer sb = new StringBuffer();
        sb.append("update mdms_w_physical set ");
        sb.append(cols);
        sb.append(" where series =?  and cancelsign = 'N' and cort_num_id=?  and  sub_unit_num_id=?  ");
        int row = jdbcTemplate.update(sb.toString(), list.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    String.format("更新物理仓信息失败!物理仓行号:%d", series));
        }
        return row;
    }

    public Long getSeriesByPhysicalNumId(Long tenantNumId, Long dataSign, Long physicalNumId) {
        String sql = "select series from mdms_w_physical where tenant_num_id = ? and data_sign = ? and physical_num_id = ? and cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, physicalNumId}, Long.class);
    }

    public void insertEntityAllFeild(MDMS_W_PHYSICAL mdmsWPhysical) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_W_PHYSICAL.class, mdmsWPhysical));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "  数据插入失败! ");
        }
    }

    public void updateStatus(Long tenantNumId, Long dataSign, Long physicalNumId, String cancelSign) {
        String sql = "update mdms_w_physical set cancelsign=? where tenant_num_id = ? and data_sign = ? and physical_num_id = ? and cancelsign = 'N'";
        jdbcTemplate.update(sql, cancelSign, tenantNumId, dataSign, physicalNumId);
    }

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public boolean checkPhyNumIdExist(Long tenantNumId, Long dataSign, String physicalNumId) {
        StringBuilder sb = new StringBuilder("SELECT count(1) from mdms_w_physical WHERE tenant_num_id = ? AND data_sign = ?");
        sb.append(" AND physical_num_id = ? AND cancelsign = 'N'");
        Integer count = jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, physicalNumId}, Integer.class);
        return count > 0 ? true : false;
    }
}
