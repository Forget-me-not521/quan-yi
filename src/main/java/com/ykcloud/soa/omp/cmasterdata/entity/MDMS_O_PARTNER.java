package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class MDMS_O_PARTNER implements Serializable {

    private Long SERIES;

    private Integer TENANT_NUM_ID;

    private Byte DATA_SIGN;

    private String PARTNER_NUM_ID;

    private String PARTNER_ID;

    private String PARTNER_NAME;

    private String UNIFIED_CREDIT_CODE;

    private Byte PARTNER_CLASSIFY;

    private String BUSINESS_SCOPE;

    private String SALE_ALLOW;

    private String PARTNER_ADR;

    private Byte PARTNER_TYPE;

    private String LEGAL_PERSON;

    private String REGISTER_ADR;

    private Date REGISTER_DATE;

    private BigDecimal REGISTER_CAPITAL;

    private Date CREATE_DTME;

    private Date LAST_UPDTME;

    private Long CREATE_USER_ID;

    private Long LAST_UPDATE_USER_ID;

    private String CANCELSIGN;

    private Long PRV_NUM_ID;

    private Integer CITY_NUM_ID;

    private Long TOWN_NUM_ID;

    private Integer CITY_AREA_NUM_ID;
}