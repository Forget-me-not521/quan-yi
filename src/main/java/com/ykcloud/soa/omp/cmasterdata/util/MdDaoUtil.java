package com.ykcloud.soa.omp.cmasterdata.util;

import io.seata.common.util.StringUtils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class MdDaoUtil {

    public static String CORT_NUM_ID_SUB_UNIT_NUM_ID_SQL(String tableName){
        if(StringUtils.isNotBlank(tableName)){
            tableName = tableName+".";
        }
        return "and " + tableName + "CORT_NUM_ID = ? and " + tableName + "SUB_UNIT_NUM_ID = ? ";
    }
    public static String TENANT_NUM_ID_DATA_SIGN_SQL(String tableName){
        if(StringUtils.isNotBlank(tableName)){
            tableName = tableName+".";
        }
        return "and " + tableName + "TENANT_NUM_ID = ? and " + tableName + "DATA_SIGN = ? and " + tableName + "cancelsign = 'N' ";
    }
    public static String CORT_NUM_ID_SUB_UNIT_NUM_ID_SQL = CORT_NUM_ID_SUB_UNIT_NUM_ID_SQL("");
    public static String TENANT_NUM_ID_DATA_SIGN_SQL = TENANT_NUM_ID_DATA_SIGN_SQL("");

    public static String IF_NOT_NULL_COL_SQL(String tableName,String colName,Object colValue){
        if(colValue!=null){
            tableName = tableName!=null?tableName + "." : "";
            return "and " + tableName  + colName + "= ? ";
        }
        return "";
    }
    public static String IF_NOT_NULL_COL_SQL(String colName,Object colValue){
        return IF_NOT_NULL_COL_SQL(null,colName,colValue);
    }

    public static String DATE_COL_SQL(String tableName, String colName, Date colValue1, Date colValue2){
        if(colValue1!=null){
            tableName = tableName!=null?tableName + "." : "";
            return "and " + tableName + colName + " between ? and ? ";
        }
        return "";
    }

    public static String DATE_COL_SQL(String tableName, String colName, Date date){
        if(null!=date){
            tableName = tableName!=null?tableName + "." : "";
            return "and DATE_FORMAT(" + tableName + colName + ", '%Y-%m-%d')= ? ";
        }
        return "";
    }

    public static String DATE_COL_SQL(String colName, Date date){
        if(null!=date){
            return "and DATE_FORMAT(" + colName + ", '%Y-%m-%d')= ? ";
        }
        return "";
    }
    public static Object[] THUE_OBJECT(Object[] objects){
        List<Object> list = new LinkedList<>();
        for(Object obj : objects){
            if(obj != null){
                list.add(obj);
            }
        }
        return list.toArray();
    }
}
