package com.ykcloud.soa.omp.cmasterdata.entity;



public class MDMS_M_WAREHOUSE_CONFIG extends BaseEntity{
    private Integer UNIT_NUM_ID;
    private Long SUB_UNIT_NUM_ID;
    private Long PHYSICAL_NUM_ID;
    private String PHYSICAL_ID;
    private Integer REPLENISH_FALG;

    public Integer getUNIT_NUM_ID() {
        return UNIT_NUM_ID;
    }

    public void setUNIT_NUM_ID(Integer UNIT_NUM_ID) {
        this.UNIT_NUM_ID = UNIT_NUM_ID;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long SUB_UNIT_NUM_ID) {
        this.SUB_UNIT_NUM_ID = SUB_UNIT_NUM_ID;
    }

    public Long getPHYSICAL_NUM_ID() {
        return PHYSICAL_NUM_ID;
    }

    public void setPHYSICAL_NUM_ID(Long PHYSICAL_NUM_ID) {
        this.PHYSICAL_NUM_ID = PHYSICAL_NUM_ID;
    }

    public String getPHYSICAL_ID() {
        return PHYSICAL_ID;
    }

    public void setPHYSICAL_ID(String PHYSICAL_ID) {
        this.PHYSICAL_ID = PHYSICAL_ID;
    }

    public Integer getREPLENISH_FALG() {
        return REPLENISH_FALG;
    }

    public void setREPLENISH_FALG(Integer REPLENISH_FALG) {
        this.REPLENISH_FALG = REPLENISH_FALG;
    }
}
