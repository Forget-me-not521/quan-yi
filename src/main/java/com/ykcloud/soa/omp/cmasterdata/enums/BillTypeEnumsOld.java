package com.ykcloud.soa.omp.cmasterdata.enums;

public enum BillTypeEnumsOld {
    MDMSOCORTINSERT(111, "单据审核价格新增"),
    MDMSOCORTUPDATE(112, "单据审核价格更新"),
    MDMSOUNITINSERT(121, "客户单元表新增新增"),
    MDMSOUNITUPDATE(122, "客户单元表新增更新"),
    MDMSOSUBUNITINSERT(131, "门店主表新增新增"),
    MDMSOSUBUNITUPDATE(132, "门店主表新增更新"),
    PHYSICALSTORAGINSERT(140, "物理仓信息新增"),
    PHYSICALSTORAGUPDATE(141, "物理仓信息更新"),
    PHYSICALSTORAGDELETE(142, "物理仓信息删除"),
    LOGICSTORAGEINSERT(143, "物理仓信息新增"),
    LOGICSTORAGEUPDATE(144, "物理仓信息更新"),
    LOGICSTORAGEDELETE(145, "物理仓信息删除"),
    LOCINSERT(146, "库位信息新增"),
    LOCUPDATE(147, "库位信息更新"),
    LOCUPDELETE(148, "库位信息删除"),
    TAGGROUPINSERT(149, "标签组信息新增"),
    TAGGROUPUPDATE(150, "标签组信息更新"),
    TAGGROUPUPDELETE(151, "标签组信息删除"),
    TAGINSERT(152, "标签信息新增"),
    TAGUPDATE(153, "标签信息更新"),
    TAGDELETE(154, "标签信息删除"),
    TAGITEMINSERT(155, "标签执行机构信息新增"),
    TAGITEMUPDATE(156, "标签执行机构信息更新"),
    TAGITEMDELETE(157, "标签执行机构信息删除");

    private int id;
    private String value;

    BillTypeEnumsOld(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getBillTypeById(int id) {
        for (BillTypeEnumsOld item : BillTypeEnumsOld.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
