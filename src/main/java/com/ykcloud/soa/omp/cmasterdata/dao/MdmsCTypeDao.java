package com.ykcloud.soa.omp.cmasterdata.dao;


import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.EntityFieldUtil;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.api.model.TypeInfo;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_C_TYPE;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

/**
 * @author Song
 * @Description:获取业务类型名称
 * @Date 2018年5月7日 下午8:59:30
 */
@Repository
public class MdmsCTypeDao {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_C_TYPE.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_C_TYPE.class, ",");
    private static final String TABLE_NAME = "MDMS_C_TYPE";


    //根据业务类型ID，获取业务类型名称
    public String getTypeNameByTableNameAndFieldNameAndTypeNumId(Long tenantNumId, Long dataSign,
                                                                 String tableName, String fieldName, Long typeNumId) {
        String sql = "select type_name from  mdms_c_type " +
                "where tenant_num_id=? and data_sign=? and table_name=? and field_name=?   " +
                "and type_num_id=? and cancelsign='N' ";
        List<String> typeNameList = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, tableName, fieldName,
                typeNumId}, String.class);
        if (typeNameList.isEmpty()) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "查数据字典名称失败！表名:" + tableName + ",栏位名:" + fieldName + ",值:" + typeNumId);
        }
        if (typeNameList.size() > 1) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "查到多笔数据字典名称！表名:" + tableName + ",栏位名:" + fieldName + ",值:" + typeNumId);
        }
        return typeNameList.get(0);
    }

    //根据业务类型ID，获取业务类型名称
    public boolean checkExistByTableNameAndFieldNameAndTypeName(Long tenantNumId, Long dataSign,
                                                                String tableName, String fieldName, String typeName) {
        String sql = "select type_num_id from mdms_c_type where tenant_num_id=? and data_sign=? and " +
                "table_name=? and field_name=? and type_name=? and cancelsign='N' ";
        List<Long> typeNumIds = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign,
                tableName, fieldName, typeName}, Long.class);
        if (CollectionUtils.isNotEmpty(typeNumIds)) {
            return true;
        }
        return false;
    }

    public boolean checkExistByTableNameAndFieldNameAndStatusNumId(Long tenantNumId, Long dataSign,
                                                                   String tableName, String fieldName, Long statusNumId) {
        String sql = "select type_num_id from mdms_c_type where tenant_num_id=? and data_sign=? and " +
                "table_name=? and field_name=? and status_num_id=? and cancelsign='N' ";
        List<Long> typeNumIds = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign,
                tableName, fieldName, statusNumId}, Long.class);
        if (CollectionUtils.isNotEmpty(typeNumIds)) {
            return true;
        }
        return false;
    }

    public boolean checkExistByTableNameAndFieldNameAndTypeNumId(Long tenantNumId, Long dataSign,
                                                                 String tableName, String fieldName, Long typeNunId) {
        String sql = "select count(1) from mdms_c_type where tenant_num_id=? and data_sign=? and " +
                "table_name=? and field_name=? and type_num_id=? and cancelsign='N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, tableName, fieldName, typeNunId},
                Long.class) > 0 ? true : false;
    }

    public List<Long> getTypeNumIdListByTableNameAndFieldName(Long tenantNumId, Long dataSign, String tableName, String fieldName) {
        StringBuilder sb = new StringBuilder();
        sb.append("select type_num_id");
        sb.append(" from ");
        sb.append(TABLE_NAME);
        sb.append(" where tenant_Num_id=? And data_sign=? And table_name =? AND field_name =? ");
        return jdbcTemplate.queryForList(sb.toString(), new Object[]{tenantNumId, dataSign, tableName, fieldName}, Long.class);
    }

    public List<Long> getTypeNumIdListByTableNameAndFieldNameAndTypeName(Long tenantNumId, Long dataSign, String tableName, String fieldName, String typeName) {
        StringBuilder sb = new StringBuilder();
        sb.append("select type_num_id");
        sb.append(" from ");
        sb.append(TABLE_NAME);
        sb.append(" where tenant_Num_id=? And data_sign=? And table_name =? AND field_name =? and type_name=? and status_num_id=1");
        return jdbcTemplate.queryForList(sb.toString(), new Object[]{tenantNumId, dataSign, tableName, fieldName, typeName}, Long.class);
    }

    public List<String> getTypeNameListByTableNameAndFieldNameAndTypeNumId(Long tenantNumId, Long dataSign, String tableName, String fieldName, String typeNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select type_name");
        sb.append(" from ");
        sb.append(TABLE_NAME);
        sb.append(" where tenant_Num_id=? And data_sign=? And table_name =? AND field_name =? and type_num_id=? and status_num_id=1");
        return jdbcTemplate.queryForList(sb.toString(), new Object[]{tenantNumId, dataSign, tableName, fieldName, typeNumId}, String.class);
    }

    public List<Long> getSubUnitTypeByTableNameAndFieldName(Long tenantNumId, Long dataSign, String tableName, String fieldName) {
        StringBuilder sb = new StringBuilder();
        sb.append("select type_num_id");
        sb.append(" from ");
        sb.append(TABLE_NAME);
        sb.append(" where tenant_Num_id=? And data_sign=? And table_name =? AND field_name =? and type_num_id not in (12,21)");
        return jdbcTemplate.queryForList(sb.toString(), new Object[]{tenantNumId, dataSign, tableName, fieldName}, Long.class);
    }


    public List<Long> getTypeNumIdListByTableNameAndFieldNameAndTypeNumId(Long tenantNumId, Long dataSign, String tableName, String fieldName, String typeNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select type_num_id");
        sb.append(" from ");
        sb.append(TABLE_NAME);
        sb.append(" where tenant_Num_id=? And data_sign=? And table_name =? AND field_name =? and type_num_id=? and status_num_id=1 and cancelsign='N'");
        return jdbcTemplate.queryForList(sb.toString(), new Object[]{tenantNumId, dataSign, tableName, fieldName, typeNumId}, Long.class);
    }

    //获取数据库的当前时间
    public Date getDbCurrentDtme() {
        String sql = "select now()";
        return jdbcTemplate.queryForObject(sql, Date.class);
    }


    public void insertMdmsCType(MDMS_C_TYPE mdms_c_type) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_C_TYPE.class, mdms_c_type));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "  数据插入失败! ");
        }
    }

    public boolean checkExistBySeries(Long tenantNumId, Long dataSign, Long series) {
        return jdbcTemplate.queryForObject("select count(*) from MDMS_C_TYPE where tenant_num_id=? and data_sign=? and series=? and cancelsign='N'", new Object[]{tenantNumId, dataSign, series}, Integer.class) > 0;
    }

    public void updateEntity(MDMS_C_TYPE entity) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE mdms_c_type SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(entity, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND series = ?");
        List<Object> objects = new ArrayList(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_C_TYPE.class, entity)));
        objects.add(entity.getDATA_SIGN());
        objects.add(entity.getTENANT_NUM_ID());
        objects.add(entity.getSERIES());
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_C_TYPE更新失败!");
        }
    }

    public void deleteRecordBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "delete from MDMS_C_TYPE where TENANT_NUM_ID = ? AND DATA_SIGN = ? and series=? and cancelsign='N'";
        int row = jdbcTemplate.update(sql, tenantNumId, dataSign, series);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_C_TYPE删除失败!");
        }
    }

    public Long getTypeNumIdByTableNameAndFieldNameAndTypeName(Long tenantNumId, Long dataSign, String tableName, String fieldName) {
        StringBuilder sb = new StringBuilder();
        sb.append("select type_num_id");
        sb.append(" from ");
        sb.append(TABLE_NAME);
        sb.append(" where tenant_Num_id=? And data_sign=? And table_name =? AND field_name =? and type_name=? and status_num_id=1");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, tableName, fieldName}, Long.class);
    }

    public boolean checkPoSpecExistBySeries(Long tenantNumId, Long dataSign, String series) {
        return jdbcTemplate.queryForObject("select count(*) from mdms_p_product_po_spec where tenant_num_id=? and data_sign=? and series=? and cancelsign='N'", new Object[]{tenantNumId, dataSign, series}, Integer.class) > 0;
    }

    public String getItemidByItemNumId(Long tenantNumId, Long dataSign, Long itemNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select itemid ");
        sb.append(" from mdms_p_product_basic ");
        sb.append(" where tenant_Num_id=? And data_sign=?  and item_num_id = ? and cancelsign = 'N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, itemNumId}, String.class);
    }



    public boolean checkPoSpecExistByItemNumId(Long tenantNumId, Long dataSign, Long itemNumId, Long subUnitNumId, Long subUnitType) {
        return jdbcTemplate.queryForObject("select count(1) from mdms_p_product_po_spec where tenant_num_id=? and data_sign=? and item_num_id=? and sub_unit_num_id=? and sub_unit_type=? and  cancelsign='N'", new Object[]{tenantNumId, dataSign, itemNumId, subUnitNumId, subUnitType}, Integer.class) > 0 ? true : false;
    }




    public void deletePoSpecBySeries(Long tenantNumId, Long dataSign, String series) {
        String sql = "delete from mdms_p_product_po_spec where TENANT_NUM_ID = ? AND DATA_SIGN = ? and series=? and cancelsign='N'";
        int row = jdbcTemplate.update(sql, tenantNumId, dataSign, series);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_P_PRODUCT_PO_SPEC删除失败!");
        }
    }


    public List<MDMS_C_TYPE> getCTypeList(Long tenantNumId, Long dataSign) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append("select ");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" where tenant_num_id =? and data_sign=?  and  cancelsign='N'");
        return jdbcTemplate.query(stringBuffer.toString(), new Object[]{tenantNumId, dataSign}, new BeanPropertyRowMapper<MDMS_C_TYPE>(MDMS_C_TYPE.class));
    }


    public void updateCType(MDMS_C_TYPE entity) {
        String sql = "update MDMS_C_TYPE set  type_name=?,notes=?,status_num_id=?,last_updtme = now(),"
                + " last_update_user_id = ? where tenant_num_id=? and data_sign=? and table_name =? and field_name=? "
                + " and cancelsign='N'";
        int row = jdbcTemplate.update(sql, entity.getTYPE_NAME(), entity.getNOTES(), entity.getSTATUS_NUM_ID(), entity.getLAST_UPDATE_USER_ID(), entity.getTENANT_NUM_ID(), entity.getDATA_SIGN(), entity.getTABLE_NAME(), entity.getFIELD_NAME());
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35002, "更新失败!");
        }

    }

    public void deleteMdmsCTypeBySeries(Long userNumId, String series, String tableName, String fieldName, Long tenantNumId,
                                        Long dataSign) {
        String sql = "delete from MDMS_C_TYPE "
                + " where series = ? and table_name = ? and field_name= ? and tenant_num_id=? and data_sign=? and cancelsign='N' ";
        int row = jdbcTemplate.update(sql, new Object[]{series, tableName, fieldName, tenantNumId, dataSign});
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35002, "deleteMdmsCTypeBySeries删除失败!");
        }
    }


    public List<TypeInfo> getTypeInfoList(Long tenantNumId, Long dataSign,String tableName, String fieldName) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append("select type_num_id,type_name,notes ");
        stringBuffer.append(" from ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" where tenant_num_id =? and data_sign=?  and  table_name=? and field_name=?  and status_num_id=1  and cancelsign='N'");
        return jdbcTemplate.query(stringBuffer.toString(), new Object[]{tenantNumId, dataSign,tableName,fieldName}, new BeanPropertyRowMapper<TypeInfo>(TypeInfo.class));
    }
}
