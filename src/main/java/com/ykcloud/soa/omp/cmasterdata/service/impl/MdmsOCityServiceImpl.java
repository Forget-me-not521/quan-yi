package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.ykcloud.soa.omp.cmasterdata.api.service.MdmsOCityService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import org.springframework.web.bind.annotation.RestController;


@Service("mdCityService")
@RestController
public class MdmsOCityServiceImpl implements MdmsOCityService {
    private static final Logger log = LoggerFactory.getLogger(MdTenantServiceImpl.class);

}
