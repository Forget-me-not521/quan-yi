package com.ykcloud.soa.omp.cmasterdata.entity;

public class MDMS_P_UNITS extends BaseEntity {

    private Long UNITS_NUM_ID;
    private String UNITS_NAME;
    private String EN_UNITS_NAME;
    private String WL_SIGN;
    private String ALREADYSEND;
    private String UNIT_SIM_NO;

    public Long getUNITS_NUM_ID() {
        return UNITS_NUM_ID;
    }

    public void setUNITS_NUM_ID(Long uNITS_NUM_ID) {
        UNITS_NUM_ID = uNITS_NUM_ID;
    }

    public String getUNITS_NAME() {
        return UNITS_NAME;
    }

    public void setUNITS_NAME(String uNITS_NAME) {
        UNITS_NAME = uNITS_NAME;
    }

    public String getEN_UNITS_NAME() {
        return EN_UNITS_NAME;
    }

    public void setEN_UNITS_NAME(String eN_UNITS_NAME) {
        EN_UNITS_NAME = eN_UNITS_NAME;
    }

    public String getWL_SIGN() {
        return WL_SIGN;
    }

    public void setWL_SIGN(String wL_SIGN) {
        WL_SIGN = wL_SIGN;
    }

    public String getALREADYSEND() {
        return ALREADYSEND;
    }

    public void setALREADYSEND(String aLREADYSEND) {
        ALREADYSEND = aLREADYSEND;
    }

    public String getUNIT_SIM_NO() {
        return UNIT_SIM_NO;
    }

    public void setUNIT_SIM_NO(String uNIT_SIM_NO) {
        UNIT_SIM_NO = uNIT_SIM_NO;
    }

}
