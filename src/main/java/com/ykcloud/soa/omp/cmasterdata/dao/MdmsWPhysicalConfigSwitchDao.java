package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.exception.ErpExceptionType;
import com.ykcloud.soa.omp.cmasterdata.api.model.ConfigSwitchInfo;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.DaoUtil;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName MdmsWPhysicalConfigSwitch
 * @Description
 * @Author Dan
 * @Date 2021/10/25 15:51
 */
@Repository
public class MdmsWPhysicalConfigSwitchDao {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public Long getOnOffStatusByKey(Long tenantNumId, Long dataSign, String cortNumId, String subUnitNumId,
                                    String physicalNumId, String switchKey) {
        StringBuilder sb = new StringBuilder("SELECT on_off from mdms_w_physical_config_switch");
        sb.append(" where tenant_num_id = ? and data_sign  =? and cort_num_id = ? and sub_unit_num_id = ?");
        sb.append(" and physical_num_id = ? and switch_key = ? and cancelsign = 'N'");
        return jdbcTemplate.queryForObject(sb.toString(),
                new Object[]{tenantNumId, dataSign, cortNumId, subUnitNumId, physicalNumId, switchKey}, Long.class);
    }

    public void updateOnOffStatusByKey(Long tenantNumId, Long dataSign, String cortNumId, String subUnitNumId,
                                      String physicalNumId, List<ConfigSwitchInfo> configSwitchInfos) {
        StringBuilder sb = new StringBuilder("update mdms_w_physical_config_switch set on_off = ?, switch_value = ?");
        sb.append(" where tenant_num_id = ? and data_sign  =? and cort_num_id = ? and sub_unit_num_id = ?");
        sb.append("  and physical_num_id = ? and switch_key = ? and cancelsign = 'N'");

        List<Object[]> batchArgs = new ArrayList<>();
        for (ConfigSwitchInfo dtl : configSwitchInfos) {
            Object[] args = new Object[]{
                    dtl.getStatus(),
                    dtl.getSwitchValue(),
                    dataSign,
                    cortNumId,
                    subUnitNumId,
                    physicalNumId,
                    dtl.getSwitchKey()
            };
            batchArgs.add(args);

        }
        int[] rows = jdbcTemplate.batchUpdate(sb.toString(), batchArgs);
        int sum = DaoUtil.sum(rows);
        if (sum != batchArgs.size()) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "更新仓库开关失败！");
        }
    }
}
