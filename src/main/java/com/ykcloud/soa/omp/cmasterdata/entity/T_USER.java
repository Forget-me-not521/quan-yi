package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class T_USER {
    private Long ID;
    private Long TENANT_NUM_ID;
    private Long DATA_SIGN;
    private String NAME;
    private String LOGIN_NAME;
    private String SALT;
    private String PASSWORD;
    private String MAIN_ORG;
    private String GENDER;
    private Date BIRTHDAY;
    private String PHONE;
    private String EMAIL;
    private String PHOTO;
    private String ENGLISH_NAME;
    private Date PASSWORDMODIFYTIME;
    private String COMMENTS;
    private String CREATE_USER;
    private Date CREATE_DATE;
    private String UPDATE_USER;
    private Date UPDATE_DATE;
    private String IS_VALID;

    public Long getID() {
        return ID;
    }

    public void setID(Long iD) {
        ID = iD;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String nAME) {
        NAME = nAME;
    }

    public String getLOGIN_NAME() {
        return LOGIN_NAME;
    }

    public void setLOGIN_NAME(String lOGIN_NAME) {
        LOGIN_NAME = lOGIN_NAME;
    }

    public String getSALT() {
        return SALT;
    }

    public void setSALT(String sALT) {
        SALT = sALT;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setPASSWORD(String pASSWORD) {
        PASSWORD = pASSWORD;
    }

    public String getMAIN_ORG() {
        return MAIN_ORG;
    }

    public void setMAIN_ORG(String mAIN_ORG) {
        MAIN_ORG = mAIN_ORG;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String gENDER) {
        GENDER = gENDER;
    }

    public Date getBIRTHDAY() {
        return BIRTHDAY;
    }

    public void setBIRTHDAY(Date bIRTHDAY) {
        BIRTHDAY = bIRTHDAY;
    }

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String pHONE) {
        PHONE = pHONE;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String eMAIL) {
        EMAIL = eMAIL;
    }

    public String getPHOTO() {
        return PHOTO;
    }

    public void setPHOTO(String pHOTO) {
        PHOTO = pHOTO;
    }

    public String getENGLISH_NAME() {
        return ENGLISH_NAME;
    }

    public void setENGLISH_NAME(String eNGLISH_NAME) {
        ENGLISH_NAME = eNGLISH_NAME;
    }

    public Date getPASSWORDMODIFYTIME() {
        return PASSWORDMODIFYTIME;
    }

    public void setPASSWORDMODIFYTIME(Date pASSWORDMODIFYTIME) {
        PASSWORDMODIFYTIME = pASSWORDMODIFYTIME;
    }

    public String getCOMMENTS() {
        return COMMENTS;
    }

    public void setCOMMENTS(String cOMMENTS) {
        COMMENTS = cOMMENTS;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public void setCREATE_USER(String cREATE_USER) {
        CREATE_USER = cREATE_USER;
    }

    public Date getCREATE_DATE() {
        return CREATE_DATE;
    }

    public void setCREATE_DATE(Date cREATE_DATE) {
        CREATE_DATE = cREATE_DATE;
    }

    public String getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(String uPDATE_USER) {
        UPDATE_USER = uPDATE_USER;
    }

    public Date getUPDATE_DATE() {
        return UPDATE_DATE;
    }

    public void setUPDATE_DATE(Date uPDATE_DATE) {
        UPDATE_DATE = uPDATE_DATE;
    }

    public String getIS_VALID() {
        return IS_VALID;
    }

    public void setIS_VALID(String iS_VALID) {
        IS_VALID = iS_VALID;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long TENANT_NUM_ID) {
        this.TENANT_NUM_ID = TENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }
}
