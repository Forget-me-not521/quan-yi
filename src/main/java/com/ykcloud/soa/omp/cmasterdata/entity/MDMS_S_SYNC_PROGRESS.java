package com.ykcloud.soa.omp.cmasterdata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class MDMS_S_SYNC_PROGRESS {

    private String SERIES;// 行号
    private Long TENANT_NUM_ID;// 租户ID
    private Long DATA_SIGN;// 0: 正式 1：测试
    private String TABLE_NAME;
    private Integer TYPE_NUM_ID;
    private String PROGRESS;
    private Integer STATUS_NUM_ID;// 3-有效
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date CREATE_DTME;// 创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date LAST_UPDTME;// 最后更新时间
    private Long CREATE_USER_ID;// 用户
    private Long LAST_UPDATE_USER_ID;// 更新用户
    private String CANCELSIGN;

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String sERIES) {
        SERIES = sERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long tENANT_NUM_ID) {
        TENANT_NUM_ID = tENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long dATA_SIGN) {
        DATA_SIGN = dATA_SIGN;
    }

    public String getTABLE_NAME() {
        return TABLE_NAME;
    }

    public void setTABLE_NAME(String tABLE_NAME) {
        TABLE_NAME = tABLE_NAME;
    }

    public String getPROGRESS() {
        return PROGRESS;
    }

    public void setPROGRESS(String pROGRESS) {
        PROGRESS = pROGRESS;
    }

    public Integer getSTATUS_NUM_ID() {
        return STATUS_NUM_ID;
    }

    public void setSTATUS_NUM_ID(Integer sTATUS_NUM_ID) {
        STATUS_NUM_ID = sTATUS_NUM_ID;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date cREATE_DTME) {
        CREATE_DTME = cREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date lAST_UPDTME) {
        LAST_UPDTME = lAST_UPDTME;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long cREATE_USER_ID) {
        CREATE_USER_ID = cREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long lAST_UPDATE_USER_ID) {
        LAST_UPDATE_USER_ID = lAST_UPDATE_USER_ID;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String cANCELSIGN) {
        CANCELSIGN = cANCELSIGN;
    }

    public Integer getTYPE_NUM_ID() {
        return TYPE_NUM_ID;
    }

    public void setTYPE_NUM_ID(Integer tYPE_NUM_ID) {
        TYPE_NUM_ID = tYPE_NUM_ID;
    }


}
