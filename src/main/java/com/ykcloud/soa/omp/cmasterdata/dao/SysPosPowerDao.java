package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.entity.SYS_POS_POWER;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.DaoUtil;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class SysPosPowerDao {

    @Resource(name = "platFormJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(SYS_POS_POWER.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(SYS_POS_POWER.class, ",");
    private static final String TABLE_NAME = "SYS_POS_POWER";

    public void insertSysPosPower(SYS_POS_POWER sys_pos_power) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(SYS_POS_POWER.class, sys_pos_power));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "  数据插入失败! ");
        }
    }

    public boolean checkExistBySeries(String series, Long tenantNumId, Long dataSign) {
        return jdbcTemplate.queryForObject("select count(*) from SYS_POS_POWER where tenant_num_id=? and data_sign=? and series=? ", new Object[]{tenantNumId, dataSign, series}, Integer.class) > 0;
    }

    public void updateEntity(SYS_POS_POWER entity) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE SYS_POS_POWER SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(entity, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND series = ?");
        List<Object> objects = new ArrayList(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(SYS_POS_POWER.class, entity)));
        objects.add(entity.getDATA_SIGN());
        objects.add(entity.getTENANT_NUM_ID());
        objects.add(entity.getSERIES());
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "SYS_POS_POWER更新失败!");
        }
    }

    public void deleteRecordBySeries(Long tenantNumId, Long dataSign, String series) {
        String sql = "delete from SYS_POS_POWER where TENANT_NUM_ID = ? AND DATA_SIGN = ? and series=? ";
        int row = jdbcTemplate.update(sql, tenantNumId, dataSign, series);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "SYS_POS_POWER删除失败!");
        }
    }

    public boolean checkExistByResourceId(String resourceId) {
        Integer count = jdbcTemplate.queryForObject("select count(*) from t_role_resource where resource_id=?", new Object[]{resourceId}, Integer.class);
        if (count.equals(0)) {
            return false;
        }
        return true;
    }

    /**
     * zhangzonglu
     */
    public void deleteRoleResourceByResourceId(String resourceId) {
        String sql = "delete from t_role_resource where resource_id=? ";
        int row = jdbcTemplate.update(sql, resourceId);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "根据resourceId删除t_role_resource数据失败!");
        }
    }

    public void deleteRoleResource(String id) {
        String sql = "delete from t_role_resource where id=? ";
        int row = jdbcTemplate.update(sql, id);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "t_role_resource删除失败!");
        }
    }

    public String getIdBySeries(Long tenantNumId, Long dataSign, String series) {
        String sql = " select id from " + TABLE_NAME + " where tenant_num_id=? and data_sign=? and "
                + " series=? ";
        String id = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, String.class);
        return id;
    }

    /**
     * 查询当前用户拥有的状态权限
     * liushihao
     */
    public List<Long> getImportDtlForItemStatusIdMaintenance(Long tenantNumId, Long dataSign, Long userNumId) {
        String sql = "SELECT pp.id FROM sys_pos_power pp WHERE pp.tenant_num_id = ? AND pp.data_sign = ? AND pp.id IN ( SELECT DISTINCT rr.resource_id FROM t_role_resource rr WHERE rr.role_id IN ( SELECT ro.role_id FROM t_role_org ro WHERE "
                + " ro.org_id IN ( SELECT t.id FROM t_org t WHERE t.user_id IN ( SELECT u1.id FROM t_user u1 INNER JOIN ex_arc_empe e ON u1.login_name = e.work_nums WHERE e.empe_num_id = ? ) OR t.id = ( SELECT org.parent_id FROM t_org org "
                + " WHERE org.user_id IN ( SELECT u2.id FROM t_user u2 INNER JOIN ex_arc_empe e ON u2.login_name = e.work_nums WHERE e.empe_num_id = ? ) ) ) ) ) AND url LIKE '%field:%' " + DaoUtil.LIMIT_STR;
        return jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, userNumId, userNumId}, Long.class);
    }
}
