package com.ykcloud.soa.omp.cmasterdata.dao;


import com.gb.soa.omp.ccommon.util.EntityFieldUtil;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_B2B_BL_CS_POLICY_DTL;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author
 * @date 2021/10/22 23:33
 */
@Repository
public class MdmsB2bCsPolicyDtlDao extends Dao<MDMS_B2B_BL_CS_POLICY_DTL> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_B2B_BL_CS_POLICY_DTL.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_B2B_BL_CS_POLICY_DTL.class, ",");
    private static final String TABLE_NAME = "MDMS_B2B_BL_CS_POLICY_DTL";


    public int deletePolicyDtlByReservedNo(String reserved_no,Long tenantNumId, Long dataSign){
        String sql = "DELETE from MDMS_B2B_BL_CS_POLICY_DTL WHERE  tenant_num_id = ? and data_sign = ? and reserved_no = ? ";
        return jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, reserved_no});
    }


    public List<MDMS_B2B_BL_CS_POLICY_DTL> getMdmsB2bCsPolicyDtlList(Long tenantNumId, Long dataSign, String reserved_no) {
        String sql = "select * from MDMS_B2B_BL_CS_POLICY_DTL where " +
                " tenant_num_id = ? and data_sign = ? and reserved_no = ?";
        return jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, reserved_no}, MDMS_B2B_BL_CS_POLICY_DTL.class);
    }


}
