package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class MDMS_O_SUPPLY extends BaseEntity{


    private String SUPPLY_NUM_ID;

    private String SUPPLY_NAME;

    private Integer PARTNER_ROLE_GROUP;

    private Integer SUPPLY_CLASSIFY;

    private String MNEMONIC_CODE;

    private String FINANCE_NAME;

    private String FINANCE_TEL;

    private String ADR;

    private Integer SUPPLY_TYPE;

    private String BUSINESS_SCOPE;

    private String BUSINESS_PERMIT;

    private String PRODUCTION_SCOPE;

    private String UNIFIED_SOCIAL_CREDIT_CODE;

    private String REGISTER_PLACE;

    private Date REGISTER_DATE;

    private Integer ABC_MARK;

    private String TAX_ACCOUNT;

    private Integer BASIC_BANK_NUM;

    private String BASIC_BANK_NAME;

    private String BASIC_BANK_ACCOUNT;

    private Integer SUPPLY_STATUS;

    private Integer GOVERNMENT_RELATED_SUPPLIER;

    private Integer POTENTIAL_LICHONG_SUPPLIER;

    private String GOVERNMENT_RELATED_SUPPLIER_REMARK;

    private String POTENTIAL_LICHONG_SUPPLIER_REMARK;

    private Double REGISTERED_CAPITAL;

    private Double PAID_IN_CAPITAL;


}
