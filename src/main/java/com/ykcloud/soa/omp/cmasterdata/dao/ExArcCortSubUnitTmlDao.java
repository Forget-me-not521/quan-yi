package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.ykcloud.soa.erp.common.utils.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.entity.EX_ARC_CORT_SUB_UNIT_TML;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

/**
 * @Author stark.jiang
 * @Date 2021/10/13/19:07
 * @Description:
 * @Version 1.0
 */
@Repository
public class ExArcCortSubUnitTmlDao {
    @Resource(name = "masterDataJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(EX_ARC_CORT_SUB_UNIT_TML.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(EX_ARC_CORT_SUB_UNIT_TML.class, ",");
    private static final String TABLE_NAME = "EX_ARC_CORT_SUB_UNIT_TML";

    public List<EX_ARC_CORT_SUB_UNIT_TML> queryEntityListBySubUnitNumId(Long dataSign, Long tenantNumId, Long subUnitNumId){
        String safesql = "SELECT " + //
                "SERIES," + //行号
                "TENANT_NUM_ID," + //租户ID
                "DATA_SIGN," + //0: 正式  1：测试
                "SUB_UNIT_NUM_ID," + //门店编号(子单元编号)
                "TML_CLIENT_ID," + //终端号
                "TML_CLIENT_NAME," + //终端名称
                "TYPE_NUM_ID," + //收银台类型（0 全部,1：非童车，2：童车）
                "ADD_TYPE," + //扫码时添加标识 （0：不累加，1：累加到原记录行）
                "DESCRIPTION," + //POS描述
                "BILL_COUNT," + //制单单量
                "FINISHED_COUNT," + //结单单量
                "CANCEL_COUNT," + //取消单量
                "CURRENT_USER_ID," + //当前用户
                "TML_ADRESS," + //POS机所在地址
                "TML_BARCODE," + //终端设备条码
                "PRINT_SALES_DEVICE_NAME," + //小票打印机
                "PRINT_SALES_DEVICE_TYPE," + //打印机类别 1: 逐行 2：非逐行
                "CUSTOMER_DISPLAY_SIGN," + //客显标识 0: 无客显 1：有客显
                "KEYBOARD_TYPE_NUM_ID," + //键盘种类
                "COMPUTER_NAME," + //电脑名
                "COMPUTER_IP," + //电脑ip
                "COMPUTER_MAC," + //电脑mac地址
                "PRINT_INVOICE_DEVICE_NAME," + //发票打印机
                "PRINT_TRAN_DEVICE_NAME," + //快递打印机
                "DEVICE_CODE," + //终端设备识别码
                "CREATE_DTME," + //终端启用时间
                "LAST_UPDTME," + //更新时间
                "STATUS_NUM_ID," + //设备状态 1:空闲；2：在用；3：维修；4：报废
                "CREATE_USER_ID," + //用户
                "LAST_UPDATE_USER_ID," + //更新用户
                "CANCELSIGN," + //删除
                "TXC " + //分布式事务ID
                "FROM EX_ARC_CORT_SUB_UNIT_TML WHERE DATA_SIGN = "+dataSign+" AND TENANT_NUM_ID = "+tenantNumId+" AND SUB_UNIT_NUM_ID = "+subUnitNumId;
        List<EX_ARC_CORT_SUB_UNIT_TML> exArcCortSubUnitTmls = jdbcTemplate.query(safesql,new Object[]{}, new BeanPropertyRowMapper<EX_ARC_CORT_SUB_UNIT_TML>(EX_ARC_CORT_SUB_UNIT_TML.class));
        if(exArcCortSubUnitTmls == null){
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店:"+subUnitNumId+"未查询到原店数据");
        }
        return exArcCortSubUnitTmls;
    }

    public void insertEntity(EX_ARC_CORT_SUB_UNIT_TML entity){
        String safesql = "INSERT INTO EX_ARC_CORT_SUB_UNIT_TML(" + //
                "SERIES," + //行号
                "TENANT_NUM_ID," + //租户ID
                "DATA_SIGN," + //0: 正式  1：测试
                "SUB_UNIT_NUM_ID," + //门店编号(子单元编号)
                "TML_CLIENT_ID," + //终端号
                "TML_CLIENT_NAME," + //终端名称
                "TYPE_NUM_ID," + //收银台类型（0 全部,1：非童车，2：童车）
                "ADD_TYPE," + //扫码时添加标识 （0：不累加，1：累加到原记录行）
                "DESCRIPTION," + //POS描述
                "BILL_COUNT," + //制单单量
                "FINISHED_COUNT," + //结单单量
                "CANCEL_COUNT," + //取消单量
                "CURRENT_USER_ID," + //当前用户
                "TML_ADRESS," + //POS机所在地址
                "TML_BARCODE," + //终端设备条码
                "PRINT_SALES_DEVICE_NAME," + //小票打印机
                "PRINT_SALES_DEVICE_TYPE," + //打印机类别 1: 逐行 2：非逐行
                "CUSTOMER_DISPLAY_SIGN," + //客显标识 0: 无客显 1：有客显
                "KEYBOARD_TYPE_NUM_ID," + //键盘种类
                "COMPUTER_NAME," + //电脑名
                "COMPUTER_IP," + //电脑ip
                "COMPUTER_MAC," + //电脑mac地址
                "PRINT_INVOICE_DEVICE_NAME," + //发票打印机
                "PRINT_TRAN_DEVICE_NAME," + //快递打印机
                "DEVICE_CODE," + //终端设备识别码
                "CREATE_DTME," + //终端启用时间
                "LAST_UPDTME," + //更新时间
                "STATUS_NUM_ID," + //设备状态 1:空闲；2：在用；3：维修；4：报废
                "CREATE_USER_ID," + //用户
                "LAST_UPDATE_USER_ID," + //更新用户
                "CANCELSIGN," + //删除
                "TXC" + //分布式事务ID
                ") VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        String series = SeqUtil.getSeqNextValue(SeqUtil.EX_ARC_CORT_SUB_UNIT_TML_SERIES);//待配置
        int result = jdbcTemplate.update(safesql, //
                series,//行号
                entity.getTENANT_NUM_ID(),//租户ID
                entity.getDATA_SIGN(),//0: 正式  1：测试
                entity.getSUB_UNIT_NUM_ID(),//门店编号(子单元编号)
                entity.getTML_CLIENT_ID(),//终端号
                entity.getTML_CLIENT_NAME(),//终端名称
                entity.getTYPE_NUM_ID(),//收银台类型（0 全部,1：非童车，2：童车）
                entity.getADD_TYPE(),//扫码时添加标识 （0：不累加，1：累加到原记录行）
                entity.getDESCRIPTION(),//POS描述
                entity.getBILL_COUNT(),//制单单量
                entity.getFINISHED_COUNT(),//结单单量
                entity.getCANCEL_COUNT(),//取消单量
                entity.getCURRENT_USER_ID(),//当前用户
                entity.getTML_ADRESS(),//POS机所在地址
                entity.getTML_BARCODE(),//终端设备条码
                entity.getPRINT_SALES_DEVICE_NAME(),//小票打印机
                entity.getPRINT_SALES_DEVICE_TYPE(),//打印机类别 1: 逐行 2：非逐行
                entity.getCUSTOMER_DISPLAY_SIGN(),//客显标识 0: 无客显 1：有客显
                entity.getKEYBOARD_TYPE_NUM_ID(),//键盘种类
                entity.getCOMPUTER_NAME(),//电脑名
                entity.getCOMPUTER_IP(),//电脑ip
                entity.getCOMPUTER_MAC(),//电脑mac地址
                entity.getPRINT_INVOICE_DEVICE_NAME(),//发票打印机
                entity.getPRINT_TRAN_DEVICE_NAME(),//快递打印机
                entity.getDEVICE_CODE(),//终端设备识别码
                entity.getCREATE_DTME(),//终端启用时间
                entity.getLAST_UPDTME(),//更新时间
                entity.getSTATUS_NUM_ID(),//设备状态 1:空闲；2：在用；3：维修；4：报废
                entity.getCREATE_USER_ID(),//用户
                entity.getLAST_UPDATE_USER_ID(),//更新用户
                entity.getCANCELSIGN()
        );
        if (result <= 0){
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "pos基础数据  mdms_p_bom_shop 拷贝 报错！");
        }
    }


    public void insertExArcCortSubUnitTml(EX_ARC_CORT_SUB_UNIT_TML ex_arc_cort_sub_unit_tml) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i= jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(EX_ARC_CORT_SUB_UNIT_TML.class, ex_arc_cort_sub_unit_tml));
        if(i<=0){
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,"  数据插入失败! ");
        }
    }

    public boolean checkExistBySeries( Long tenantNumId, Long dataSign,String series) {
        return jdbcTemplate.queryForObject("select count(1) from EX_ARC_CORT_SUB_UNIT_TML where tenant_num_id=? and data_sign=? and series=? and cancelsign='N'", new Object[]{tenantNumId,dataSign,series}, Integer.class) > 0;
    }

    public void updateEntity(EX_ARC_CORT_SUB_UNIT_TML entity) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE EX_ARC_CORT_SUB_UNIT_TML SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(entity, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND series = ?");
        List<Object> objects = new ArrayList(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(EX_ARC_CORT_SUB_UNIT_TML.class, entity)));
        objects.add(entity.getDATA_SIGN());
        objects.add(entity.getTENANT_NUM_ID());
        objects.add(entity.getSERIES());
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "EX_ARC_CORT_SUB_UNIT_TML更新失败!");
        }
    }

    public void deleteRecordBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "delete from EX_ARC_CORT_SUB_UNIT_TML where TENANT_NUM_ID = ? AND DATA_SIGN = ? and series=? and cancelsign='N'";
        int row = jdbcTemplate.update(sql,tenantNumId,dataSign,series);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "EX_ARC_CORT_SUB_UNIT_TML删除失败!");
        }
    }
}
