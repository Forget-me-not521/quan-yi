package com.ykcloud.soa.omp.cmasterdata.service.model;

import com.gb.soa.omp.ccommon.api.request.AbstractRequest;

public class TableByMaxMinSeriesSyncRequest extends AbstractRequest {

    private static final long serialVersionUID = 1L;

    private String tableName;

    private Integer typeNumId;

    private String maxSeries;

    private String minSeries;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Integer getTypeNumId() {
        return typeNumId;
    }

    public void setTypeNumId(Integer typeNumId) {
        this.typeNumId = typeNumId;
    }

    public String getMaxSeries() {
        return maxSeries;
    }

    public void setMaxSeries(String maxSeries) {
        this.maxSeries = maxSeries;
    }

    public String getMinSeries() {
        return minSeries;
    }

    public void setMinSeries(String minSeries) {
        this.minSeries = minSeries;
    }

}
