package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.utils.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_M_WAREHOUSE_CONFIG;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @Author stark.jiang
 * @Date 2021/09/18/14:36
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsMWarehouseConfigDao {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_M_WAREHOUSE_CONFIG.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_M_WAREHOUSE_CONFIG.class, ",");
    private static final String TABLE_NAME = "MDMS_M_WAREHOUSE_CONFIG";


    /**
     * 改变replenishFalg状态
     * @param replenishFalg
     */
    public void updateReplenishFalg(Long tenantNumId, Long dataSign, Integer replenishFalg){
        StringBuilder sql = new StringBuilder();
        sql.append("update ");
        sql.append(TABLE_NAME);
        sql.append("  set replenish_falg = ? ");
        sql.append(" where tenant_num_id = ? and data_sign = ? ");
        int row = jdbcTemplate.update(sql.toString(),
                new Object[]{replenishFalg, tenantNumId, dataSign});

        if (row < 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "更新表MDMS_M_WAREHOUSE_CONFIG记录失败!");
        }
    }

}
