package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccache.client.util.CacheUtil;
import com.gb.soa.omp.ccommon.api.exception.BusinessException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.*;
import com.google.common.collect.Lists;
import com.ykcloud.soa.erp.common.enums.BlProcessTypeEnum;
import com.ykcloud.soa.omp.cmasterdata.api.model.*;
import com.ykcloud.soa.omp.cmasterdata.api.request.*;
import com.ykcloud.soa.omp.cmasterdata.api.response.*;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdTenantService;
import com.ykcloud.soa.omp.cmasterdata.dao.*;
import com.ykcloud.soa.omp.cmasterdata.entity.*;
import com.ykcloud.soa.omp.cmasterdata.util.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import java.util.*;
import java.util.stream.Collectors;


//与租户相关服务
@Service("mdTenantService")
@RestController
public class MdTenantServiceImpl implements MdTenantService {
    private static final Logger log = LoggerFactory.getLogger(MdTenantServiceImpl.class);
    @Resource
    MdmsCTypeDao mdmsCTypeDao;
    @Resource
    StringRedisTemplate stringRedisTemplate;

    @Resource
    private MdmsSConfigDao mdmsSConfigDao;

    @Resource(name = "masterDataTransactionManager")
    private PlatformTransactionManager masterDataTransactionManager;
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate masterDataJdbcTemplate;


    @Resource
    private CommoncalltableDao commoncalltableDao;
    @Resource
    private PlatformAutoSequenceDao platformAutoSequenceDao;
    @Resource
    private PlatformSequenceDao platformSequenceDao;
    @Resource
    private CommonQueryDao commonQueryDao;
    @Resource
    private ExArcCortSubUnitTmlDao exArcCortSubUnitTmlDao;
    @Resource
    private MdmsOLicenseResourcesDao mdmsOLicenseResourcesDao;
    @Resource
    private MdmsBlProcessOperateLogDao mdmsBlProcessOperateLogDao;

    @Resource
    private MdmsOLicenseFirstDao mdmsOLicenseFirstDao;

    @Resource
    private MdmsOLicenseSecondDao mdmsOLicenseSecondDao;

    @Resource
    private MdmsOLicenseAllowDao mdmsOLicenseAllowDao;

    @Resource
    private MdmsOBankDao mdmsOBankDao;


    // 根据配置项名称获取配置内容
    @Override
    public ConfigValueGetResponse getConfigValue(ConfigValueGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getConfigValue request:{}", JsonUtil.toJson(request));
        }
        ConfigValueGetResponse response = new ConfigValueGetResponse();
        try {
            try {
                response = CacheUtil.getCache("getConfigValueByConfigName", request, ConfigValueGetResponse.class);
            } catch (BusinessException be) {
                if (be.getCode() == ExceptionType.BE40071.getCode()) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "根据配置名:" + request.getConfigName() + "获取的配置属性为空！");
                } else {
                    throw be;
                }
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getConfigValue response:{}", JsonUtil.toJson(response));
        }
        return response;
    }


    /**
     * 获取seq
     *
     * @author tz.x
     * @date 2018年6月22日下午1:45:51
     * @see MdTenantService#getAutomicSequence(AutomicSequenceGetRequest)
     */
    @Override
    public AutomicSequenceGetResponse getAutomicSequence(AutomicSequenceGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getAutomicSequence request: {}", JsonUtil.toJson(request));
        }

        AutomicSequenceGetResponse response = new AutomicSequenceGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            String sequence = SeqUtil.getSeqAutoNextValue(request.getTenantNumId(), request.getDataSign(), request.getSeriesName());
            response.setSequenceNum(sequence);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }

        if (log.isDebugEnabled()) {
            log.debug("end getAutomicSequence response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    //清除缓存
    @Override
    public CachePrefixDeleteResponse deleteCacheByPrefix(CachePrefixDeleteRequest request) {
        CachePrefixDeleteResponse response = new CachePrefixDeleteResponse();
        if (log.isDebugEnabled()) {
            log.debug("begin deleteCacheByPrefix request: {}", JsonUtil.toJson(request));
        }
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            StringBuffer sb = new StringBuffer();
            if (request.getCachePrefix() == null || request.getCachePrefix() == "") {
                request.setCachePrefix("*");
            }
            sb.append(request.getCachePrefix());
            sb.append("*");
            String key = sb.toString();
            Set<String> keys = stringRedisTemplate.keys(key);
            stringRedisTemplate.delete(keys);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getAutomicSequence response: {}", JsonUtil.toJson(response));
        }
        return response;
    }


    // 获取表栏位的值对应的中文名
    @Override
    public TypeNameByTypeNumIdGetResponse getTypeNameByTypeNumId(TypeNameByTypeNumIdGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getTypeNameByTypeNumId request:{}", JsonUtil.toJson(request));
        }
        TypeNameByTypeNumIdGetResponse response = new TypeNameByTypeNumIdGetResponse();
        try {
            try {
                response = CacheUtil.getCache("getTypeNameByTypeNumId", request, TypeNameByTypeNumIdGetResponse.class);
            } catch (BusinessException be) {
                if (be.getCode() == ExceptionType.BE40071.getCode()) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "根据表名:" + request.getTableName() + ",栏位名:" + request.getFieldName() + ",值名:"
                                    + request.getTypeNumId() + "获取的对应名称失败！");
                } else {
                    throw be;
                }
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getTypeNameByTypeNumId response:{}", JsonUtil.toJson(request));
        }
        return response;
    }






    @Override
    public TypeNumIdsByTableNameAndFieldNameGetResponse getTypeNumIdsByTableNameAndFieldName(
            TypeNumIdsByTableNameAndFieldNameGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getTypeNumIdsByTableNameAndFieldName request: {}", JsonUtil.toJson(request));
        }
        TypeNumIdsByTableNameAndFieldNameGetResponse response = new TypeNumIdsByTableNameAndFieldNameGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<Long> typeNumIdList = mdmsCTypeDao.getTypeNumIdListByTableNameAndFieldName(request.getTenantNumId(),
                    request.getDataSign(), request.getTableName(), request.getFieldName());
            response.setTypeNumIds(typeNumIdList);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getTypeNumIdsByTableNameAndFieldName response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public TypeNumIdsByTableNameAndFieldNameGetResponse getTypeNumIdsByTableNameAndFieldNameAndTypeName(
            TypeNumIdsByTableNameAndFieldNameGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getTypeNumIdsByTableNameAndFieldName request: {}", JsonUtil.toJson(request));
        }
        TypeNumIdsByTableNameAndFieldNameGetResponse response = new TypeNumIdsByTableNameAndFieldNameGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<Long> typeNumIdList = mdmsCTypeDao.getTypeNumIdListByTableNameAndFieldNameAndTypeName(request.getTenantNumId(),
                    request.getDataSign(), request.getTableName(), request.getFieldName(), request.getTypeName());
            response.setTypeNumIds(typeNumIdList);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getTypeNumIdsByTableNameAndFieldName response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    /**
     * 根据typeNumId查对应的TypeName
     *
     * @param request
     * @return
     */
    @Override
    public TypeNamesByTableNameAndFieldNameGetResponse getTypeNamesByTableNameAndFieldNameAndTypeNumId(
            TypeNamesByTableNameAndFieldNameGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getTypeNamesByTableNameAndFieldNameAndTypeNumId request: {}", JsonUtil.toJson(request));
        }
        TypeNamesByTableNameAndFieldNameGetResponse response = new TypeNamesByTableNameAndFieldNameGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<String> typeNameList = mdmsCTypeDao.getTypeNameListByTableNameAndFieldNameAndTypeNumId(request.getTenantNumId(),
                    request.getDataSign(), request.getTableName(), request.getFieldName(), request.getTypeNumId());
            response.setTypeNumNames(typeNameList);
            ;
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getTypeNamesByTableNameAndFieldNameAndTypeNumId response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public TypeInfoByTableNameAndFieldNameGetResponse getTypeInfoByTableNameAndFieldName(TypeInfoByTableNameAndFieldNameGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getTypeInfoByTableNameAndFieldName request:{}", JsonUtil.toJson(request));
        }
        TypeInfoByTableNameAndFieldNameGetResponse response = new TypeInfoByTableNameAndFieldNameGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<TypeInfo> list= mdmsCTypeDao.getTypeInfoList(request.getTenantNumId(),request.getDataSign(),request.getTableName(),request.getFieldName());
            response.setList(list);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getTypeInfoByTableNameAndFieldName response:{}", JsonUtil.toJson(request));
        }
        return response;
    }


    @Override
    public TypeNumIdListByTableNameAndFieldNameAndTypeNumIdGetResponse getTypeNumIdListByTableNameAndFieldNameAndTypeNumId(
            TypeNumIdListByTableNameAndFieldNameAndTypeNumIdGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getTypeNumIdListByTableNameAndFieldNameAndTypeNumId request: {}", JsonUtil.toJson(request));
        }
        TypeNumIdListByTableNameAndFieldNameAndTypeNumIdGetResponse response = new TypeNumIdListByTableNameAndFieldNameAndTypeNumIdGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<Long> typeNumIdList = mdmsCTypeDao.getTypeNumIdListByTableNameAndFieldNameAndTypeNumId(request.getTenantNumId(),
                    request.getDataSign(), request.getTableName(), request.getFieldName(), request.getTypeNumId());
            response.setTypeNumIds(typeNumIdList);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getTypeNumIdListByTableNameAndFieldNameAndTypeNumId response: {}", JsonUtil.toJson(response));
        }
        return response;
    }







    @Override
    public TypeNumIdsByTableNameAndFieldNameGetResponse getSubUnitTypeByTableNameAndFieldName(
            TypeNumIdsByTableNameAndFieldNameGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getSubUnitNumIdsByTableNameAndFieldName request: {}", JsonUtil.toJson(request));
        }
        TypeNumIdsByTableNameAndFieldNameGetResponse response = new TypeNumIdsByTableNameAndFieldNameGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<Long> typeNumIdList = mdmsCTypeDao.getSubUnitTypeByTableNameAndFieldName(request.getTenantNumId(),
                    request.getDataSign(), request.getTableName(), request.getFieldName());
            response.setTypeNumIds(typeNumIdList);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getSubUnitNumIdsByTableNameAndFieldName response: {}", JsonUtil.toJson(response));
        }
        return response;
    }


    /*
     *新增系统参数
     *@Author 曾傲
     *@Time   2019-3-20
     *@param request
     *@return
     */
    @Override
    public MdmsSConfigSaveResponse saveMdmsSConfig(MdmsSConfigSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveMdmsSConfig request: {}", JsonUtil.toJson(request));
        }
        MdmsSConfigSaveResponse response = new MdmsSConfigSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            MDMS_S_CONFIG mdms_s_config = new MDMS_S_CONFIG();
            mdms_s_config.setCONFIG_NAME(request.getConfigName());
            mdms_s_config.setCONFIG_VALUE(request.getConfigValue());
            mdms_s_config.setCONFIG_DESCRIPTION(request.getConfigDescription());
            mdms_s_config.setREF_ONE(request.getRefOne());
            mdms_s_config.setTENANT_NUM_ID(tenantNumId);
            mdms_s_config.setDATA_SIGN(dataSign);
            if (!ValidatorUtils.isNullOrZero(request.getSeries())) {
                if (mdmsSConfigDao.checkExistBySeries(tenantNumId, dataSign, request.getSeries() + "")) {
                    mdms_s_config.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdms_s_config.setSERIES(request.getSeries() + "");
                    mdmsSConfigDao.updateConfig(mdms_s_config);
                }
            } else {
                if (mdmsSConfigDao.checkExistByConfigName(tenantNumId, dataSign, request.getConfigName(), request.getConfigValue())) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "改配置信息已存在! 参数名称：" + request.getConfigName() + ",参数值:" + request.getConfigValue());
                } else {
                    mdms_s_config.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MMDMS_S_CONFIG_SERIES));
                    mdms_s_config.setCREATE_USER_ID(request.getUserNumId());
                    mdms_s_config.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdmsSConfigDao.insertMdmsSConfig(mdms_s_config);
                }
            }

        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveMdmsSConfig response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    /*
     *删除系统参数
     *@Author 曾傲
     *@Time   2019-3-20
     *@param request
     *@return
     */
    @Override
    public MdmsSConfigDeleteResponse deleteMdmsSConfig(MdmsSConfigDeleteRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteMdmsSConfig request: {}", JsonUtil.toJson(request));
        }
        MdmsSConfigDeleteResponse response = new MdmsSConfigDeleteResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            List<Long> seriess = request.getSeriess();
            for (Long series : seriess) {
                boolean checkExist = mdmsSConfigDao.checkExistBySeries(tenantNumId, dataSign, series + "");
                if (checkExist) {
                    mdmsSConfigDao.deleteRecordBySeries(tenantNumId, dataSign, series + "");
                }
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteMdmsSConfig response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    /*
     *新增枚举字典
     *@Author 曾傲
     *@Time   2019-3-20
     *@param request
     *@return
     */
    @Override
    public MdmsCTypeSaveResponse saveMdmsCType(MdmsCTypeSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveMdmsCType request: {}", JsonUtil.toJson(request));
        }
        MdmsCTypeSaveResponse response = new MdmsCTypeSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            MDMS_C_TYPE mdms_c_type = new MDMS_C_TYPE();
            mdms_c_type.setTABLE_NAME(request.getTableName());
            mdms_c_type.setFIELD_NAME(request.getFieldName());
            mdms_c_type.setTYPE_NUM_ID(request.getTypeNumId());
            mdms_c_type.setTYPE_NAME(request.getTypeName());
            mdms_c_type.setNOTES(request.getNotes());
            mdms_c_type.setSTATUS_NUM_ID(request.getStatusNumId());
            mdms_c_type.setTENANT_NUM_ID(tenantNumId);
            mdms_c_type.setDATA_SIGN(dataSign);
            String series = request.getSeries();
            boolean checkExist = mdmsCTypeDao.checkExistByTableNameAndFieldNameAndTypeNumId(tenantNumId, dataSign, request.getTableName(), request.getFieldName(), request.getTypeNumId());
            if (checkExist) {
                mdms_c_type.setSERIES(series);
                mdms_c_type.setLAST_UPDTME(new Date());
                mdms_c_type.setLAST_UPDATE_USER_ID(request.getUserNumId());
                mdmsCTypeDao.updateEntity(mdms_c_type);
            } else {
                series = SeqUtil.getSeqNextValue(SeqUtil.MDMS_C_TYPE_SERIES);
                mdms_c_type.setSERIES(series);
                mdms_c_type.setCREATE_DTME(new Date());
                mdms_c_type.setLAST_UPDTME(new Date());
                mdms_c_type.setCREATE_USER_ID(request.getUserNumId());
                mdms_c_type.setLAST_UPDATE_USER_ID(request.getUserNumId());
                mdmsCTypeDao.insertMdmsCType(mdms_c_type);
            }
            response.setSeries(series);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveMdmsCType response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    /*
     *删除枚举字典
     *@Author 曾傲
     *@Time   2019-3-20
     *@param request
     *@return
     */
    @Override
    public MdmsCTypeDeleteResponse deleteMdmsCType(MdmsCTypeDeleteRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteMdmsCType request: {}", JsonUtil.toJson(request));
        }
        MdmsCTypeDeleteResponse response = new MdmsCTypeDeleteResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            List<Long> seriess = request.getSeriesList();
            for (Long series : seriess) {
                boolean checkExist = mdmsCTypeDao.checkExistBySeries(tenantNumId, dataSign, series);
                if (checkExist) {
                    mdmsCTypeDao.deleteRecordBySeries(tenantNumId, dataSign, series);
                }
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteMdmsCType response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public CommoncalltableSaveResponse saveCommoncalltable(CommoncalltableSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveCommoncalltable request: {}", JsonUtil.toJson(request));
        }
        ArrayList<String> exceptionMessageList = new ArrayList<String>();
        CommoncalltableSaveResponse response = new CommoncalltableSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);

            String series = request.getSeries();
            if (series == null || "".equals(series) || "0".equals(series)) {
                if (commoncalltableDao.checkExistCmd(request.getCmd())) {
                    exceptionMessageList.add("cmd已存在");
                }
                if (commoncalltableDao.checkExistFuncname(request.getFuncname())) {
                    exceptionMessageList.add("funcname已存在");
                }
                if (commoncalltableDao.checkExistBeanidMethod(request.getBeanid(), request.getMethod())) {
                    exceptionMessageList.add("beanid+method已存在");
                }
                if (CollectionUtils.isNotEmpty(exceptionMessageList)) {
                    String exceptionMessage = String.join(",", exceptionMessageList);
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, exceptionMessage);
                }
                series = SeqUtil.getSeqNextValue(SeqUtil.COMMONCALLTABLE_SERIES);
                if ("".equals(request.getCmd()) || request.getCmd() == null)
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "cmd不能为空");
                if ("".equals(request.getFuncname()) || request.getFuncname() == null)
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "Funcname不能为空");
                int insert = commoncalltableDao.insertCommoncalltableNew(request.getCmd(), request.getFuncname(),
                        request.getBeanid(), request.getMethod(), request.getRequest_sample(), request.getRemark(), series);
                if (insert <= 0) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "insertCommoncalltableNew插入不成功");
                }
            } else {
                if (commoncalltableDao.checkExistSeries(series)) {
                    COMMONCALLTABLE commoncall = commoncalltableDao.findCommoncallBySeries(series);
                    if (!("".equals(request.getCmd()) || request.getCmd() == null))
                        commoncall.setCMD(request.getCmd());
                    if (!("".equals(request.getFuncname()) || request.getFuncname() == null))
                        commoncall.setFUNCNAME(request.getFuncname());
                    if (!("".equals(request.getBeanid()) || request.getBeanid() == null))
                        commoncall.setBEANID(request.getBeanid());
                    if (!("".equals(request.getMethod()) || request.getMethod() == null))
                        commoncall.setMETHOD(request.getMethod());
                    if (!("".equals(request.getRequest_sample()) || request.getRequest_sample() == null))
                        commoncall.setREQUEST_SAMPLE(request.getRequest_sample());
                    int update = commoncalltableDao.updateCommoncalltable(commoncall.getCMD(), commoncall.getFUNCNAME(),
                            commoncall.getBEANID(), commoncall.getMETHOD(), commoncall.getREQUEST_SAMPLE(), request.getRemark(),
                            request.getSeries());
                    if (update <= 0) {
                        throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                                "insertCommoncalltableNew更新不成功");
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveCommoncalltable  response: {}", JsonUtil.toJson(response));
        }
        return response;
    }


    @Override
    public PlatformAutoSequenceSaveResponse savePlatformAutoSequence(PlatformAutoSequenceSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin savePlatformAutoSequence request: {}", JsonUtil.toJson(request));
        }
        PlatformAutoSequenceSaveResponse response = new PlatformAutoSequenceSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            String series = request.getSeries();
            if (series == null || "".equals(series) || "0".equals(series)) {
                if (platformAutoSequenceDao.checkExistSeqName(request.getSeqName(), request.getTenantNumId(),
                        request.getDataSign())) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "输入的SeqName已存在");

                }
                series = SeqUtil.getSeqNextValue(SeqUtil.PLATFORM_AUTO_SEQUENCE_SERIES);
                int insert = platformAutoSequenceDao.insertPlatformAutoSequenceNew(request.getSeqName(),
                        request.getSeqProject(), request.getSeqPrefix(), request.getCurrentNum(), request.getRemark(),
                        request.getInitValue(), request.getCacheNum(), series, request.getTenantNumId(),
                        request.getDataSign());
                if (insert <= 0) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "insertPlatformAutoSequenceNew插入不成功");
                }
            } else {
                if (platformAutoSequenceDao.checkExistSeries(series, request.getTenantNumId(), request.getDataSign())) {

                    PLATFORM_AUTO_SEQUENCE platformAutoSequence = platformAutoSequenceDao
                            .findPlatformAutoSequenceBySeries(series, request.getTenantNumId(), request.getDataSign());
					/*
					 * platformAutoSequence.setSEQ_NAME(request.getSeqName());
					 * platformAutoSequence.setSEQ_PROJECT(request.getSeqProject()); if
					 * (!("".equals(request.getSeqPrefix()) || request.getSeqPrefix() == null))
					 * platformAutoSequence.setSEQ_PREFIX(request.getSeqPrefix()); if
					 * (!(request.getInitValue() == null))
					 * platformAutoSequence.setINIT_VALUE(request.getInitValue());
					 * int update = platformAutoSequenceDao.updatePlatformAutoSequence(platformAutoSequence.getSEQ_NAME(),
							platformAutoSequence.getSEQ_PROJECT(), platformAutoSequence.getSEQ_PREFIX(),
							platformAutoSequence.getCURRENT_NUM(), platformAutoSequence.getREMARK(),
							platformAutoSequence.getINIT_VALUE(), platformAutoSequence.getCACHE_NUM(),
							platformAutoSequence.getSERIES(), platformAutoSequence.getTENANT_NUM_ID(),
							platformAutoSequence.getDATA_SIGN());
					 */
                    platformAutoSequence.setCURRENT_NUM(request.getCurrentNum());
                    if (!("".equals(request.getRemark()) || request.getRemark() == null))
                        platformAutoSequence.setREMARK(request.getRemark());
                    if (!(request.getCacheNum() == null))
                        platformAutoSequence.setCACHE_NUM(request.getCacheNum());
                    int update = platformAutoSequenceDao.updatePlatformAutoSequence(platformAutoSequence.getCURRENT_NUM(),
                            platformAutoSequence.getREMARK(), platformAutoSequence.getCACHE_NUM(),
                            platformAutoSequence.getSERIES(), platformAutoSequence.getTENANT_NUM_ID(),
                            platformAutoSequence.getDATA_SIGN());

                    if (update <= 0) {
                        throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                                "updatePlatformAutoSequence更新不成功");
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end savePlatformAutoSequence  response: {}", JsonUtil.toJson(response));
        }
        return response;
    }


    @Override
    public PlatformSequenceSaveResponse savePlatformSequence(PlatformSequenceSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin savePlatformSequence request: {}", JsonUtil.toJson(request));
        }
        PlatformSequenceSaveResponse response = new PlatformSequenceSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);

            String series = request.getSeries();
            if (series == null || "".equals(series) || "0".equals(series)) {
                if (platformSequenceDao.checkExistSeqName(request.getSeqName())) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "输入的SeqName已存在");
                }
                series = SeqUtil.getSeqNextValue(SeqUtil.PLATFORM_SEQUENCE_SERIES);
                PLATFORM_SEQUENCE platformSequence = new PLATFORM_SEQUENCE();
                platformSequence.setSERIES(series);
                platformSequence.setSEQ_VAL("9999");
                platformSequence.setSEQ_PROJECT(request.getSeqProject());
                platformSequence.setSEQ_PREFIX("");
                platformSequence.setSEQ_NUM_START(request.getSeqNumStart());
                platformSequence.setSEQ_NUM_END(request.getSeqNumEnd());
                platformSequence.setSEQ_NUM("999");
                platformSequence.setSEQ_NAME(request.getSeqName());
                platformSequence.setCURRENT_NUM(request.getCurrentNum());
                platformSequence.setCREATE_TIME(new Date());
                int insert = platformSequenceDao.insertPlatformSequenceNew(platformSequence);
                if (insert <= 0) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "insertPlatformSequenceNew插入不成功");
                }
            } else {
                if (platformSequenceDao.checkExistSeries(series)) {

                    PLATFORM_SEQUENCE platformSequence = platformSequenceDao
                            .findPlatformSequenceBySeries(series);
					 /*
					  `SEQ_NUM` varchar(10) NOT NULL COMMENT '序列number',
					  `SEQ_VAL` varchar(10) NOT NULL COMMENT '序列value',
					  `CURRENT_NUM` bigint(10) NOT NULL COMMENT '当前序列',
					  `SEQ_NUM_START` bigint(10) DEFAULT NULL COMMENT 'CURRENT_NUM开始值',
					  `SEQ_NUM_END` bigint(10) DEFAULT NULL COMMENT 'CURRENT_NUM结束值',*/
                    platformSequence.setCURRENT_NUM(request.getCurrentNum());
                    platformSequence.setSEQ_PROJECT(request.getSeqProject());


                    int update = platformSequenceDao.updatePlatformSequence(platformSequence.getSEQ_NUM(),
                            platformSequence.getSEQ_VAL(), platformSequence.getCURRENT_NUM(),
                            platformSequence.getSEQ_NUM_START(), platformSequence.getSEQ_NUM_END(), platformSequence.getSEQ_PROJECT(), series);
                    if (update <= 0) {
                        throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                                "updatePlatformSequence更新不成功");
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end savePlatformSequence  response: {}", JsonUtil.toJson(response));
        }
        return response;

    }




    @Override
    public CommonQuerySaveResponse saveCommonQuery(CommonQuerySaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveCommonQuery request: {}", JsonUtil.toJson(request));
        }
        CommonQuerySaveResponse response = new CommonQuerySaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            String series = request.getSeries();
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            String sqlId = request.getSqlId();
            String sqlName = request.getSqlName();
            COMMON_QUERY query = buildCommonQuery(request);
            if (series == null || "".equals(series) || "0".equals(series)) {
                if (commonQueryDao.checkExistsqlId(sqlId, tenantNumId, dataSign) != null) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "sql编号已存在");
                }
                if (commonQueryDao.checkExistsqlName(sqlName, tenantNumId, dataSign) != null) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "sql执行描述已存在");
                }
                series = SeqUtil.getSeqNextValue(SeqUtil.COMMON_QUERY_SERIES);
                query.setSERIES(series);
                commonQueryDao.insertCommonQuery(query);
                response.setSeries(series);
            } else {
                COMMON_QUERY oldQuery = commonQueryDao.checkExistQueryBySeries(series, tenantNumId, dataSign);

                if (oldQuery != null) {
                    if (!oldQuery.getSQL_ID().equals(sqlId)) {
                        if (commonQueryDao.checkExistsqlId(sqlId, tenantNumId, dataSign) != null) {
                            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                                    "sql编号已存在");
                        }
                    }
                    if (!oldQuery.getSQL_NAME().equals(sqlName)) {
                        if (commonQueryDao.checkExistsqlName(sqlName, tenantNumId, dataSign) != null) {
                            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                                    "sql执行描述已存在");
                        }
                    }
                    int update = commonQueryDao.updateQueryDao(request.getSqlName(),
                            request.getSqlId(), request.getSqlContent(), request.getParamContent(),
                            request.getJdbcName(), request.getDbType(), request.getAnnotatePrefix(), request.getSubSqlId(),
                            request.getNoDataException(), request.getMethodName(), request.getCacheLiveTime(),
                            request.getReturnHandleContent(), request.getDbleOpTime(), request.getRemark()
                            , request.getUserNumId(), series, tenantNumId, dataSign);
                    if (update <= 0) {
                        throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                                "updateCommonQuery更新不成功");
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveCommonQuery  response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    private COMMON_QUERY buildCommonQuery(CommonQuerySaveRequest request) {
        COMMON_QUERY entity = new COMMON_QUERY();
        entity.set_DBLE_OP_TIME(request.getDbleOpTime());
        entity.setANNOTATE_PREFIX(request.getAnnotatePrefix());
        entity.setCACHE_LIVE_TIME(request.getCacheLiveTime());
        entity.setCACHE_SIGN(request.getCacheSign());
        entity.setCANCEL_SIGN("N");
        entity.setCANCELSIGN("N");
        entity.setCREATE_DTME(new Date());
        entity.setCREATE_USER_ID(request.getUserNumId());
        entity.setDATA_SIGN(request.getDataSign());
        entity.setDB_TYPE(request.getDbType());
        entity.setJDBC_NAME(request.getJdbcName());
        entity.setLAST_UPDATE_USER_ID(request.getUserNumId());
        entity.setLAST_UPDTME(new Date());
        entity.setMETHOD_NAME(request.getMethodName());
        entity.setNO_DATA_EXCEPTION(request.getNoDataException());
        entity.setPARAM_CONTENT(request.getParamContent());
        entity.setREMARK(request.getRemark());
        entity.setRETURN_HANDLE_CONTENT(request.getReturnHandleContent());
        entity.setSQL_CONTENT(request.getSqlContent());
        entity.setSQL_ID(request.getSqlId());
        entity.setSQL_NAME(request.getSqlName());
        entity.setSUB_SQL_ID(request.getSubSqlId());
        entity.setTENANT_NUM_ID(request.getTenantNumId());
        return entity;
    }






    @Override
    public CommonQueryDeleteResponse deleteCommonQuery(CommonQueryDeleteRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteCommonQuery request: {}", JsonUtil.toJson(request));
        }
        CommonQueryDeleteResponse response = new CommonQueryDeleteResponse();

        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<String> seriesList = request.getSeriesList();
            Long tenantNumId = request.getTenantNumId();
            Long userNumId = request.getUserNumId();
            Long dataSign = request.getDataSign();
            for (String series : seriesList) {
                commonQueryDao.deleteCommonQueryByseries(userNumId, series, tenantNumId, dataSign);
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteCommonQuery  response: {}", JsonUtil.toJson(response));
        }
        return response;
    }



    @Override
    public BlProcessOperateLogSaveResponse saveBlProcessOperateLog(BlProcessOperateLogSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveBlProcessOperateLog request:{}", JsonUtil.toJson(request));
        }
        BlProcessOperateLogSaveResponse response = new BlProcessOperateLogSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            if(CollectionUtils.isNotEmpty(request.getLogList())){
                List<MDMS_BL_PROCESS_OPERATE_LOG> entityList=Lists.newArrayList();
                for(ProcessOperateLog lo : request.getLogList()){
                    MDMS_BL_PROCESS_OPERATE_LOG entity=createProcessOperateLog(tenantNumId,dataSign,usrNumId,lo.getCortNumId(),lo.getReservedNo(),lo.getTypeNumId(), lo.getOperateContent(),lo.getSourceType(),lo.getBusinessId());
                    entityList.add(entity);
                }
                if(CollectionUtils.isNotEmpty(entityList)){
                    mdmsBlProcessOperateLogDao.batchInsert(entityList);
                }
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveBlProcessOperateLog response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public BlProcessOperateLogGetResponse getBlProcessOperateLog(BlProcessOperateLogGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getBlProcessOperateLog request:{}", JsonUtil.toJson(request));
        }
        BlProcessOperateLogGetResponse response = new BlProcessOperateLogGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<MdmsBlProcessOperateLog> list=mdmsBlProcessOperateLogDao.getByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getReservedNo(),request.getSourceType(),request.getBusinessType(),request.getBillType(),request.getPageNum(),request.getPageSize());
            Integer count=mdmsBlProcessOperateLogDao.getOperateLogCount(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getReservedNo(),request.getSourceType(),request.getBusinessType(),request.getBillType());
            if(CollectionUtils.isNotEmpty(list)){
                for(MdmsBlProcessOperateLog log : list){
                    String billTypeName=BlProcessTypeEnum.getBillTypeNameByBillType(log.getBillType());
                    log.setBillTypeName(billTypeName);
                }
            }
            response.setResults(list);
            response.setRecordCount(count);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveBlProcessOperateLog response:{}", JsonUtil.toJson(response));
        }
        return response;
    }


    @Override
    public LicenseResSaveResponse saveLicenseRes(LicenseResSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveLicenseRes request:{}", JsonUtil.toJson(request));
        }
        LicenseResSaveResponse response = new LicenseResSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            List<LicenseResourceDetails> licenseList=request.getResourcesInfoList();
            if(CollectionUtils.isNotEmpty(licenseList)){
                List<MDMS_O_LICENSE_RESOURCES> licenseEntityList=createLicenseEntity(tenantNumId,dataSign,usrNumId,request.getRelationId(), request.getBusinessType(),request.getCortNumId(),licenseList);
                if(mdmsOLicenseResourcesDao.checkLicenseExistByUnit(tenantNumId,dataSign,request.getRelationId())){
                    mdmsOLicenseResourcesDao.deleteLicenseByUnitId(tenantNumId,dataSign,request.getRelationId());
                }
                mdmsOLicenseResourcesDao.batchInsert(licenseEntityList);
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveLicenseRes response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ExArcCortSubUnitTmlSaveResponse saveExArcCortSubUnitTml(ExArcCortSubUnitTmlSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveExArcCortSubUnitTml request: {}", JsonUtil.toJson(request));
        }
        ExArcCortSubUnitTmlSaveResponse response = new ExArcCortSubUnitTmlSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            EX_ARC_CORT_SUB_UNIT_TML entity=new EX_ARC_CORT_SUB_UNIT_TML();
            entity.setSERIES(request.getSeries()+"");
            entity.setSUB_UNIT_NUM_ID(request.getSubUnitNumId());
            entity.setADD_TYPE(request.getAddType()+"");
            entity.setTML_CLIENT_ID(request.getTmlClientId());
            entity.setTML_CLIENT_NAME(request.getTmlClientName());
            entity.setTYPE_NUM_ID(request.getTypeNumId());
            entity.setDESCRIPTION(request.getDescription());
            entity.setBILL_COUNT(request.getBillCount());
            entity.setFINISHED_COUNT(request.getFinishedCount());
            entity.setCANCEL_COUNT(request.getCancelCount());
            entity.setCURRENT_USER_ID(request.getCurrentUserId());
            entity.setTML_ADRESS(request.getTmlAdress());
            entity.setTML_BARCODE(request.getTmlBarcode());
            entity.setPRINT_SALES_DEVICE_NAME(request.getPrintSalesDeviceName());
            entity.setPRINT_SALES_DEVICE_TYPE(request.getPrintSalesDeviceType());
            entity.setCUSTOMER_DISPLAY_SIGN(request.getCustomerDisplaySign());
            entity.setKEYBOARD_TYPE_NUM_ID(request.getKeyboardTypeNumId());
            entity.setCOMPUTER_NAME(request.getComputerName());
            entity.setCOMPUTER_IP(request.getComputerIp());
            entity.setCOMPUTER_MAC(request.getComputerMac());
            entity.setPRINT_INVOICE_DEVICE_NAME(request.getPrintInvoiceDeviceName());
            entity.setPRINT_TRAN_DEVICE_NAME(request.getPrintTranDeviceName());
            entity.setDEVICE_CODE(request.getDeviceCode());
            entity.setLAST_UPDTME(new Date());
            entity.setLAST_UPDATE_USER_ID(request.getUserNumId());
            entity.setTENANT_NUM_ID(tenantNumId);
            entity.setDATA_SIGN(dataSign);
            boolean checkExist = exArcCortSubUnitTmlDao.checkExistBySeries(tenantNumId,dataSign,request.getSeries());
            if (checkExist){
                exArcCortSubUnitTmlDao.updateEntity(entity);
            }else {
                entity.setCREATE_DTME(new Date());
                entity.setCREATE_USER_ID(request.getUserNumId());
                exArcCortSubUnitTmlDao.insertExArcCortSubUnitTml(entity);
            }
        }catch (Exception ex){
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveExArcCortSubUnitTml response: {}", JsonUtil.toJson(response));
        }
        return response;
    }


    @Override
    public LicenseFirstSaveResponse saveLicenseFirst(LicenseFirstSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveLicenseFirst request:{}", JsonUtil.toJson(request));
        }
        LicenseFirstSaveResponse response = new LicenseFirstSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            MDMS_O_LICENSE_FIRST oldEntity = null;
            if (null != request.getSeries()) {
                oldEntity = mdmsOLicenseFirstDao.getBySeries(tenantNumId, dataSign.intValue(), Long.valueOf(request.getSeries()));
            }
            MDMS_O_LICENSE_FIRST mdmsOLicenseFirst = createMdmsOLicenseFirst(request);
            if (null != oldEntity) {
                mdmsOLicenseFirst.setSERIES(oldEntity.getSERIES());
                mdmsOLicenseFirst.setFirstNumId(oldEntity.getFirstNumId());
                mdmsOLicenseFirst.setCREATE_DTME(oldEntity.getCREATE_DTME());
                mdmsOLicenseFirst.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                mdmsOLicenseFirstDao.update(mdmsOLicenseFirst, tenantNumId, dataSign, oldEntity.getSERIES());
            } else {
                mdmsOLicenseFirstDao.insert(mdmsOLicenseFirst);
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveLicenseFirst response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public LicenseFirstDeteleResponse deleteLicenseFirst(LicenseFirstDeleteRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteLicenseFirst request:{}", JsonUtil.toJson(request));
        }
        LicenseFirstDeteleResponse response = new LicenseFirstDeteleResponse();
        try {
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            if (CollectionUtils.isEmpty(request.getSeriesList())) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "参数错误[缺少行号]");
            }
            mdmsOLicenseFirstDao.updateBySeries(tenantNumId, dataSign.intValue(), request.getSeriesList());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteLicenseFirst response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public LicenseSecondSaveResponse saveLicenseSecond(LicenseSecondSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveLicenseSecond request:{}", JsonUtil.toJson(request));
        }
        LicenseSecondSaveResponse response = new LicenseSecondSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            MDMS_O_LICENSE_SECOND oldEntity = null;
            if (null != request.getSeries()) {
                oldEntity = mdmsOLicenseSecondDao.getBySeries(tenantNumId, dataSign.intValue(), Long.valueOf(request.getSeries()));
            }
            MDMS_O_LICENSE_SECOND mdmsOLicenseSecond = createMdmsOLicenseSecond(request);
            if (null != oldEntity) {
                mdmsOLicenseSecond.setSERIES(oldEntity.getSERIES());
                mdmsOLicenseSecond.setSecondNumId(oldEntity.getFirstNumId());
                mdmsOLicenseSecond.setCREATE_DTME(oldEntity.getCREATE_DTME());
                mdmsOLicenseSecond.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                mdmsOLicenseSecondDao.update(mdmsOLicenseSecond, tenantNumId, dataSign, oldEntity.getSERIES());
            } else {
                mdmsOLicenseSecondDao.insert(mdmsOLicenseSecond);
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveLicenseSecond response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public LicenseSecondDeteleResponse deleteLicenseSecond(LicenseSecondDeleteRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteLicenseSecond request:{}", JsonUtil.toJson(request));
        }
        LicenseSecondDeteleResponse response = new LicenseSecondDeteleResponse();
        try {
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            if (CollectionUtils.isEmpty(request.getSeriesList())) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "参数错误[缺少行号]");
            }
            mdmsOLicenseSecondDao.updateBySeries(tenantNumId, dataSign.intValue(), request.getSeriesList());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteLicenseSecond response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public LicenseAllowSaveResponse saveLicenseAllow(LicenseAllowSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveLicenseAllow request:{}", JsonUtil.toJson(request));
        }
        LicenseAllowSaveResponse response = new LicenseAllowSaveResponse();
        try {
            if (StringUtils.isBlank(request.getSecondNumId())) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "参数错误[缺少二级类型编号]");
            }
            if (CollectionUtils.isEmpty(request.getAllowNumIds())) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "参数错误[缺少许可id]");
            }
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            List<MDMS_O_LICENSE_ALLOW> allowList = new ArrayList<>();
            MDMS_O_LICENSE_ALLOW licenseAllow = null;
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<MDMS_O_LICENSE_ALLOW> oldList = mdmsOLicenseAllowDao.getBySecondNumId(tenantNumId, dataSign, request.getSecondNumId());
            if (CollectionUtils.isNotEmpty(oldList)) {
                List<String> seriesList = oldList.stream().map(e -> e.getSERIES()).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(seriesList)) {
                    mdmsOLicenseAllowDao.updateBySeries(tenantNumId, dataSign, seriesList);
                }
            }
            for (String allowNumId: request.getAllowNumIds()) {
                licenseAllow = createMdmsOLicenseAllow(request, allowNumId);
                allowList.add(licenseAllow);
            }
            if (CollectionUtils.isNotEmpty(allowList)) {
                mdmsOLicenseAllowDao.batchInsert(allowList);
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveLicenseAllow response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public BankPageResponse pageBank(BankPageRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin pageBank request:{}", JsonUtil.toJson(request));
        }
        BankPageResponse response = new BankPageResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<MdmsOBank> list=mdmsOBankDao.getPage(request.getTenantNumId(), request.getDataSign(), request.getBankNumId(), request.getBankName(),request.getPageNum(),request.getPageSize());
            Integer count=mdmsOBankDao.getCount(request.getTenantNumId(), request.getDataSign(), request.getBankNumId(), request.getBankName());
            response.setResults(list);
            response.setRecordCount(count);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end pageBank response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public LicenseSecondGetResponse getLicenseSecond(LicenseSecondGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getLicenseSecond request:{}", JsonUtil.toJson(request));
        }
        LicenseSecondGetResponse response = new LicenseSecondGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            List<MdmsOLicenseSecond> list = new ArrayList<>();
            List<MDMS_O_LICENSE_FIRST> firstList = mdmsOLicenseFirstDao.getByLicenseNumId(request.getTenantNumId(), request.getDataSign(), request.getLicenseNumId());
            if (CollectionUtils.isNotEmpty(firstList)) {
                List<String> numIds = firstList.stream().map(e -> e.getFirstNumId()).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(numIds)) {
                    list = mdmsOLicenseSecondDao.getByFirstNumIds(request.getTenantNumId(), request.getDataSign(), numIds);
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                List<MDMS_O_LICENSE_ALLOW> allowList = new ArrayList<>();
                for (MdmsOLicenseSecond second: list) {
                    allowList = mdmsOLicenseAllowDao.getBySecondNumId(request.getTenantNumId(), request.getDataSign(), second.getSecondNumId());
                    if (CollectionUtils.isNotEmpty(allowList)) {
                        second.setSaleAllowList(allowList.stream().map(e -> e.getAllowNumId()).collect(Collectors.toList()));
                    }
                }
            }
            response.setResults(list);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getLicenseSecond response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    private MDMS_O_LICENSE_ALLOW createMdmsOLicenseAllow(LicenseAllowSaveRequest request, String allowNumId) {
        MDMS_O_LICENSE_ALLOW en = new MDMS_O_LICENSE_ALLOW();
        en.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_LICENSE_ALLOW_SERIES));
        en.setTENANT_NUM_ID(request.getTenantNumId());
        en.setDATA_SIGN(request.getDataSign());
        en.setSecondNumId(request.getSecondNumId());
        en.setAllowNumId(allowNumId);
        en.setCREATE_DTME(new Date());
        en.setLAST_UPDTME(new Date());
        en.setCREATE_USER_ID(request.getUserNumId());
        en.setLAST_UPDATE_USER_ID(request.getUserNumId());
        en.setCANCELSIGN("N");
        return en;
    }

    private MDMS_O_LICENSE_SECOND createMdmsOLicenseSecond(LicenseSecondSaveRequest request) {
        MDMS_O_LICENSE_SECOND en = new MDMS_O_LICENSE_SECOND();
        en.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_LICENSE_SECOND_SERIES));
        en.setTENANT_NUM_ID(request.getTenantNumId());
        en.setDATA_SIGN(request.getDataSign());
        en.setFirstNumId(request.getFirstNumId());
        en.setSecondNumId(getSecondNumId(request.getTenantNumId(), request.getDataSign(), request.getFirstNumId()));
        en.setSecondName(request.getSecondName());
        en.setCREATE_DTME(new Date());
        en.setLAST_UPDTME(new Date());
        en.setCREATE_USER_ID(request.getUserNumId());
        en.setLAST_UPDATE_USER_ID(request.getUserNumId());
        en.setCANCELSIGN("N");
        return en;
    }

    private MDMS_O_LICENSE_FIRST createMdmsOLicenseFirst(LicenseFirstSaveRequest request) {
        MDMS_O_LICENSE_FIRST en = new MDMS_O_LICENSE_FIRST();
        en.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_LICENSE_FIRST_SERIES));
        en.setTENANT_NUM_ID(request.getTenantNumId());
        en.setDATA_SIGN(request.getDataSign());
        en.setLicenseNumId(request.getLicenseNumId());
        en.setFirstNumId(getFirstNumId(request.getTenantNumId(), request.getDataSign()));
        en.setFirstName(request.getFirstName());
        en.setCREATE_DTME(new Date());
        en.setLAST_UPDTME(new Date());
        en.setCREATE_USER_ID(request.getUserNumId());
        en.setLAST_UPDATE_USER_ID(request.getUserNumId());
        en.setCANCELSIGN("N");
        return en;
    }

    private String getFirstNumId(Long tenantNumId, Long dataSign) {
        StringBuffer firstNumId = new StringBuffer();
        MDMS_O_LICENSE_FIRST en = mdmsOLicenseFirstDao.getLast(tenantNumId, dataSign.intValue());
        if (null == en) {
            firstNumId.append("111");
        } else {
            firstNumId.append(Integer.valueOf(en.getFirstNumId()) + 1);
        }
        return firstNumId.toString();
    }

    private String getSecondNumId(Long tenantNumId, Long dataSign, String firstNumId) {
        StringBuffer secondNumId = new StringBuffer();
        MDMS_O_LICENSE_SECOND en = mdmsOLicenseSecondDao.getLastByFirstNumId(tenantNumId, dataSign.intValue(), firstNumId);
        if (null == en) {
            secondNumId.append(firstNumId + "001");
        } else {
            secondNumId.append(getNewEquipmentNo(firstNumId, en.getSecondNumId().split(firstNumId)[1]));
        }
        return secondNumId.toString();
    }

    public static String getNewEquipmentNo(String firstNumId, String equipmentNo){
        String newEquipmentNo = "";
        if(equipmentNo != null && !equipmentNo.isEmpty()){
            int newEquipment = Integer.parseInt(equipmentNo) + 1;
            newEquipmentNo = String.format(firstNumId + "%03d", newEquipment);
        }

        return newEquipmentNo;
    }

    private MDMS_BL_PROCESS_OPERATE_LOG createProcessOperateLog(Long tenantNumId, Long dataSign, Long usrNumId,String cortNumId, String reservedNo,int billType,String content,int sourceType,String businessId){
        MDMS_BL_PROCESS_OPERATE_LOG entity = new MDMS_BL_PROCESS_OPERATE_LOG();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_PROCESS_OPERATE_LOG_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setBUSINESS_ID(businessId);
        entity.setSOURCE_TYPE(sourceType);
        entity.setCANCELSIGN("N");
        entity.setBUSINESS_TYPE(BlProcessTypeEnum.getBusinessTypeByBillType(billType));
        entity.setCORT_NUM_ID(cortNumId);
        entity.setOPERATE_CONTENT(content);
        entity.setRESERVED_NO(reservedNo);
        entity.setBILL_TYPE(billType);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        return entity;
    }

    private List<MDMS_O_LICENSE_RESOURCES> createLicenseEntity(Long tenantNumId, Long dataSign, Long usrNumId, String relationId,int typeNumId,String cortNumId,List<LicenseResourceDetails> infos){
        List<MDMS_O_LICENSE_RESOURCES> entityList=Lists.newArrayList();
        if(!Objects.isNull(infos)){
            for(LicenseResourceDetails info : infos){
                MDMS_O_LICENSE_RESOURCES entity = new MDMS_O_LICENSE_RESOURCES();
                entity.setBUSINESS_TYPE(typeNumId);
                entity.setCORT_NUM_ID(cortNumId);
                entity.setRELATION_ID(relationId);
                entity.setTITLE(info.getTitle());
                entity.setIMAGE_TYPE(info.getImageType());
                entity.setSECOND_LICENSE_TYPE_ID(info.getSecondLicenseTypeId());
                entity.setLICENSE_NO(info.getLicenseNo());
                entity.setLICENSE_BEGIN_DATE(info.getLicenseBeginDate());
                entity.setLICENSE_END_DATE(info.getLicenseEndDate());
                entity.setURL(info.getUrl());
                entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_LICENSE_RESOURCES_SERIES));
                entity.setTENANT_NUM_ID(tenantNumId);
                entity.setDATA_SIGN(dataSign);
                entity.setCREATE_USER_ID(usrNumId);
                entity.setLAST_UPDATE_USER_ID(usrNumId);
                entity.setCANCELSIGN("N");
                entity.setCREATE_DTME(new Date());
                entity.setLAST_UPDTME(new Date());
                entityList.add(entity);
            }
        }
        return entityList;
    }
}
