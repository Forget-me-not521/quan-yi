package com.ykcloud.soa.omp.cmasterdata.entity;

import com.ykcloud.soa.erp.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MDMS_B2B_CS_POLICY extends BaseEntity {
    private static final long serialVersionUID=1L;

    /** 公司 */
    @ApiModelProperty(value = "公司")
    private String CORT_NUM_ID;

    /** 业务单元 */
    @ApiModelProperty(value = "业务单元")
    private String UNIT_NUM_ID;

    /** 组织 */
    @ApiModelProperty(value = "组织")
    private String ORG_UNIT;

    /** 客户 */
    @ApiModelProperty(value = "客户")
    private String CUSTOMER_NUM_ID;

    /** 渠道 */
    @ApiModelProperty(value = "渠道")
    private Integer CHANNEL_NUM_ID;

    /** 客户门店 */
    @ApiModelProperty(value = "客户门店")
    private String SUB_UNIT_NUM_ID;

    /** 商品范围层级。1商品，2小类 */
    @ApiModelProperty(value = "商品范围层级。1商品，2小类")
    private Integer SELECT_ITEM_LV;

    /** 小类 */
    @ApiModelProperty(value = "小类")
    private String PTY3_NUM_ID;

    /** 商品 */
    @ApiModelProperty(value = "商品")
    private String ITEM_NUM_ID;

    /** 是否生效。0不生效，1生效 */
    @ApiModelProperty(value = "是否生效。0不生效，1生效")
    private Integer EFFECT_SIGN;

    /** 取价模式 */
    @ApiModelProperty(value = "取价模式")
    private Integer PRICE_MODEL;

    /** 参考价 */
    @ApiModelProperty(value = "参考价")
    private BigDecimal PRICE_RESULT;

    /** 加价毛利率 */
    @ApiModelProperty(value = "加价毛利率")
    private BigDecimal PRICE_ADD_RATE;

    /** 固定加价 */
    @ApiModelProperty(value = "固定加价")
    private BigDecimal PRICE_ADD_PRICE;

    /** 来源 */
    @ApiModelProperty(value = "来源")
    private String FORM_TYPE;

    /** 来源单号 */
    @ApiModelProperty(value = "来源单号")
    private String FORM_BILL;

    /** 更新用户 */
    @ApiModelProperty(value = "更新用户")
    private Long UPDATE_USER_ID;

    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private Date UPDATE_DTME;

}
