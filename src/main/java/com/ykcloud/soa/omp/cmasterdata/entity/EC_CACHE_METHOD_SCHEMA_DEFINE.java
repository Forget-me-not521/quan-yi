package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class EC_CACHE_METHOD_SCHEMA_DEFINE {

    private String SERIES;
    private Long TENANT_NUM_ID;
    private Long DATA_SIGN;
    private String SUB_SYSTEM;
    private String METHOD_NAME;
    private String SQL_CONTENT;
    private String DB;
    private String CACHE_METHOD;
    private String CACHE_MULTI_COL;
    private Long TTL;
    private Integer LIST_SIGN;
    private Date CREATE_DTME;
    private Date LAST_UPDTME;
    private Long CREATE_USER_ID;
    private Long LAST_UPDATE_USER_ID;
    private String DESCRIPTION;
    private Integer ALLOW_LIST_EMPTY_SIGN;
    private String REMARK;

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String sERIES) {
        SERIES = sERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long tENANT_NUM_ID) {
        TENANT_NUM_ID = tENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long dATA_SIGN) {
        DATA_SIGN = dATA_SIGN;
    }

    public String getSUB_SYSTEM() {
        return SUB_SYSTEM;
    }

    public void setSUB_SYSTEM(String sUB_SYSTEM) {
        SUB_SYSTEM = sUB_SYSTEM;
    }

    public String getMETHOD_NAME() {
        return METHOD_NAME;
    }

    public void setMETHOD_NAME(String mETHOD_NAME) {
        METHOD_NAME = mETHOD_NAME;
    }

    public String getSQL_CONTENT() {
        return SQL_CONTENT;
    }

    public void setSQL_CONTENT(String sQL_CONTENT) {
        SQL_CONTENT = sQL_CONTENT;
    }

    public String getDB() {
        return DB;
    }

    public void setDB(String dB) {
        DB = dB;
    }

    public String getCACHE_METHOD() {
        return CACHE_METHOD;
    }

    public void setCACHE_METHOD(String cACHE_METHOD) {
        CACHE_METHOD = cACHE_METHOD;
    }

    public String getCACHE_MULTI_COL() {
        return CACHE_MULTI_COL;
    }

    public void setCACHE_MULTI_COL(String cACHE_MULTI_COL) {
        CACHE_MULTI_COL = cACHE_MULTI_COL;
    }

    public Long getTTL() {
        return TTL;
    }

    public void setTTL(Long tTL) {
        TTL = tTL;
    }

    public Integer getLIST_SIGN() {
        return LIST_SIGN;
    }

    public void setLIST_SIGN(Integer lIST_SIGN) {
        LIST_SIGN = lIST_SIGN;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date cREATE_DTME) {
        CREATE_DTME = cREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date lAST_UPDTME) {
        LAST_UPDTME = lAST_UPDTME;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long cREATE_USER_ID) {
        CREATE_USER_ID = cREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long lAST_UPDATE_USER_ID) {
        LAST_UPDATE_USER_ID = lAST_UPDATE_USER_ID;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String dESCRIPTION) {
        DESCRIPTION = dESCRIPTION;
    }

    public Integer getALLOW_LIST_EMPTY_SIGN() {
        return ALLOW_LIST_EMPTY_SIGN;
    }

    public void setALLOW_LIST_EMPTY_SIGN(Integer aLLOW_LIST_EMPTY_SIGN) {
        ALLOW_LIST_EMPTY_SIGN = aLLOW_LIST_EMPTY_SIGN;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String rEMARK) {
        REMARK = rEMARK;
    }
}
