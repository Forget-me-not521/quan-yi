package com.ykcloud.soa.omp.cmasterdata.entity;

public class MDMS_P_RETURN_CONFIG extends BaseEntity {

    // 供应商
    private Long SUPPLY_UNIT_NUM_ID;

    // 商品编码
    private Long ITEM_NUM_ID;

    // 门店编码
    private Long SUB_UNIT_NUM_ID;

    // 是否允许退货 N不允许 Y允许
    private String RETURN_SIGN;

    // 发布标志
    private String ALREADYSEND;

    // 商品商家编码
    private String ITEMID;

    // 全局表保存修改时间戳的字段名
    private Long _MYCAT_OP_TIME;

    // 退货补偿
    private Double COMPENSATE_AMOUNT;
    // 新增
    private String INSERTDATA;
    // 更新
    private String UPDATEDATA;
    // 通讯
    private String SENDDATA;

    public String getINSERTDATA() {
        return INSERTDATA;
    }

    public void setINSERTDATA(String iNSERTDATA) {
        INSERTDATA = iNSERTDATA;
    }

    public String getUPDATEDATA() {
        return UPDATEDATA;
    }

    public void setUPDATEDATA(String uPDATEDATA) {
        UPDATEDATA = uPDATEDATA;
    }

    public String getSENDDATA() {
        return SENDDATA;
    }

    public void setSENDDATA(String sENDDATA) {
        SENDDATA = sENDDATA;
    }

    public Long getSUPPLY_UNIT_NUM_ID() {
        return SUPPLY_UNIT_NUM_ID;
    }

    public void setSUPPLY_UNIT_NUM_ID(Long sUPPLY_UNIT_NUM_ID) {
        SUPPLY_UNIT_NUM_ID = sUPPLY_UNIT_NUM_ID;
    }

    public Long getITEM_NUM_ID() {
        return ITEM_NUM_ID;
    }

    public void setITEM_NUM_ID(Long iTEM_NUM_ID) {
        ITEM_NUM_ID = iTEM_NUM_ID;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long sUB_UNIT_NUM_ID) {
        SUB_UNIT_NUM_ID = sUB_UNIT_NUM_ID;
    }

    public String getRETURN_SIGN() {
        return RETURN_SIGN;
    }

    public void setRETURN_SIGN(String rETURN_SIGN) {
        RETURN_SIGN = rETURN_SIGN;
    }

    public String getALREADYSEND() {
        return ALREADYSEND;
    }

    public void setALREADYSEND(String aLREADYSEND) {
        ALREADYSEND = aLREADYSEND;
    }

    public String getITEMID() {
        return ITEMID;
    }

    public void setITEMID(String iTEMID) {
        ITEMID = iTEMID;
    }

    public Long get_MYCAT_OP_TIME() {
        return _MYCAT_OP_TIME;
    }

    public void set_MYCAT_OP_TIME(Long _MYCAT_OP_TIME) {
        this._MYCAT_OP_TIME = _MYCAT_OP_TIME;
    }

    public Double getCOMPENSATE_AMOUNT() {
        return COMPENSATE_AMOUNT;
    }

    public void setCOMPENSATE_AMOUNT(Double cOMPENSATE_AMOUNT) {
        COMPENSATE_AMOUNT = cOMPENSATE_AMOUNT;
    }

}