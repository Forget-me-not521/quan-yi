package com.ykcloud.soa.omp.cmasterdata.service.model;

import java.io.Serializable;

public class BussinessUnit implements Serializable {
    private Integer id;
    private String code;
    private String rootCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRootCode() {
        return rootCode;
    }

    public void setRootCode(String rootCode) {
        this.rootCode = rootCode;
    }
}
