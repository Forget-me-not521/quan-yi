package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

@Data
public class MDMS_O_LICENSE_FIRST extends BaseEntity {
    private String licenseNumId;

    private String firstNumId;

    private String firstName;

}
