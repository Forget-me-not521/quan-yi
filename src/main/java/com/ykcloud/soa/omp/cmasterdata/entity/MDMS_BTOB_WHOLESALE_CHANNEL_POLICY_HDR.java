package com.ykcloud.soa.omp.cmasterdata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 批销-渠道政策结果对象 scm_wholesale_channel_policy
 *
 */
@Data
public class MDMS_BTOB_WHOLESALE_CHANNEL_POLICY_HDR implements Serializable {
    private static final long serialVersionUID=1L;

    /** 行号 */
    @ApiModelProperty(value = "行号")
    private Long series;

    /** 租户 */
    @ApiModelProperty(value = "租户")
    private Long tenantNumId;

    /** 生产或测试标识 */
    @ApiModelProperty(value = "生产或测试标识")
    private Long dataSign;

    /** 单据号 */
    @ApiModelProperty(value = "单据号")
    private String billNumId;

    /** 公司编码 */
    @ApiModelProperty(value = "公司编码")
    private Integer cortNumId;

    /** 销售组织 */
    @ApiModelProperty(value = "销售组织")
    private Integer unitNumId;

    /** 调价主题 */
    @ApiModelProperty(value = "调价主题")
    private String adjustPriceTheme;

    /** 单据类型 */
    @ApiModelProperty(value = "单据类型")
    private Byte billType;

    @ApiModelProperty(value = "单据状态(1:未确认 2:确认 3:提交 4:驳回 5:审批中 6:已完成)")
    private Byte billState;

    /** 分销渠道 */
    @ApiModelProperty(value = "分销渠道")
    private Byte channel;

    /** 生效日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "生效日期")
    private Date effectiveDate;

    /** 制单日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "制单日期")
    private Date makerDate;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createDtme;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "最后更新时间")
    private Date lastUpdtme;

    /** 用户 */
    @ApiModelProperty(value = "用户")
    private Long createUserId;

    /** 更新用户 */
    @ApiModelProperty(value = "更新用户")
    private Long lastUpdateUserId;

    /** 删除 */
    @ApiModelProperty(value = "删除")
    private String cancelsign;

    @ApiModelProperty(value = "备注")
    private String remark;

}
