package com.ykcloud.soa.omp.cmasterdata.enums;

public enum AssistStatusEnums {
    CLOSE(1, "关闭"),
    OPEN(2, "开启");

    private int id;
    private String value;

    AssistStatusEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (AssistStatusEnums item : AssistStatusEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
