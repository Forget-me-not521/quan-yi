package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.erp.common.exception.ErpExceptionType;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_LICENSE_FIRST;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;


@Repository
public class MdmsOLicenseFirstDao extends Dao<MDMS_O_LICENSE_FIRST> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public MDMS_O_LICENSE_FIRST getBySeries(Long tenantNumId, Integer dataSign, Long series) {
        String sql = "select * from mdms_o_license_first where tenant_num_id = ? and data_sign = ? " +
                " and series = ? and cancelsign='N'";
        List<MDMS_O_LICENSE_FIRST> list = jdbcTemplate.query(sql,
                new Object[]{tenantNumId, dataSign, series}, new BeanPropertyRowMapper<>(MDMS_O_LICENSE_FIRST.class));
        if (CollectionUtils.isEmpty(list)) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "找不到当前信息 ！行号:" + series);
        }
        if (list.size() > 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "当前信息存在多条 ！行号:" + series);
        }
        return list.get(0);
    }

    public MDMS_O_LICENSE_FIRST getLast(Long tenantNumId, Integer dataSign) {
        String sql = "select * from mdms_o_license_first where tenant_num_id = ? and data_sign = ? " +
                " and cancelsign='N' order by series desc";
        List<MDMS_O_LICENSE_FIRST> list = jdbcTemplate.query(sql,
                new Object[]{tenantNumId, dataSign}, new BeanPropertyRowMapper<>(MDMS_O_LICENSE_FIRST.class));
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    public void updateBySeries(Long tenantNumId, Integer dataSign, List<String> seriesList) {
        String sql = "update mdms_o_license_first set cancelsign = 'Y' where tenant_num_id = ? and data_sign = ?  " +
                "and cancelsign='N' and series in (" + StringUtils.join(seriesList, ",") + ")";
        int rows = jdbcTemplate.update(sql, tenantNumId, dataSign);
        if (rows != seriesList.size()) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ErpExceptionType.DOE33003,
                    "更新失败!");
        }
    }

    public List<MDMS_O_LICENSE_FIRST> getByLicenseNumId(Long tenantNumId, Long dataSign, String licenseNumId) {
        String sql = "select * from mdms_o_license_first where tenant_num_id = ? and data_sign = ? " +
                " and license_num_id = ? and cancelsign='N'";
        List<MDMS_O_LICENSE_FIRST> list = jdbcTemplate.query(sql,
                new Object[]{tenantNumId, dataSign, licenseNumId}, new BeanPropertyRowMapper<>(MDMS_O_LICENSE_FIRST.class));
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list;
    }



}
