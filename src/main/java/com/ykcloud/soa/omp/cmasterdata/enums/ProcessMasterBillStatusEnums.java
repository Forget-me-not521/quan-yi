package com.ykcloud.soa.omp.cmasterdata.enums;

public enum ProcessMasterBillStatusEnums {
    CREATE(1, "新建"),
    COMMIT(2, "提交审核"),
    PASS(3, "审核通过"),
    Invalid(4, "作废");

    private int id;
    private String value;

    ProcessMasterBillStatusEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (ProcessMasterBillStatusEnums item : ProcessMasterBillStatusEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }
}
