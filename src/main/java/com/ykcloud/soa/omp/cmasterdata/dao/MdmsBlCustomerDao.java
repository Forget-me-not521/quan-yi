package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.erp.common.utils.DaoUtil;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_CUSTOMER;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_PARTNER;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_SUB_UNIT;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

/**
 * @ author
 * @date: 2021年09月10日19:42
 * @describtion:
 */

@Repository
public class MdmsBlCustomerDao extends Dao<MDMS_BL_CUSTOMER> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public MDMS_BL_CUSTOMER getOldBlCustomer(Long tenantNumId, Long dataSign, String reservedNo, String series) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_customer ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=? and series=?  and cancelsign='N'  ");

        List<MDMS_BL_CUSTOMER> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo,series }, new BeanPropertyRowMapper<>(MDMS_BL_CUSTOMER.class));
        if(list.size()>1){
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据单号和明细行号查出多条数据!");
        }
        if(CollectionUtils.isNotEmpty(list)){
            return  list.get(0);
        }
        return  null;
    }

    public boolean checkBlCustomerExist(Long tenantNumId, Long dataSign,String reservedNo, String unifiedSocialCreditCode,String series) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from mdms_bl_customer ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=? and unified_social_credit_code=?  and cancelsign='N'  ");
        if(!Objects.isNull(series)){
            sb.append(" and series!="+series );
        }
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo,unifiedSocialCreditCode }, int.class) >0 ? true : false;
    }

    public List<MDMS_BL_CUSTOMER> getBlCustomerEntity(Long tenantNumId, Long dataSign, String reservedNo) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_customer ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=?   and cancelsign='N'  ");
        List<MDMS_BL_CUSTOMER> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo }, new BeanPropertyRowMapper<>(MDMS_BL_CUSTOMER.class));
        if(CollectionUtils.isNotEmpty(list)){
            return  list;
        }
        return  null;
    }
}
