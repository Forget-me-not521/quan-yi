package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOCity;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CITY;
import com.ykcloud.soa.omp.cmasterdata.service.model.AdministrativeRegionQueryCondition;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.*;

@Repository
public class MdmsOCityDao {

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_CITY.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_CITY.class, ",");
    private static final String TABLE_NAME = "mdms_o_city";
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public boolean checkExistByPrvNumIdAndCityNumId(Long tenantNumId, Long dataSign, Long prvNumId, Long cityNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1)");
        sb.append(" from ");
        sb.append(" mdms_o_city ");
        sb.append(" where tenant_num_id=? and data_sign=? and prv_num_id=? and city_num_id = ? and cancelsign='N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, prvNumId, cityNumId}, Integer.class) == 0 ? true : false;
    }

    public boolean checkExistByCityNumId(Long tenantNumId, Long dataSign, Long cityNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1)");
        sb.append(" from ");
        sb.append(" mdms_o_city ");
        sb.append(" where tenant_num_id=? and data_sign=? and city_num_id = ? and cancelsign='N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, cityNumId}, Integer.class) == 0 ? true : false;
    }


    public List<MDMS_O_CITY> getEntityByCityName(Long tenantNumId, Long dataSign, String cityName) {
        String sql = "select * from mdms_o_city where tenant_num_id=? and data_sign=? and city_num_id = ? and cancelsign='N'";
        return jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, cityName}, new BeanPropertyRowMapper<>(MDMS_O_CITY.class));
    }

    public boolean checkExistSameCity(Long cityNumId, Long prvNumId, Long tenantNumId, Long dataSign) {
        String sql = "select count(1) from mdms_o_city where city_num_id=? and prv_num_id =? and tenant_num_id=? and data_sign=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{cityNumId, prvNumId, tenantNumId, dataSign}) == 0 ? false : true;
    }

    public int insertMdmsOCity(MDMS_O_CITY city) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(TABLE_NAME);
        sb.append(" ( ");
        sb.append(SQL_COLS);
        sb.append(") values (");
        sb.append(WILDCARDS);
        sb.append(")");
        System.out.println(sb.toString());
        System.out.println(Arrays.asList(EntityFieldUtil.fieldSplitValue(MDMS_O_CITY.class, city)));

        return jdbcTemplate.update(sb.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_CITY.class, city));
    }

    public Long checkExistSeries(String series, Long tenantNumId, Long dataSign) {
        String sql = "select city_num_id from mdms_o_city where SERIES = ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        Long cityNumId = jdbcTemplate.queryForObject(sql, Long.class, new Object[]{series, tenantNumId, dataSign});
        return cityNumId;
    }

    public int updateMdmsOCity(Long cityNumId, Long prvNumId, String cityName, String citySimNo, Long userNumId,
                               String series, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city set city_num_id=?,prv_num_id=?,city_name=?,city_sim_no=?,last_update_user_id=? ,last_updtme = now(),updatedata='Y' where series =? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.update(sql, new Object[]{cityNumId, prvNumId, cityName, citySimNo, userNumId, series, tenantNumId, dataSign});
    }

    public MDMS_O_CITY getMdmsOCityBySeries(String series, Long tenantNumId, Long dataSign) {
        String sql = "select * from mdms_o_city where SERIES = ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(MDMS_O_CITY.class), new Object[]{series, tenantNumId, dataSign});
    }

    public int deleteByseries(Long userNumId, String series, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city set cancelsign ='Y' ,last_update_user_id= ?,last_updtme=now() where series= ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.update(sql, new Object[]{userNumId, series, tenantNumId, dataSign});
    }

    public int deleteByprvId(Long userNumId, Long prvId, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city set cancelsign ='Y' ,last_update_user_id= ?,last_updtme=now() where prv_num_id =? and tenant_num_id=? and data_sign=? and cancelsign ='N' ";
        return jdbcTemplate.update(sql, new Object[]{userNumId, prvId, tenantNumId, dataSign});
    }

    public int checkExistCityByPrvNumId(Long prvId, Long tenantNumId, Long dataSign) {
        String sql = "select count(1) from mdms_o_city where prv_num_id=? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{prvId, tenantNumId, dataSign});
    }

    public Integer countCityInfo(Long tenantNumId, Long dataSign, AdministrativeRegionQueryCondition condition) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT  count(1) " +
                "FROM " +
                " mdms_o_city city " +
                "LEFT JOIN mdms_o_province prv ON city.tenant_num_id = prv.tenant_num_id " +
                "AND city.data_sign = prv.data_sign " +
                "AND city.cancelsign = prv.cancelsign " +
                "WHERE " +
                " city.tenant_num_id = :tenantNumId " +
                "AND city.data_sign = :dataSign " +
                "AND city.cancelsign = 'N' ");
        Map<String, Object> params = new HashMap<>();
        params.put("tenantNumId", tenantNumId);
        params.put("dataSign", dataSign);

        if (Objects.nonNull(condition.getPrvNumId())) {
            sql.append(" AND city.prv_num_id = :prvNumId ");
            params.put("prvNumId", condition.getPrvNumId());
        }

        if (Objects.nonNull(condition.getCityNumId())) {
            sql.append(" AND city.city_num_id = :cityNumId ");
            params.put("cityNumId", condition.getCityNumId());
        }
        if (Objects.nonNull(condition.getCitySimNo())) {
            sql.append(" AND locate(:citySimNo, city.city_sim_no) > 0");
            params.put("citySimNo", condition.getCitySimNo());
        }
        if (Objects.nonNull(condition.getCityName())) {
            sql.append(" AND locate(:cityName, city.city_name) > 0 ");
            params.put("cityName", condition.getCityName());
        }
        if (Objects.nonNull(condition.getPrvName())) {
            sql.append(" AND locate(:prvName, prv.prv_name) > 0 ");
            params.put("prvName", condition.getPrvName());
        }

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.TYPE);
    }

    public List<MdmsOCity> queryCityInfo(Long tenantNumId, Long dataSign, AdministrativeRegionQueryCondition condition, Integer start, Integer pageSize) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT " +
                " prv.prv_name, " +
                " prv.prv_num_id, " +
                " city.city_num_id, " +
                " city.city_sim_no, " +
                " city.city_name, " +
                " city.create_dtme, " +
                " city.create_user_id, " +
                " city.last_update_user_id, " +
                " city.last_updtme " +
                "FROM " +
                " mdms_o_city city " +
                "LEFT JOIN mdms_o_province prv ON city.tenant_num_id = prv.tenant_num_id " +
                "AND city.data_sign = prv.data_sign " +
                "AND city.cancelsign = prv.cancelsign " +
                "WHERE " +
                " city.tenant_num_id = :tenantNumId " +
                "AND city.data_sign = :dataSign " +
                "AND city.cancelsign = 'N' ");
        Map<String, Object> params = new HashMap<>();
        params.put("tenantNumId", tenantNumId);
        params.put("dataSign", dataSign);

        if (Objects.nonNull(condition.getPrvNumId())) {
            sql.append(" AND city.prv_num_id = :prvNumId ");
            params.put("prvNumId", condition.getPrvNumId());
        }

        if (Objects.nonNull(condition.getCityNumId())) {
            sql.append(" AND city.city_num_id = :cityNumId ");
            params.put("cityNumId", condition.getCityNumId());
        }
        if (Objects.nonNull(condition.getCitySimNo())) {
            sql.append(" AND locate(:citySimNo, city.city_sim_no) > 0");
            params.put("citySimNo", condition.getCitySimNo());
        }
        if (Objects.nonNull(condition.getCityName())) {
            sql.append(" AND locate(:cityName, city.city_name) > 0 ");
            params.put("cityName", condition.getCityName());
        }
        if (Objects.nonNull(condition.getPrvName())) {
            sql.append(" AND locate(:prvName, prv.prv_name) > 0 ");
            params.put("prvName", condition.getPrvName());
        }

        sql.append(" limit :start, :pageSize ");
        params.put("start", start);
        params.put("pageSize", pageSize);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(MdmsOCity.class));
    }

}
