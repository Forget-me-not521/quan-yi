package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

/**
 * @Author stark.jiang
 * @Date 2021/10/22/9:25
 * @Description:
 * @Version 1.0
 */
@Data
public class MDMS_O_SUPPLY_ANNUAL_REPORT_DTL extends BaseEntity{
     private String CORT_NUM_ID;//公司编码',
    private String REPORT_YEAR;//年份 ',
    private String SUPPLY_NUM_ID;//供应商编码 ',
    private String SUPPLY_NAME;//供应商名称',
    private String UNIFIED_SOCIAL_CREDIT_CODE;//统一社会信用代码',
    private Integer SUPPLY_STATUS;//供应商状态-总部',
    private Integer SUB_SUPPLY_STATUS;//分部供应商状态ID',
    private Integer CONFIRM_STATUS;//确认状态(1:待确认,2:已上传,3:无需上传)',
    private String REMARK;
}
