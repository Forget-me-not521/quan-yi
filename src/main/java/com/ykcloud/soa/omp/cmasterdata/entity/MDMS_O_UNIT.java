package com.ykcloud.soa.omp.cmasterdata.entity;

import io.swagger.models.auth.In;
import lombok.Data;

/**
 * 业务单元
 */
@Data
public class MDMS_O_UNIT extends BaseEntity {
    private String CORT_NUM_ID;
    private String UNIT_NUM_ID;
    private String PARENT_UNIT_NUM_ID;
    private String UNIT_NAME;
    private Integer UNIT_TYPE;
    private Integer SUB_TYPE;
}
