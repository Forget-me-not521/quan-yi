package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

@Data
public class MDMS_O_DEPART extends BaseEntity {
    private int DEPART_NUM_ID;
    private String DEPART_NAME;
    private String CORT_NUM_ID;
    private int PARENT_DEPART_NUM_ID;
}
