package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccommon.util.ExceptionUtil;
import com.gb.soa.omp.ccommon.util.JsonUtil;
import com.gb.soa.sequence.util.SeqGetUtil;
import com.ykcloud.soa.omp.cmasterdata.api.request.B2bCsProductConfirmRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.B2bCsProductGetRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.B2bCsProductQyRequest;
import com.ykcloud.soa.omp.cmasterdata.api.response.B2bCsProductEmptyResponse;
import com.ykcloud.soa.omp.cmasterdata.api.response.B2bCsProductGetResponse;
import com.ykcloud.soa.omp.cmasterdata.api.response.B2bCsProductQyResponse;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdmsB2bCsProductService;
import com.ykcloud.soa.omp.cmasterdata.dao.MdmsB2bCsProductDao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_B2B_CS_PRODUCT;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.ExcelParam;
import com.ykcloud.soa.omp.cmasterdata.util.ExcelUtil;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("mdmsB2bCsProductService")
@RestController
@Slf4j
public class MdmsB2bCsProductServiceImpl implements MdmsB2bCsProductService {
    @Resource
    private HttpServletResponse response;
    @Resource
    private MdmsB2bCsProductDao mdmsB2bCsProductDao;

    @Override
    public B2bCsProductEmptyResponse saveCsProduct(B2bCsProductConfirmRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin addCsProduct request:{}", JsonUtil.toJson(request));
        }
        B2bCsProductEmptyResponse response = new B2bCsProductEmptyResponse();
        try {
            MDMS_B2B_CS_PRODUCT hdr = createCsProduct(request, request.getTenantNumId(), request.getDataSign(), request.getUserNumId());
            if (null==request.getSeries()){
                hdr.setSeries(SeqGetUtil.getNoSubSequence(SeqUtil.MDMS_B2B_CS_PRODUCT_SERIES));
                mdmsB2bCsProductDao.insert(hdr);
            }else{
                mdmsB2bCsProductDao.update(hdr,request.getTenantNumId(), request.getDataSign(),request.getSeries().toString());
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end addSdBlCoSalesOrder response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public B2bCsProductQyResponse getCsProductListPage(B2bCsProductQyRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCsProductListPage request:{}", JsonUtil.toJson(request));
        }
        B2bCsProductQyResponse response = new B2bCsProductQyResponse();
        try {
            response.setResults(mdmsB2bCsProductDao.getListPage(request));
            response.setCount(mdmsB2bCsProductDao.getListPageCount(request));
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCsProductListPage response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public B2bCsProductGetResponse getCsProduct(B2bCsProductGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCsProduct request:{}", JsonUtil.toJson(request));
        }
        B2bCsProductGetResponse response = new B2bCsProductGetResponse();
        try {

            response.setCsProduct(mdmsB2bCsProductDao.getB2bCsProduct(request.getTenantNumId(),request.getDataSign(),request.getSeries()));
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCsProduct response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public B2bCsProductEmptyResponse delCsProduct(B2bCsProductGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin delCsProduct request:{}", JsonUtil.toJson(request));
        }
        B2bCsProductEmptyResponse response = new B2bCsProductEmptyResponse();
        try {
            mdmsB2bCsProductDao.deleteB2bCsProduct(request.getTenantNumId(),request.getDataSign(),request.getSeries());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end delCsProduct response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public void importTemplate() {
        try {
            ExcelParam.Builder eBuilder = new ExcelParam.Builder("导入模版");
            List<String> headerList = new ArrayList<>();
            headerList.add("公司ID");
            headerList.add("销售组织ID");
            headerList.add("分销渠道ID");
            headerList.add("客户ID");
            headerList.add("商品ID");
            headerList.add("是否启用ID");
            headerList.add("条款类型ID");
            eBuilder.headers(headerList.toArray(new String[headerList.size()]));
            eBuilder.data(new ArrayList<String[]>(0));
            ExcelUtil.exportOneBookForXlsx(eBuilder.build(), response, "导入模版");
        } catch (Exception e) {
            log.error("模版下载报错", e);
        }
    }

    private MDMS_B2B_CS_PRODUCT createCsProduct(B2bCsProductConfirmRequest request, Long tenantNumId, Long dataSign, Long userNumId) {
        MDMS_B2B_CS_PRODUCT product = JsonUtil.fromJsonCamel(JsonUtil.toJson(request), MDMS_B2B_CS_PRODUCT.class);
        product.setTenant_num_id(tenantNumId);
        product.setData_sign(dataSign);
        product.setCreate_user_id(userNumId);
        product.setCreate_dtme(new Date());
        product.setLast_update_user_id(userNumId);
        product.setLast_updtme(new Date());
        product.setCancelsign(Constant.CANCEL_SIGN_N);
        return product;
    }
}
