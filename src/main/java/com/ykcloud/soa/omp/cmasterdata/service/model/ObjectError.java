package com.ykcloud.soa.omp.cmasterdata.service.model;

import java.util.List;

public class ObjectError {

    private List<Object> list;

    private List<String> error;

    public List<Object> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }
}
