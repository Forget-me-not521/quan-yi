package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.erp.common.dao.SqlUtil;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_UNIT;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;

@Repository
public class MdmsOUnitDao extends Dao<MDMS_O_UNIT> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public boolean checkUnitExistByUnitNumId(Long tenantNumId, Long dataSign, String cortNumId, String unitNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from  mdms_o_unit ");
        sb.append(" where tenant_num_id=? and data_sign=? and cort_num_id=? and unit_num_id = ? and cancelsign='N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId,unitNumId}, Integer.class) > 0 ? true : false;
    }

    public int updateEntity(Long tenantNumId, Long dataSign,Long userNumId,String cortNumId,String unitNumId,String unitName) {
        String sql = "update mdms_o_unit set unit_name =? ,last_update_user_id= ?,last_updtme=now() where tenant_num_id=? and data_sign=? and cort_num_id=? and unit_num_id=? and cancelsign ='N' ";
        return jdbcTemplate.update(sql, new Object[]{unitName,userNumId,tenantNumId, dataSign,cortNumId,unitNumId});
    }
}