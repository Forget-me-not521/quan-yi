package com.ykcloud.soa.omp.cmasterdata.entity;

public class COMMONCALLTABLE {
    private String CMD;//前端调用命令',
    private String FUNCNAME;//前端调用命令别名',
    private String BEANID;//服务名',
    private String METHOD;//服务方法',
    private String REMARK;//备注',
    private String REQUEST_SAMPLE;//请求参数范例',
    private String DATA_SIGN;//测试标识',
    private String SERIES;

    public String getCMD() {
        return CMD;
    }

    public void setCMD(String CMD) {
        this.CMD = CMD;
    }

    public String getFUNCNAME() {
        return FUNCNAME;
    }

    public void setFUNCNAME(String FUNCNAME) {
        this.FUNCNAME = FUNCNAME;
    }

    public String getBEANID() {
        return BEANID;
    }

    public void setBEANID(String BEANID) {
        this.BEANID = BEANID;
    }

    public String getMETHOD() {
        return METHOD;
    }

    public void setMETHOD(String METHOD) {
        this.METHOD = METHOD;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public String getREQUEST_SAMPLE() {
        return REQUEST_SAMPLE;
    }

    public void setREQUEST_SAMPLE(String REQUEST_SAMPLE) {
        this.REQUEST_SAMPLE = REQUEST_SAMPLE;
    }

    public String getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(String DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String SERIES) {
        this.SERIES = SERIES;
    }
}
