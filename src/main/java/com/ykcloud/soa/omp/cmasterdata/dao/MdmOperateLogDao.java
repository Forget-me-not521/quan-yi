package com.ykcloud.soa.omp.cmasterdata.dao;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

import javax.annotation.Resource;

import com.ykcloud.soa.omp.cmasterdata.api.model.MdmOperateLog;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.entity.MDM_OPERATE_LOG;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

import java.util.List;

@Repository
public class MdmOperateLogDao {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDM_OPERATE_LOG.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDM_OPERATE_LOG.class, ",");
    private static final String TABLE_NAME = "MDM_OPERATE_LOG";


    public int insterMdmOperateLog(MDM_OPERATE_LOG mdm_operate_log) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int sum = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDM_OPERATE_LOG.class, mdm_operate_log));
        if (sum < 1) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001, mdm_operate_log.getRESERVED_NO() + "插入失败！");
        }
        return sum;
    }

    public List<MdmOperateLog> selectByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        String sql = "select SERIES, CREATE_USER_ID, CREATE_DTME, OPERATE_CONTENT from MDM_OPERATE_LOG WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  RESERVED_NO = ? AND cancelsign = 'N'";
        List<MdmOperateLog> mdmOperateLogList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(MdmOperateLog.class));
        return mdmOperateLogList;
    }
}
