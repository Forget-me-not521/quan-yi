package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class EX_ARC_POS_CONFIG extends BaseEntity {
    private Long TENANT_NUM_ID;//租户ID
    private Long DATA_SIGN;//0: 正式  1：测试
    private Long SUB_UNIT_NUM_ID = 0l;//门店编号(子单元编号)
    private Long TML_CLIENT_ID = 0l;//终端号 0:代表只配置到门店别
    private String PARAM_NAME = "";//参数标识
    private String PARAM_VALUE = "";//参数值
    private String DESCRIPT = "";//参数描述
    private Long DIV_NUM_ID = 0l;//事业部
    private Long STATUS_SIGN = 0l;//是否生效 1是，0否
    private Date CREATE_DTME = new Date();//创建时间
    private Date LAST_UPDTME = new Date();//最后更新时间（离线POS根据更新时间不一致，差异更新本地CONFIG）
    private Long CREATE_USER_ID = 0l;//用户
    private Long LAST_UPDATE_USER_ID = 0l;//更新用户
    private String CANCELSIGN = "";//删除


    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long sUB_UNIT_NUM_ID) {
        SUB_UNIT_NUM_ID = sUB_UNIT_NUM_ID;
    }

    public Long getTML_CLIENT_ID() {
        return TML_CLIENT_ID;
    }

    public void setTML_CLIENT_ID(Long tML_CLIENT_ID) {
        TML_CLIENT_ID = tML_CLIENT_ID;
    }

    public String getPARAM_NAME() {
        return PARAM_NAME;
    }

    public void setPARAM_NAME(String pARAM_NAME) {
        PARAM_NAME = pARAM_NAME;
    }

    public String getPARAM_VALUE() {
        return PARAM_VALUE;
    }

    public void setPARAM_VALUE(String pARAM_VALUE) {
        PARAM_VALUE = pARAM_VALUE;
    }

    public String getDESCRIPT() {
        return DESCRIPT;
    }

    public void setDESCRIPT(String dESCRIPT) {
        DESCRIPT = dESCRIPT;
    }

    public Long getDIV_NUM_ID() {
        return DIV_NUM_ID;
    }

    public void setDIV_NUM_ID(Long dIV_NUM_ID) {
        DIV_NUM_ID = dIV_NUM_ID;
    }

    public Long getSTATUS_SIGN() {
        return STATUS_SIGN;
    }

    public void setSTATUS_SIGN(Long sTATUS_SIGN) {
        STATUS_SIGN = sTATUS_SIGN;
    }


    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long tENANT_NUM_ID) {
        TENANT_NUM_ID = tENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long dATA_SIGN) {
        DATA_SIGN = dATA_SIGN;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date cREATE_DTME) {
        CREATE_DTME = cREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date lAST_UPDTME) {
        LAST_UPDTME = lAST_UPDTME;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long cREATE_USER_ID) {
        CREATE_USER_ID = cREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long lAST_UPDATE_USER_ID) {
        LAST_UPDATE_USER_ID = lAST_UPDATE_USER_ID;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String cANCELSIGN) {
        CANCELSIGN = cANCELSIGN;
    }

}
