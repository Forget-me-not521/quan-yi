package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.EntityFieldUtil;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.entity.CANAL_KAFKA_LISTENER_LOG;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_C_TYPE;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_S_CONFIG;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

/**
 * @Author Hewei
 * @Date 2018/5/16 14:35
 */
@Repository
public class MdmsSConfigDao {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_S_CONFIG.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_S_CONFIG.class, ",");
    private static final String TABLE_NAME = "MDMS_S_CONFIG";

    public String getConfigValue(Long tenantNumId, Long dataSign, String configName) {
        String sql = "select config_value from mdms_s_config where tenant_num_id=? and data_sign=? and config_name=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, configName}, String.class);
    }

    public void insertMdmsSConfig(MDMS_S_CONFIG mdms_s_config) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_S_CONFIG.class, mdms_s_config));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "  数据插入失败! ");
        }
    }

    public boolean checkExistBySeries(Long tenantNumId, Long dataSign, String series) {
        return jdbcTemplate.queryForObject("select count(*) from MDMS_S_CONFIG where tenant_num_id=? and data_sign=? and series=? and cancelsign='N'", new Object[]{tenantNumId, dataSign, series}, Integer.class) > 0 ? true : false;
    }

    public void updateEntity(MDMS_S_CONFIG entity) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE MDMS_S_CONFIG SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(entity, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND series = ?");
        List<Object> objects = new ArrayList(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_S_CONFIG.class, entity)));
        objects.add(entity.getDATA_SIGN());
        objects.add(entity.getTENANT_NUM_ID());
        objects.add(entity.getSERIES());
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_S_CONFIG更新失败!");
        }
    }

    public void updateConfig(MDMS_S_CONFIG entity) {
        String sql = "update MDMS_S_CONFIG set config_value = ?, config_description=?,config_name=?,last_updtme = now(),"
                + " last_update_user_id = ? where tenant_num_id=? and data_sign=? and series =?"
                + " and cancelsign='N'";
        int row = jdbcTemplate.update(sql, entity.getCONFIG_VALUE(), entity.getCONFIG_DESCRIPTION(), entity.getCONFIG_NAME(), entity.getLAST_UPDATE_USER_ID(), entity.getTENANT_NUM_ID(), entity.getDATA_SIGN(), entity.getSERIES());
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35002, "更新失败!");
        }
    }

    public void deleteRecordBySeries(Long tenantNumId, Long dataSign, String series) {
        String sql = "delete from MDMS_S_CONFIG where TENANT_NUM_ID = ? AND DATA_SIGN = ? and series=? and cancelsign='N'";
        int row = jdbcTemplate.update(sql, tenantNumId, dataSign, series);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_S_CONFIG删除失败!");
        }
    }

    public List<MDMS_S_CONFIG> getSConfigList(Long tenantNumId, Long dataSign) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append("select ");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" where tenant_num_id =? and data_sign=?  and  cancelsign='N'");
        return jdbcTemplate.query(stringBuffer.toString(), new Object[]{tenantNumId, dataSign}, new BeanPropertyRowMapper<MDMS_S_CONFIG>(MDMS_S_CONFIG.class));
    }

    public boolean checkExistByConfigName(Long tenantNumId, Long dataSign, String configName, String configValue) {
        String sql = "select count(1) from mdms_s_config where tenant_num_id=? and data_sign=? and config_name=? and config_value=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, configName, configValue}, Long.class) > 0 ? true : false;
    }

    public Integer getCanalKafkaListenerLogCount(Long tenant_num_id, Long data_sign) {
        String sql = "select count(1) from canal_kafka_listener_log where tenant_num_id=? and data_sign=? and create_dtme < DATE_ADD(now(),INTERVAL -3 DAY) ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenant_num_id, data_sign}, Integer.class);
    }

    public List<String> queryListenerLogSeries(Long tenantNumId, Long dataSign, int offset, int limit) {
        String sql = "SELECT  SERIES FROM canal_kafka_listener_log WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ?   limit ?,?";
        return jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, offset, limit}, String.class);
    }

    public CANAL_KAFKA_LISTENER_LOG getCanalKafkaListenerLog(Long tenantNumId, Long dataSign, String series) {
        String sql = "select * from canal_kafka_listener_log where tenant_num_id=? and data_sign=? and series=? ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, new BeanPropertyRowMapper<CANAL_KAFKA_LISTENER_LOG>(CANAL_KAFKA_LISTENER_LOG.class));
    }

    public boolean checkExistKafkaListenerLog(Long tenantNumId, Long dataSign, String series) {
        String sql = "select count(1) from canal_kafka_listener_log_history "
                + " where tenant_num_id=? and data_sign=? and series=? ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, Long.class) > 0 ? true : false;
    }

    public void interCanalKafkaListenerHistoryLog(CANAL_KAFKA_LISTENER_LOG log) {
        StringBuilder sql = new StringBuilder();
        sql.append("insert into canal_kafka_listener_log_history ");
        sql.append("(series,tenant_num_id,data_sign,create_dtme,last_updtme,topics,group_id,msg_content,request,response,error,item_num_id,sub_unit_num_id,msg_status,send_count)");
        sql.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        Object[] args = new Object[]{log.getSERIES(), log.getTENANT_NUM_ID(),
                log.getDATA_SIGN(), log.getCREATE_DTME(), log.getLAST_UPDTME(),
                log.getTopics(), log.getGroup_id(), log.getMsg_content(),
                log.getRequest(), log.getResponse(), log.getError(),
                log.getItem_num_id(), log.getSub_unit_num_id(),
                log.getMsg_status(), log.getSend_count()};
        jdbcTemplate.update(sql.toString(), args);
    }


    public void deleteCanalKafkaListenerLog(Long tenant_num_id, Long data_sign, String series) {
        String sql = "delete from canal_kafka_listener_log where series=? and tenant_num_id=? and data_sign=? ";
        jdbcTemplate.update(sql, new Object[]{series, tenant_num_id, data_sign});
    }

}
