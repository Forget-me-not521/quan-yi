package com.ykcloud.soa.omp.cmasterdata.entity;

import java.io.Serializable;
import java.util.Date;

public class MDMS_O_SUB_UNIT_PAY_TYPE implements Serializable {

    private Long SERIES;

    private Long TENANT_NUM_ID;

    private Long SUB_UNIT_NUM_ID;

    private Long PAY_TYPE_ID;

    private Long MERCHANT_NUM_ID;

    private Double FEE_RATIO;

    private Long CREATE_USER_ID;

    private Long LAST_UPDATE_USER_ID;

    private Date CREATE_DTME;

    private Date LAST_UPDTME;

    private Integer CARRY_SIGN;

    private Long DATA_SIGN;

    private String CANCELSIGN;

    private Integer PAY_TYPE_CLASSIFY;

    public Long getSERIES() {
        return SERIES;
    }

    public void setSERIES(Long SERIES) {
        this.SERIES = SERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long TENANT_NUM_ID) {
        this.TENANT_NUM_ID = TENANT_NUM_ID;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long SUB_UNIT_NUM_ID) {
        this.SUB_UNIT_NUM_ID = SUB_UNIT_NUM_ID;
    }

    public Long getPAY_TYPE_ID() {
        return PAY_TYPE_ID;
    }

    public void setPAY_TYPE_ID(Long PAY_TYPE_ID) {
        this.PAY_TYPE_ID = PAY_TYPE_ID;
    }

    public Long getMERCHANT_NUM_ID() {
        return MERCHANT_NUM_ID;
    }

    public void setMERCHANT_NUM_ID(Long MERCHANT_NUM_ID) {
        this.MERCHANT_NUM_ID = MERCHANT_NUM_ID;
    }


    public Double getFEE_RATIO() {
        return FEE_RATIO;
    }

    public void setFEE_RATIO(Double FEE_RATIO) {
        this.FEE_RATIO = FEE_RATIO;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long CREATE_USER_ID) {
        this.CREATE_USER_ID = CREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long LAST_UPDATE_USER_ID) {
        this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date CREATE_DTME) {
        this.CREATE_DTME = CREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date LAST_UPDTME) {
        this.LAST_UPDTME = LAST_UPDTME;
    }

    public Integer getCARRY_SIGN() {
        return CARRY_SIGN;
    }

    public void setCARRY_SIGN(Integer CARRY_SIGN) {
        this.CARRY_SIGN = CARRY_SIGN;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String CANCELSIGN) {
        this.CANCELSIGN = CANCELSIGN;
    }

    public Integer getPAY_TYPE_CLASSIFY() {
        return PAY_TYPE_CLASSIFY;
    }

    public void setPAY_TYPE_CLASSIFY(Integer PAY_TYPE_CLASSIFY) {
        this.PAY_TYPE_CLASSIFY = PAY_TYPE_CLASSIFY;
    }
}
