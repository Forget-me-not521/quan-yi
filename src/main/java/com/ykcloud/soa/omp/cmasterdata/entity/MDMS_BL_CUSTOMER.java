package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;
import org.omg.CORBA.PRIVATE_MEMBER;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ author
 * @date: 2021年09月10日17:20
 * @describtion:
 */
@Data
public class MDMS_BL_CUSTOMER extends BaseEntity{
    private String RESERVED_NO;//流程编号',
    private String CUSTOMER_NAME;//客户名称',
    private String CUSTOMER_SIM_NAME;//客户简称',
    private String MNEMONIC_CODE;//助记码',
    private String POSTAL_CODE;//邮政编码',
    private String CONTACT_NUM;//联系电话',
    private Integer CUSTOMER_CLASSIFY;//客户分类(零售企业,批发企业,盈利性医疗机构,非盈利性医疗机构，单体药店,其他)',
    private String ADR;//联系地址',
    private String DEFAULT_HARVEST_ADR;//默认收获地址',
    private String BUSINESS_SCOPE;//经营范围',
    private Integer BUSINESS_PERMIT;//经营许可',
    private String UNIFIED_SOCIAL_CREDIT_CODE;//统一社会信用代码',
    private Integer CUSTOMER_STATUS;//客户状态-总部',
    private Date REGISTR_BEGIN_DATE;//注册日期',
    private Date REGISTR_END_DATE;//注册日期',
    private String REGISTER_PLACE;//注册地点',
    private String TAX_ACCOUNT;//税号',
    private Long PRV_NUM_ID;//省',
    private Long CITY_NUM_ID;//市',
    private Long CITY_AREA_NUM_ID;//县',
    private Long TOWN_NUM_ID;//镇',
    private Double CREDIT_LINE;//授信额度',
    private Double REGISTERED_CAPITAL;//注册资本',
    private Double PAID_IN_CAPITAL;//实缴资本',
    private Integer ABC_MARK;//ABC标识（下拉选择，ABC）',
    private Integer BASIC_BANK_NUM;//基本户银行联行号',
    private String BASIC_BANK_NAME;//基本户银行名称',
    private String BASIC_BANK_ACCOUNT;//基本户银行账号',
    private Integer GOVERNMENT_RELATED_CUSTOMER;//政府相关客户(是否)',
    private Integer POTENTIAL_LICHONG_CUSTOMER;//潜在利冲客户(是否)',
    private String GOVERNMENT_RELATED_CUSTOMER_REMARK;//政府相关客户-备注',
    private String POTENTIAL_LICHONG_CUSTOMER_REMARK;//潜在利冲客户-备注',
    private int IS_MONOMER_DRUGSTORE;//是否单体药店
    //---------------------分部-----------------------------
    private String CORT_NUM_ID;
    private String OLD_CUSTOMER_ID;//旧客户编码 ',
    private String FILE_NO;//档案号',
    private String QUALITY_LEADER;//质量负责人',
    private Integer ANNUAL_REPORT;//年度报告',
    private Date ANNUAL_REPORT_BEGIN;//年度报告效期开始',
    private Date ANNUAL_REPORT_END;//年度报告效期结束',
    private Integer PAYMENT_TERM;//付款条件',
    private String CORPORAT;//法人',
    private String CORPORAT_PROXY_NAME;//法人委托人姓名',
    private String CORPORAT_PROXY_STAT_IDCARD;//法人委托人的身份证号码',
    private Date CORPORAT_PROXY_IDCARD_BEGIN;//法人委托人的身份证有效期开始',
    private Date CORPORAT_PROXY_IDCARD_END;//法人委托人的身份证有效期结束',
    private Date CORPORAT_PROXY_STAT_BEGIN;//法人委托书效期开始',
    private Date CORPORAT_PROXY_STAT_END;//法人委托书效期结束',
    private Double SUB_CREDIT_LINE;//分部授信额度',
    private Integer SUB_CUSTOMER_STATUS;//状态ID',
    private String FINANCE_NAME;//财务联系人',
    private String FINANCE_TEL;//财务联系人电话',
    private Integer RECEIVING_BANK_NUM;//收款银行联行号',
    private String RECEIVING_BANK_NAME;//收款银行名称',
    private String RECEIVING_BANK_ACCOUNT;//收款银行账号',
    private String REMARK;//备注',
    private Integer MONEY_TYPE;//币别',
    private String PURCHASE_LEADER;//采购负责人',
    private Integer CLOSE_DAYS;//关闭天数',
    private Integer FORBID_RETURN_FACTORY;//禁退厂(1-是,0-否)',
    private Integer FORBID_SALE;//禁售(1-是,0-否)',
    private Integer PRE_DISTRIBUTION;//送货前置期（天）',
    private Integer DIRECT_DELIVERY_FLAG;//直配标志(1-是,0-否)',
    private String STORAGE_ADR;//仓库地址',
    private String BUSINESS_CONTACT;//业务联系人',
    private String BUSINESS_CONTACT_TEL;//业务联系人电话',
    private String BUSINESS_CONTACT_IDCARD;//业务联系人身份证号码',
    private Integer SALE_LIMIT_DAYS;//限销近效期天数',
    private Integer LICENCE_WARN_DAYS;//证照预警天数',
    private String REPLENISHER;//销售员',
    private String DRUG_BUSINESS_LICENSE;//药品经营许可证',
    private Date DRUG_BUSINESS_LICENSE_BEGIN;//药品经营许可证有效期',
    private Date DRUG_BUSINESS_LICENSE_END;//药品经营许可证有效期',
    private String DRUG_PRODUCT_LICENSE;//药品生产许可证',
    private Date DRUG_PRODUCT_LICENSE_BEGIN;//药品生产许可证有效期开始',
    private Date DRUG_PRODUCT_LICENSE_END;//药品生产许可证有效期结束',
    private Date BUSINESS_LICENSE_BEGIN_DATE;//营业执照有效期
    private Date BUSINESS_LICENSE_END_DATE;//营业执照有效期
    private Date QUALITY_AGREEMENT_BEGIN_DATE;//质量协议书有效期
    private Date QUALITY_AGREEMENT_END_DATE;//质量协议书有效期
    private String MEDICAL_INSTITUTION_LICENSE;//医疗机构许可证',
    private Date MEDICAL_INSTITUTION_BEGIN;//医疗机构许可证有效期开始',
    private Date MEDICAL_INSTITUTION_END;//医疗机构许可证有效期结束',
    private String MEDICAL_DEVICE_BUS_LINCENSE;//医疗器械经营许可证',
    private String MEDICAL_DEVICE_BUS_SCOPE;//医疗器械经营范围',
    private Date MEDICAL_DEVICE_BUS_BEGIN;//医疗器械经营许可证有效期开始',
    private Date MEDICAL_DEVICE_BUS_END;//医疗器械经营许可证有效期结束',
    private String MEDICAL_DEVICE_PRO_REC2;//二类医疗器械经营备案',
    private String MEDICAL_DEVICE_PRO_REC2_SCOPE;//二类医疗器械经营备案范围',
    private String FOOD_BUS_LICENSE;//食品经营许可证',
    private String FOOD_BUS_LICENSE_SCOPE;//食品经营许可证范围',
    private Date FOOD_BUS_LICENSE_BEGIN;//食品经营许可证有效期开始',
    private Date FOOD_BUS_LICENSE_END;//食品经营许可证有效期结束',

    private String CUSTOMER_NUM_ID;//客户名称',
}
