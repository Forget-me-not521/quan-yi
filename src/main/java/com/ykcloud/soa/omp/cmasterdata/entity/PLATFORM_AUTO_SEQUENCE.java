package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class PLATFORM_AUTO_SEQUENCE {
    // 行号
    private String SERIES;
    // 租户编号
    private Long TENANT_NUM_ID;
    // 0: 正式 1：测试
    private Long DATA_SIGN;
    private String SEQ_NAME;//序列名称',
    private String SEQ_PROJECT;//序列号所属项目名',
    private String SEQ_PREFIX;//序列前缀',
    private Long CURRENT_NUM;//当前序列',
    private Date CREATE_TIME;
    private String REMARK;//备注',
    private Long INIT_VALUE;//自增序列初始化值',
    private Integer CACHE_NUM;//序号缓存长度',

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String SERIES) {
        this.SERIES = SERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long TENANT_NUM_ID) {
        this.TENANT_NUM_ID = TENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }

    public String getSEQ_NAME() {
        return SEQ_NAME;
    }

    public void setSEQ_NAME(String SEQ_NAME) {
        this.SEQ_NAME = SEQ_NAME;
    }

    public String getSEQ_PROJECT() {
        return SEQ_PROJECT;
    }

    public void setSEQ_PROJECT(String SEQ_PROJECT) {
        this.SEQ_PROJECT = SEQ_PROJECT;
    }

    public String getSEQ_PREFIX() {
        return SEQ_PREFIX;
    }

    public void setSEQ_PREFIX(String SEQ_PREFIX) {
        this.SEQ_PREFIX = SEQ_PREFIX;
    }

    public Long getCURRENT_NUM() {
        return CURRENT_NUM;
    }

    public void setCURRENT_NUM(Long CURRENT_NUM) {
        this.CURRENT_NUM = CURRENT_NUM;
    }

    public Date getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(Date CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public Long getINIT_VALUE() {
        return INIT_VALUE;
    }

    public void setINIT_VALUE(Long INIT_VALUE) {
        this.INIT_VALUE = INIT_VALUE;
    }

    public Integer getCACHE_NUM() {
        return CACHE_NUM;
    }

    public void setCACHE_NUM(Integer CACHE_NUM) {
        this.CACHE_NUM = CACHE_NUM;
    }
}
