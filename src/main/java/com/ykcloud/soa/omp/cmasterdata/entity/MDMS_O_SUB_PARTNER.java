package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MDMS_O_SUB_PARTNER extends BaseEntity {

    private String PARTNER_NUM_ID;

    private String CORT_NUM_ID;

    private String CONTACTS_TEL;

    private String CONTACTS;
}