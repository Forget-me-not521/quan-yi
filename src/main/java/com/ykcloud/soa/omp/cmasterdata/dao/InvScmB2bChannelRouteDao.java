package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.B2bBlChannelRoute;
import com.ykcloud.soa.omp.cmasterdata.api.request.B2bBlChannelRouteAddRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.B2bBlChannelRouteGetRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.BtobChannelPolicyGetRequest;
import com.ykcloud.soa.omp.cmasterdata.entity.ScmB2bChannelRoute;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.MdDaoUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class InvScmB2bChannelRouteDao extends Dao<ScmB2bChannelRoute> {
   // @Resource(name = "tenanstorJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }
    private static final String TABLE_NAME = "stor_b2b_channel_route";

    public List<ScmB2bChannelRoute> getChannelRouteListPage(BtobChannelPolicyGetRequest r) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT r.* FROM ");
        sb.append(TABLE_NAME + " r where 1=1");
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("r","unit_num_id",r.getUnitNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("r","channel_num_id",r.getChannelNumId()));
        sb.append(MdDaoUtil.DATE_COL_SQL("r","last_updtme",r.getLastUpdtme()));
        sb.append(MdDaoUtil.TENANT_NUM_ID_DATA_SIGN_SQL("r"));
        int start = (r.getPageNum()-1)*r.getPageSize();
        sb.append(" limit ?,? ");
        List<ScmB2bChannelRoute> list = jdbcTemplate.query(sb.toString(), new Object[]{r.getUnitNumId(), r.getChannelNumId(),r.getLastUpdtme(), r.getTenantNumId(), r.getDataSign(),start,r.getPageSize()}, new BeanPropertyRowMapper<>(ScmB2bChannelRoute.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public Integer getChannelRouteListPageCount(BtobChannelPolicyGetRequest r) {
        String sb = "SELECT count(1) FROM " +
                TABLE_NAME + "where cancelsign='N' and tenant_num_id =? and data_sign = ?" +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("unit_num_id", r.getUnitNumId()) +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("channel_num_id", r.getChannelNumId()) +
                MdDaoUtil.DATE_COL_SQL("last_updtme", r.getLastUpdtme());
        return jdbcTemplate.queryForObject(sb, new Object[]{r.getTenantNumId(), r.getDataSign(),r.getUnitNumId(), r.getChannelNumId(),r.getLastUpdtme()}, Integer.class);
    }

    public B2bBlChannelRoute getChannelRoute(B2bBlChannelRouteGetRequest r){
        String sql = "select * from stor_b2b_channel_route WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  series = ?";
        B2bBlChannelRoute route = jdbcTemplate.queryForObject(sql, new Object[]{r.getTenantNumId(), r.getDataSign(), r.getSeries()}, new BeanPropertyRowMapper<>(B2bBlChannelRoute.class));
        if (null == route) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "不存在系统单号为" + r.getSeries() + "的表单数据！");
        }
        return route;
    }

    public void updateChannelRoute(B2bBlChannelRouteAddRequest r) {
        String sql = "update stor_b2b_channel_route " +
                "set cort_num_id =?, unit_num_id =?,org_unit =?,channel_num_id =?," +
                "storage_num_id =?,storage_lv =?,effect_sign =?,last_update_user_id =?,last_updtme = now() " +
                "WHERE tenant_num_id = ? AND data_sign = ? AND series = ? AND cancelsign = 'N'";
        if (jdbcTemplate.update(sql, r.getCort_num_id(),r.getUnit_num_id(), r.getOrg_unit(), r.getChannel_num_id(),r.getStorage_num_id(),
                r.getStorage_lv(),r.getEffect_sign(),r.getUserNumId(),r.getTenantNumId(),r.getDataSign(),r.getSeries()) < 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "行号" + r.getSeries() + "更新status_num_id失败!");
        }
    }

}
