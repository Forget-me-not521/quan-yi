package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class T_ORG {
    private Long ID;
    private Long TENANT_NUM_ID;
    private Long DATA_SIGN;
    private String ORG_NAME;
    private String ORG_KIND;
    private String PARENT_ID;
    private String PATH_ID;
    private String PATH_NAME;
    private Long USER_ID;
    private Long SEQ;
    private String ENABLED;
    private String COMMENTS;
    private String CREATE_USER;
    private Date CREATE_DATE;
    private String UPDATE_USER;
    private Date UPDATE_DATE;
    private String IS_VALID;
    private Long SUB_UNIT_NUM_ID;

    public Long getID() {
        return ID;
    }

    public void setID(Long iD) {
        ID = iD;
    }

    public String getORG_NAME() {
        return ORG_NAME;
    }

    public void setORG_NAME(String oRG_NAME) {
        ORG_NAME = oRG_NAME;
    }

    public String getORG_KIND() {
        return ORG_KIND;
    }

    public void setORG_KIND(String oRG_KIND) {
        ORG_KIND = oRG_KIND;
    }

    public String getPARENT_ID() {
        return PARENT_ID;
    }

    public void setPARENT_ID(String pARENT_ID) {
        PARENT_ID = pARENT_ID;
    }

    public String getPATH_ID() {
        return PATH_ID;
    }

    public void setPATH_ID(String pATH_ID) {
        PATH_ID = pATH_ID;
    }

    public String getPATH_NAME() {
        return PATH_NAME;
    }

    public void setPATH_NAME(String pATH_NAME) {
        PATH_NAME = pATH_NAME;
    }

    public Long getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(Long uSER_ID) {
        USER_ID = uSER_ID;
    }

    public Long getSEQ() {
        return SEQ;
    }

    public void setSEQ(Long sEQ) {
        SEQ = sEQ;
    }

    public String getENABLED() {
        return ENABLED;
    }

    public void setENABLED(String eNABLED) {
        ENABLED = eNABLED;
    }

    public String getCOMMENTS() {
        return COMMENTS;
    }

    public void setCOMMENTS(String cOMMENTS) {
        COMMENTS = cOMMENTS;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public void setCREATE_USER(String cREATE_USER) {
        CREATE_USER = cREATE_USER;
    }

    public Date getCREATE_DATE() {
        return CREATE_DATE;
    }

    public void setCREATE_DATE(Date cREATE_DATE) {
        CREATE_DATE = cREATE_DATE;
    }

    public String getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(String uPDATE_USER) {
        UPDATE_USER = uPDATE_USER;
    }

    public Date getUPDATE_DATE() {
        return UPDATE_DATE;
    }

    public void setUPDATE_DATE(Date uPDATE_DATE) {
        UPDATE_DATE = uPDATE_DATE;
    }

    public String getIS_VALID() {
        return IS_VALID;
    }

    public void setIS_VALID(String iS_VALID) {
        IS_VALID = iS_VALID;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long SUB_UNIT_NUM_ID) {
        this.SUB_UNIT_NUM_ID = SUB_UNIT_NUM_ID;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long TENANT_NUM_ID) {
        this.TENANT_NUM_ID = TENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }
}
