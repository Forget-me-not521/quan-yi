package com.ykcloud.soa.omp.cmasterdata.service.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author stark.jiang
 * @Date 2021/10/22/10:05
 * @Description:
 * @Version 1.0
 */
@Data
public class SupplyAnnualReport implements Serializable {
    private String supplyNumId;
    private String supplyName;
    private Integer supplyStatus;
    private String unifiedSocialCreditCode;
    private String cortNumId;
    private Integer subSupplyStatus;
}
