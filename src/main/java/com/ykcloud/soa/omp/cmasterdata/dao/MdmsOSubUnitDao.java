package com.ykcloud.soa.omp.cmasterdata.dao;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import com.ykcloud.soa.omp.cmasterdata.api.model.BlSubUnit;
import com.ykcloud.soa.omp.cmasterdata.api.model.CustomerShopInfo;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.google.common.base.Joiner;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_UNIT;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.DaoUtil;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

//门店
@Repository
public class MdmsOSubUnitDao {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_SUB_UNIT.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_SUB_UNIT.class, ",");
    private static final String TABLE_NAME = "MDMS_O_SUB_UNIT";


    public boolean checkSubUnitExist(Long tenantNumId, Long dataSign, String subUnitId,String cortNumId) {
        String sql = "select count(1) from mdms_o_sub_unit where " +
                " tenant_num_id = ? and data_sign = ? and sub_unit_id = ? and cort_num_id=? and cancelsign = 'N' ";
        Integer count = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, subUnitId,cortNumId}, Integer.class);
        if(ValidatorUtils.isNullOrZero(count)){
            return false;
        }
        if (count > 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店编号" + subUnitId + "找到多个门店");
        }
        return true;
    }

    //根据门店查询总门店
    public Long getSuperSubUnitNumId(Long tenantNumId, Long dataSign, Long subUnitNumId) {
        String sql = "select super_sub_unit_num_id from "
                + "mdms_o_sub_unit where tenant_num_id=? and data_sign=? "
                + "and sub_unit_num_id=? and cancelsign='N'";
        List<Long> superList = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, subUnitNumId}, Long.class);
        Long superSubUnitNumId = 0L;
        if (superList.size() > 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店编号查询到多个总店!门店编号:" + subUnitNumId);
        }
        if (superList.isEmpty()) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店编号查询到总店不存在,请核实门店编号!门店编号:" + subUnitNumId);
        }
        if (superList.get(0) != null) {
            superSubUnitNumId = superList.get(0);
        }
        return superSubUnitNumId;
    }



    public int updateEntity(Long tenantNumId, Long dataSign,String cortNumId, String subUnitNumId, MDMS_O_SUB_UNIT entity) {
        String cols = EntityFieldUtil.wildCardSplitUpdate(entity, ",");
        List<Object> list = new ArrayList<>(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_O_SUB_UNIT.class, entity)));
        list.add(tenantNumId);
        list.add(dataSign);
        list.add(subUnitNumId);
        list.add(cortNumId);
        StringBuffer sb = new StringBuffer();
        sb.append("update mdms_o_sub_unit set ");
        sb.append(cols);
        sb.append(" where tenant_num_id=? and data_sign=? and sub_unit_num_id=? and cort_num_id=? and cancelsign='N'");
        int row = jdbcTemplate.update(sb.toString(), list.toArray());
        if (row <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, String.format("更新门店资料失败!门店编码:%d", subUnitNumId));
        }
        return row;
    }

    public Long getUnitNumIdBySubUnitNumId(Long tenantNumId, Long dataSign, Long subUnitNumId) {
        String sql = "select unit_num_id from mdms_o_sub_unit where tenant_num_id=? and data_sign=? and sub_unit_num_id=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, subUnitNumId}, Long.class);
    }

    public Long getCortNumIdBySubUnitNumId(Long tenantNumId, Long dataSign, Long subUnitNumId) {
        String sql = "select cort_num_id from mdms_o_sub_unit where tenant_num_id=? and data_sign=? and sub_unit_num_id=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, subUnitNumId}, Long.class);
    }

    public int insertEntity(MDMS_O_SUB_UNIT mdmsOSubUnit) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_SUB_UNIT.class, mdmsOSubUnit));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, String.format("新增门店资料失败!门店编码:%d", mdmsOSubUnit.getSUB_UNIT_NUM_ID()));
        }
        return i;
    }
    public MDMS_O_SUB_UNIT getSubUnitEntity(Long tenantNumId, Long dataSign, String subUnitId,String cortNumId) {
        String sql = "select * from mdms_o_sub_unit where " +
                " tenant_num_id = ? and data_sign = ? and sub_unit_id = ? and cort_num_id=? and cancelsign = 'N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, subUnitId,cortNumId}, MDMS_O_SUB_UNIT.class);
    }


    public int updateEntity(Long tenantNumId, Long dataSign,Long userNumId,String cortNumId, String subUnitNumId, String remark,int statusId) {
        String sql="update mdms_o_sub_unit set status_num_id=?,remark=? ,last_update_user_id=? ,last_updtme=now() where tenant_num_id=? and data_sign=? and sub_unit_num_id=? and cort_num_id=? and cancelsign='N'";
        int row = jdbcTemplate.update(sql, new Object[]{statusId,remark,userNumId,tenantNumId, dataSign, subUnitNumId,cortNumId});
        if (row <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, String.format("更新门店资料失败!门店编码:%d", subUnitNumId));
        }
        return row;
    }


    public List<BlSubUnit> getSubUnitList(Long tenantNumId, Long dataSign, String subUnitNumId, String subUnitName,String simSubUnitName,Long createUserId, Date createDtmeBegin, Date createDtmeEnd,  String cortNumId, Integer pageNum, Integer pageSize) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_o_sub_unit ");
        sb.append(" where tenant_Num_id=? and data_sign=? and  cancelsign='N'  and cort_num_id=? ");

        if(!isNullOrZero(subUnitNumId)){
            sb.append(" and sub_unit_num_id="+subUnitNumId );
        }
        if(!isNullOrZero(subUnitName)){
            sb.append(" and sub_unit_name="+subUnitName );
        }
        if(!isNullOrZero(simSubUnitName)){
            sb.append(" and sim_sub_unit_name="+simSubUnitName );
        }
        if(!isNullOrZero(createUserId)){
            sb.append(" and create_user_id="+createUserId );
        }
        if(!isNullOrZero(createDtmeBegin)){
            sb.append(" and create_dtme>"+createDtmeBegin );
        }
        if(!isNullOrZero(createDtmeEnd)){
            sb.append(" and create_dtme<"+createDtmeEnd );
        }

        Integer start = (pageNum-1)*pageSize;
        sb.append(" limit ?,? ");
        List<BlSubUnit> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId,start,pageSize}, new BeanPropertyRowMapper<>(BlSubUnit.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return  list;
    }


    public Integer getSubUnitCount(Long tenantNumId, Long dataSign, String subUnitNumId, String subUnitName,String simSubUnitName,Long createUserId, Date createDtmeBegin, Date createDtmeEnd,  String cortNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from mdms_o_sub_unit ");
        sb.append(" where tenant_Num_id=? and data_sign=? and  cancelsign='N' and cort_num_id=? ");

        if(!isNullOrZero(subUnitNumId)){
            sb.append(" and sub_unit_num_id="+subUnitNumId );
        }
        if(!isNullOrZero(subUnitName)){
            sb.append(" and sub_unit_name="+subUnitName );
        }
        if(!isNullOrZero(simSubUnitName)){
            sb.append(" and sim_sub_unit_name="+simSubUnitName );
        }
        if(!isNullOrZero(createUserId)){
            sb.append(" and create_user_id="+createUserId );
        }
        if(!isNullOrZero(createDtmeBegin)){
            sb.append(" and create_dtme>"+createDtmeBegin );
        }
        if(!isNullOrZero(createDtmeEnd)){
            sb.append(" and create_dtme<"+createDtmeEnd );
        }
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign,cortNumId}, Integer.class);
    }

    public List<BlSubUnit> getSubUnitByCustomerNumId(Long tenantNumId, Long dataSign, String customerNumId) {
        String sql = "select shop_num_id,shop_name,sim_shop_name from mdms_o_sub_unit where " +
                " tenant_num_id = ? and data_sign = ? and customer_num_id = ? and cancelsign = 'N' ";
        return jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, customerNumId}, new BeanPropertyRowMapper<>(BlSubUnit.class));
    }

    private static boolean isNullOrZero(Object obj) {
        if (obj == null || "0".equals(obj.toString()) || "0.0".equals(obj.toString()) || "".equals(obj.toString())) {
            return true;
        }
        return false;
    }
}
