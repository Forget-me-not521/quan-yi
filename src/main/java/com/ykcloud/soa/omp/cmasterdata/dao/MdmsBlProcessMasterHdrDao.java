package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.BillMasterHdrInfo;
import com.ykcloud.soa.omp.cmasterdata.api.model.ProcessHdr;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_PARTNER;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_PROCESS_MASTER_HDR;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Repository
public class MdmsBlProcessMasterHdrDao extends Dao<MDMS_BL_PROCESS_MASTER_HDR> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_BL_PROCESS_MASTER_HDR.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_BL_PROCESS_MASTER_HDR.class, ",");
    private static final String TABLE_NAME = "MDMS_BL_PROCESS_MASTER_HDR";


    public MDMS_BL_PROCESS_MASTER_HDR getEntityByReservedNo(Long tenantNumId, Long dataSign, String reservedNo,String cortNumId) {
        String sql = "SELECT * from mdms_bl_process_master_hdr where tenant_num_id = ? and data_sign = ? and reserved_no = ? and cort_num_id=? and cancelsign='N' ";
        List<MDMS_BL_PROCESS_MASTER_HDR> list=jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, reservedNo,cortNumId}, new BeanPropertyRowMapper<>(MDMS_BL_PROCESS_MASTER_HDR.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        if(list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据有多条!");
        }
        return list.get(0);
    }

    public boolean checkExistByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        String sql = "SELECT count(1) from mdms_bl_process_master_hdr where tenant_num_id = ? and data_sign = ? and reserved_no = ? and cancelsign ='N' ";
        int count=jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, reservedNo}, int.class);
        if(count>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                    "该流程单据有多条!");
        }
        return count> 0 ? true:false;
    }



    public List<ProcessHdr> getByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        String sql = "SELECT * from mdms_bl_process_master_hdr where tenant_num_id = ? and data_sign = ? and reserved_no = ? and cancelsign='N' ";
        List<ProcessHdr> list=jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(ProcessHdr.class));
        if(CollectionUtils.isNotEmpty(list) && list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据有多条!");
        }
        return list;
    }

    public List<BillMasterHdrInfo> getEntityList(Long tenantNumId, Long dataSign, String reservedNo, Long createUserId, Date createDtmeBegin, Date createDtmeEnd, int billStatus, String cortNumId,int typeNumId, Integer pageNum, Integer pageSize) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT reserved_no,remark,bill_status,create_dtme,create_user_id,audit_user_id,audit_dtme  from mdms_bl_process_master_hdr where tenant_num_id = ? and data_sign = ?  and cort_num_id=? and source_type=1 and type_num_id=? and cancelsign='N' ");
        if(!ValidatorUtils.isNullOrZero(reservedNo)){
            sb.append(" and reserved_no="+reservedNo );
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sb.append(" and create_user_id="+createUserId );
        }
        if(!ValidatorUtils.isNullOrZero(createDtmeBegin)){
            sb.append(" and create_dtme>"+createDtmeBegin );
        }
        if(!ValidatorUtils.isNullOrZero(createDtmeEnd)){
            sb.append(" and create_dtme<"+createDtmeEnd );
        }

        if(!ValidatorUtils.isNullOrZero(billStatus)){
            sb.append(" and bill_status="+billStatus );
        }

        Integer start = (pageNum-1)*pageSize;

        sb.append(" limit ?,? ");
        List<BillMasterHdrInfo> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId,typeNumId,start,pageSize}, new BeanPropertyRowMapper<>(BillMasterHdrInfo.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return  list;
    }
    public Integer getEntityCount(Long tenantNumId, Long dataSign, String reservedNo, Long createUserId, Date createDtmeBegin, Date createDtmeEnd, int billStatus, String cortNumId,int typeNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT count(1) from mdms_bl_process_master_hdr where tenant_num_id = ? and data_sign = ?  and cort_num_id=? and source_type=1 and type_num_id=? and cancelsign='N' ");
        if(!ValidatorUtils.isNullOrZero(reservedNo)){
            sb.append(" and reserved_no="+reservedNo );
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sb.append(" and create_user_id="+createUserId );
        }
        if(!ValidatorUtils.isNullOrZero(createDtmeBegin)){
            sb.append(" and create_dtme>"+createDtmeBegin );
        }
        if(!ValidatorUtils.isNullOrZero(createDtmeEnd)){
            sb.append(" and create_dtme<"+createDtmeEnd );
        }

        if(!ValidatorUtils.isNullOrZero(billStatus)){
            sb.append(" and bill_status="+billStatus );
        }

        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign,cortNumId,typeNumId}, Integer.class);
    }
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }
    public ProcessHdr getBlEntityByReservedNo(Long tenantNumId, Long dataSign, String reservedNo,String cortNumId) {
        String sql = "SELECT * from mdms_bl_process_master_hdr where tenant_num_id = ? and data_sign = ? and reserved_no = ? and cort_num_id=? and cancelsign='N' ";
        List<ProcessHdr> list=jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, reservedNo,cortNumId}, new BeanPropertyRowMapper<>(ProcessHdr.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        if(list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据有多条!");
        }
        return list.get(0);
    }

}