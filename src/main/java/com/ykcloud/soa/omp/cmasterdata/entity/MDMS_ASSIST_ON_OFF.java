package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MDMS_ASSIST_ON_OFF implements Serializable {
    private Long SERIES;

    private Long TENANT_NUM_ID;

    private Integer DATA_SIGN;

    private String CORT_NUM_ID;

    private String CORT_NAME;

    private Integer ASSIST_STATUS;

    private Date CREATE_DTME;

    private Date LAST_UPDTME;

    private Long CREATE_USER_ID;

    private Long LAST_UPDATE_USER_ID;

    private String CANCELSIGN;

}
