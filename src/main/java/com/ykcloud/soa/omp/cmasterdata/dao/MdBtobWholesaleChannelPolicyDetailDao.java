package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.B2bBlChannelPolicyDtl;
import com.ykcloud.soa.omp.cmasterdata.entity.ScmB2bBlChannelPolicyDtl;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class MdBtobWholesaleChannelPolicyDetailDao extends Dao<ScmB2bBlChannelPolicyDtl> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public List<B2bBlChannelPolicyDtl> getChannelPolicyDtlList(Long tenantNumId, Long dataSign, String reservedNo){
        String sql = "select * from mdms_b2b_bl_channel_policy_dtl where TENANT_NUM_ID = ? and DATA_SIGN = ? and cancelsign='N' AND reserved_no = ?";
        List<B2bBlChannelPolicyDtl> dtlList = jdbcTemplate.query(sql, new Object[]{tenantNumId,dataSign, reservedNo}, new BeanPropertyRowMapper<>(B2bBlChannelPolicyDtl.class));
        if(CollectionUtils.isEmpty(dtlList)){
            return null;
        }
        return dtlList;
    }

    public int deleteChannelPolicyDtl(Long tenantNumId, Long dataSign,String reservedNo){
        String sql = "DELETE from mdms_b2b_bl_channel_policy_dtl WHERE   tenant_num_id = ? and data_sign = ? and reserved_no = ? ";
        return jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, reservedNo});
    }
}
