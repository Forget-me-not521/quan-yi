package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class MDMS_B2B_SUB_RELATION implements Serializable {
    private String SERIES;

    private Long TENANT_NUM_ID;

    private Long DATA_SIGN;

    private String CORT_NUM_ID;

    private String CORT_NAME;

    private String UNIT_NUM_ID;

    private String ORG_UNIT;

    private Integer CHANNEL_NUM_ID;

    private String SUB_UNIT_NUM_ID;

    private String SUB_UNIT_NAME;

    private Integer EFFECT_SIGN;

    private String ANNEX;

    private String REMARK;

    private Date CREATE_DTME;

    private Date LAST_UPDTME;

    private Long CREATE_USER_ID;

    private Long LAST_UPDATE_USER_ID;
}