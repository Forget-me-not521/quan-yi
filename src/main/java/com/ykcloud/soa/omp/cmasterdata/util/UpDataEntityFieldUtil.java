package com.ykcloud.soa.omp.cmasterdata.util;

import org.apache.commons.lang3.ArrayUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class UpDataEntityFieldUtil {
    private static final String filterStr = "serialVersionUID";
    private static final List<String> filterList = Arrays.asList("CREATE_DTME", "LAST_UPDTME", "create_dtme", "last_updtme", "lastUpdtme", "createDtme");
    private static final Map<String, String> defaultMap = new HashMap();

    public static Object[] fieldSplitValue(Class<?> klass, Object dataObject) {
        ArrayList objectList = null;

        try {
            Field[] fields = klass.getDeclaredFields();
            Field[] superFields = klass.getSuperclass().getDeclaredFields();
            int len = fields.length;
            int superLen = superFields.length;
            if (len > 0 || superLen > 0) {
                objectList = new ArrayList();

                Method method;
                int i;
                for (i = 0; i < len; ++i) {
                    if (!"serialVersionUID".equalsIgnoreCase(fields[i].getName()) && !filterList.contains(fields[i].getName()) && !defaultMap.containsKey(fields[i].getName())) {
                        method = dataObject.getClass().getDeclaredMethod("get" + fields[i].getName().toUpperCase());
                        Object object = method.invoke(dataObject);
                        if (object != null) {
                            objectList.add(object);
                        }
                    }
                }

                for (i = 0; i < superLen; ++i) {
                    if (!"serialVersionUID".equalsIgnoreCase(superFields[i].getName()) && !filterList.contains(superFields[i].getName()) && !defaultMap.containsKey(superFields[i].getName())) {
                        method = dataObject.getClass().getSuperclass().getDeclaredMethod("get" + superFields[i].getName().toUpperCase());
                        if (method.invoke(dataObject) != null) {
                            objectList.add(method.invoke(dataObject));
                        }
                    }
                }
            }
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException var10) {
            var10.printStackTrace();
        }

        return objectList == null ? null : objectList.toArray();
    }

    static {
        defaultMap.put("cancelsign", "'N'");
        defaultMap.put("CANCELSIGN", "'N'");
    }


    public static String wildCardSplitUpdate(Object dataObject, String regx) {
        StringBuilder stringBuilder = new StringBuilder();
        Field[] fields = (Field[]) ArrayUtils.addAll(dataObject.getClass().getDeclaredFields(), dataObject.getClass().getSuperclass().getDeclaredFields());
        int len = fields.length;
        if (len > 0) {
            for (int i = 0; i < len; ++i) {
                Method method;
                try {
                    method = dataObject.getClass().getDeclaredMethod("get" + fields[i].getName().toUpperCase());
                } catch (NoSuchMethodException var11) {
                    try {
                        method = dataObject.getClass().getSuperclass().getDeclaredMethod("get" + fields[i].getName().toUpperCase());
                    } catch (NoSuchMethodException var10) {
                        continue;
                    }
                }

                try {
                    Object object = method.invoke(dataObject);
                    if (object != null) {
                        if (stringBuilder.length() > 0) {
                            stringBuilder.append(regx);
                        }

                        stringBuilder.append(fields[i].getName().toUpperCase());
                        stringBuilder.append("=?");
                    }
                } catch (InvocationTargetException | IllegalAccessException var9) {
                    ;
                }
            }
        }

        return stringBuilder.toString();
    }
}
