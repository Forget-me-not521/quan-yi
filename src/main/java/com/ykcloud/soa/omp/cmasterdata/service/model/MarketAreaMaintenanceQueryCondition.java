package com.ykcloud.soa.omp.cmasterdata.service.model;

public class MarketAreaMaintenanceQueryCondition {
    // 大区编号
    private Long regionNumId;

    // 大区简码
    private String regionSimNo;

    // 大区名称
    private String regionName;

    public Long getRegionNumId() {
        return regionNumId;
    }

    public void setRegionNumId(Long regionNumId) {
        this.regionNumId = regionNumId;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionSimNo() {
        return regionSimNo;
    }

    public void setRegionSimNo(String regionSimNo) {
        this.regionSimNo = regionSimNo;
    }
}
