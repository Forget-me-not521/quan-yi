package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOSubSupply;
import com.ykcloud.soa.omp.cmasterdata.api.model.SubSupplyInfoForQuery;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_SUPPLY;
import com.ykcloud.soa.omp.cmasterdata.service.model.SupplyAnnualReport;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;


@Repository
public class MdmsOSubSupplyDao extends Dao<MDMS_O_SUB_SUPPLY> {
    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_SUB_SUPPLY.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_SUB_SUPPLY.class, ",");
    private static final String TABLE_NAME = "MDMS_O_SUB_SUPPLY";
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public List<MDMS_O_SUB_SUPPLY> selectBySupplyNumIdAndCortNumId(Long tenantNumId, Long dataSign, String supplyNumId, String cortNumId) {
        String sql = "select * from MDMS_O_SUB_SUPPLY WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  supply_num_id = ? AND cort_num_id = ? AND cancelsign = 'N'";
        List<MDMS_O_SUB_SUPPLY> mdmsOSubSupplyList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, supplyNumId, cortNumId}, new BeanPropertyRowMapper<>(MDMS_O_SUB_SUPPLY.class));
        if (CollectionUtils.isNotEmpty(mdmsOSubSupplyList) && mdmsOSubSupplyList.size() > 1) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "存在多笔表单数据！");
        }
        return mdmsOSubSupplyList;
    }

    public List<MDMS_O_SUB_SUPPLY> selectByCortNumId(Long tenantNumId, Long dataSign, String cortNumId) {
        String sql = "select * from MDMS_O_SUB_SUPPLY WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND cort_num_id = ? AND cancelsign = 'N'";
        List<MDMS_O_SUB_SUPPLY> mdmsOSubSupplyList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, cortNumId}, new BeanPropertyRowMapper<>(MDMS_O_SUB_SUPPLY.class));
        if (CollectionUtils.isNotEmpty(mdmsOSubSupplyList) && mdmsOSubSupplyList.size() > 1) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "存在多笔表单数据！");
        }
        return mdmsOSubSupplyList;
    }

    public List<MdmsOSubSupply> getMdmsOSubSupplyList(Long tenantNumId, Long dataSign, String cortNumId) {
        String sql = "select a.series, a.cort_num_id as cortNumId, a.supply_num_id as supplyNumId, b.supply_id as supplyId, b.supply_name as supplyName " +
                "from MDMS_O_SUB_SUPPLY a left join MDMS_O_SUPPLY b on a.supply_num_id = b.supply_num_id and a.tenant_num_id=b.tenant_num_id and a.data_sign=b.data_sign " +
                "WHERE a.TENANT_NUM_ID = ? AND a.DATA_SIGN = ? AND a.cancelsign = 'N' and a.cort_num_id=? ";
        List<MdmsOSubSupply> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, cortNumId}, new BeanPropertyRowMapper<>(MdmsOSubSupply.class));
        return list;
    }

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }



    public List<SupplyAnnualReport> getSupplyAnnualReportList(Long tenantNumId, Long dataSign) {
        String sql = "select s.supply_num_id,s.supply_name,s.supply_status,s.unified_social_credit_code,u.cort_num_id,u.sub_supply_status " +
                "from mdms_o_supply s left join mdms_o_sub_supply u on s.tenant_num_id=u.tenant_num_id and s.data_sign=u.data_sign and s.supply_num_id=u.supply_num_id" +
                "WHERE s.tenant_num_id = ? AND s.data_sign = ? AND a.cancelsign = 'N' ";
        List<SupplyAnnualReport> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign}, new BeanPropertyRowMapper<>(SupplyAnnualReport.class));
        return list;
    }


    public SubSupplyInfoForQuery getSubSupplyBySupplyNumId(Long tenantNumId, Long dataSign, String supplyNumId, String cortNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_o_sub_supply ");
        sb.append(" where tenant_Num_id=? and data_sign=? and  supply_num_id=? and cort_num_id=?   and cancelsign='N'  ");
        List<SubSupplyInfoForQuery> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,supplyNumId,cortNumId}, new BeanPropertyRowMapper<>(SubSupplyInfoForQuery.class));
        if(CollectionUtils.isEmpty(list)){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                    "该分部供应商不存在!");
        }
        if(list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                    "查出多条数据!");
        }
        return  list.get(0);
    }
}
