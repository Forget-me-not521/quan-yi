package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_W_ROUTE_HDR;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_W_SHIPMENT;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class MdmsWRouteHdrDao extends Dao<MDMS_W_ROUTE_HDR> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }


    public String checkExistRouteDaySign(Long tenantNumId, String cortNumId, Long dataSign, String physicalNumId, Long daySign) {
        StringBuilder sb = new StringBuilder("select series from MDMS_W_ROUTE_HDR");
        sb.append(" where tenant_num_id=? and cort_num_id=? and data_sign=? and physical_num_id=?");
        sb.append(" and day_sign=? and cancelsign='N' ");
        return jdbcTemplate.queryForObject(sb.toString(),
                new Object[]{tenantNumId, cortNumId, dataSign, physicalNumId, daySign}, String.class);
    }

    public boolean checkExistRouteNumId(Long tenantNumId, String cortNumId, Long dataSign, String physicalNumId, String routeNumId) {
        StringBuilder sb = new StringBuilder("select count(1) from MDMS_W_ROUTE_HDR");
        sb.append(" where tenant_num_id=? and cort_num_id=? and data_sign=? and physical_num_id=?");
        sb.append(" and route_num_id=? and cancelsign='N' ");
        Integer count = jdbcTemplate.queryForObject(sb.toString(),
                new Object[]{tenantNumId, cortNumId, dataSign, physicalNumId, routeNumId}, Integer.class);
        return count != null && count > 0;
    }
}
