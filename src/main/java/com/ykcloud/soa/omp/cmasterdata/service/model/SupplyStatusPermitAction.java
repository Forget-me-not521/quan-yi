package com.ykcloud.soa.omp.cmasterdata.service.model;

public class SupplyStatusPermitAction {

    private String purchaseSign;// 是否采购

    private String balanceSign;// 是否结算

    private String returnSign;// 是否允许退货

    private String checkingSign;// 是否对账

    public String getPurchaseSign() {
        return purchaseSign;
    }

    public void setPurchaseSign(String purchaseSign) {
        this.purchaseSign = purchaseSign;
    }

    public String getBalanceSign() {
        return balanceSign;
    }

    public void setBalanceSign(String balanceSign) {
        this.balanceSign = balanceSign;
    }

    public String getReturnSign() {
        return returnSign;
    }

    public void setReturnSign(String returnSign) {
        this.returnSign = returnSign;
    }

    public String getCheckingSign() {
        return checkingSign;
    }

    public void setCheckingSign(String checkingSign) {
        this.checkingSign = checkingSign;
    }

}
