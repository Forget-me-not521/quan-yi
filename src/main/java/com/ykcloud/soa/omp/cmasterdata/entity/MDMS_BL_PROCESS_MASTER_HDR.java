package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MDMS_BL_PROCESS_MASTER_HDR implements Serializable {

    private String SERIES;

    private Long TENANT_NUM_ID;

    private Long DATA_SIGN;

    private String CORT_NUM_ID;
    private String CHOICE_PROCESS_ID;
    private String RESERVED_NO;

    private String PROCESS_TITLE;

    private Integer TYPE_NUM_ID;

    private String REMARK;

    private Integer DEGREE_URGENCY;

    private String FILE_URL;

    private Date CREATE_DTME;

    private Date LAST_UPDTME;

    private Long CREATE_USER_ID;

    private Long LAST_UPDATE_USER_ID;

    private String CANCELSIGN;

    private Integer SOURCE_TYPE;

    private Integer BILL_STATUS;
}