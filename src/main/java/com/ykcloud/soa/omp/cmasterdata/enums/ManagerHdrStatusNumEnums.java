package com.ykcloud.soa.omp.cmasterdata.enums;

public enum ManagerHdrStatusNumEnums {
    CREATE(1, "创建"),
    WAITING_AUDIT(2, "待审核"),
    AUDITED(3, "已审核"),
    REJECTED(4, "驳回"),
    INVALID(5, "作废");

    private int id;
    private String value;

    ManagerHdrStatusNumEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (ManagerHdrStatusNumEnums item : ManagerHdrStatusNumEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
