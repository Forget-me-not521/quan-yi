package com.ykcloud.soa.omp.cmasterdata.dao;


import java.util.Arrays;


import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.entity.PLATFORM_SEQUENCE;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

@Repository
public class PlatformSequenceDao {
    @Resource(name = "platFormJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(PLATFORM_SEQUENCE.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(PLATFORM_SEQUENCE.class, ",");
    private static final String TABLE_NAME = "platform_sequence";

    public boolean checkExistSeqName(String seqName) {
        String sql = "select count(*) from platform_sequence where SEQ_NAME = ?";
        int number = jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{seqName});
        return number == 0 ? false : true;
    }

    public boolean checkExistSeries(String series) {
        String sql = "select count(*) from platform_sequence where SERIES = ?";
        int number = jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{series});
        return number == 0 ? false : true;
    }


    public int insertPlatformSequenceNew(PLATFORM_SEQUENCE platformSequence) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(TABLE_NAME);
        sb.append(" ( ");
        sb.append(SQL_COLS);
        sb.append(") values (");
        sb.append(WILDCARDS);
        sb.append(")");
        System.out.println(sb.toString());
        System.out.println(Arrays.asList(EntityFieldUtil.fieldSplitValue(PLATFORM_SEQUENCE.class, platformSequence)));

        return jdbcTemplate.update(sb.toString(), EntityFieldUtil.fieldSplitValue(PLATFORM_SEQUENCE.class, platformSequence));
    }

    public PLATFORM_SEQUENCE findPlatformSequenceBySeries(String series) throws Exception {
        String sql = "select * from platform_sequence where SERIES = ? ";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(PLATFORM_SEQUENCE.class), new Object[]{series});
    }

    public int updatePlatformSequence(String seq_NUM, String seq_VAL, Long current_NUM, Long seq_NUM_START,
                                      Long seq_NUM_END, String SEQ_PROJECT, String series) {

        String sql = "update platform_sequence set SEQ_NUM=? ,SEQ_VAL=?,CURRENT_NUM=?,SEQ_NUM_START=?,SEQ_NUM_END=?,SEQ_PROJECT=? where series = ?";

        return jdbcTemplate.update(sql, new Object[]{seq_NUM, seq_VAL, current_NUM, seq_NUM_START, seq_NUM_END, SEQ_PROJECT, series});
    }

    public PLATFORM_SEQUENCE findPlatformSequenceBySeqName(String SEQ_NAME) {
        String sql = "select * from platform_sequence where SEQ_NAME = ? ";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(PLATFORM_SEQUENCE.class), new Object[]{SEQ_NAME});

    }

}
