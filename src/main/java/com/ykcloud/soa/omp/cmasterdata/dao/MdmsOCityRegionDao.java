package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.gb.soa.sequence.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOCityRegion;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CITY_REGION;
import com.ykcloud.soa.omp.cmasterdata.service.model.MarketAreaMaintenanceQueryCondition;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

@Repository
public class MdmsOCityRegionDao {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_CITY_REGION.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_CITY_REGION.class, ",");
    private static final String TABLE_NAME = "MDMS_O_CITY_REGION";

    public MDMS_O_CITY_REGION selectMdmsWZone(Long tenantNumId, Long dataSign, Long regionNumId) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select ");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" where TENANT_NUM_ID=? and DATA_SIGN=? and REGION_NUM_ID=?");
        return jdbcTemplate.queryForObject(stringBuffer.toString(), new Object[]{tenantNumId, dataSign, regionNumId}, new BeanPropertyRowMapper<>(MDMS_O_CITY_REGION.class));
    }

    public int insetMdmsWZone(List<MDMS_O_CITY_REGION> mdms_o_city_regions) {
        int sum = 0;
        for (MDMS_O_CITY_REGION mdms_o_city_region : mdms_o_city_regions) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("insert into ");
            stringBuffer.append(TABLE_NAME);
            stringBuffer.append(" (");
            stringBuffer.append(SQL_COLS);
            stringBuffer.append(") values (");
            stringBuffer.append(WILDCARDS);
            stringBuffer.append(")");
            if (jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_CITY_REGION.class, mdms_o_city_region)) == 1) {
                sum++;
            }
        }
        if (sum != mdms_o_city_regions.size()) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001, "批量插入失败");
        }
        return sum;
    }

    public Long getRegionNumId(Long tenantNumId, Long dataSign, Long regionNumId) {
        String sql = "select count(*) from MDMS_O_city_REGION " +
                "where TENANT_NUM_ID=? and DATA_SIGN=? and region_sim_no = ?";
        return jdbcTemplate.queryForObject(sql.toString(), new Object[]{tenantNumId, dataSign, regionNumId}, Long.class);
    }

    public boolean checkRegoinIsExist(Long tenantNumId, Long dataSign, Long regionNumId) {

        String sql = "select count(*) from MDMS_O_city_REGION " +
                "where TENANT_NUM_ID=? and DATA_SIGN=? and region_num_id = ? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql.toString(), new Object[]{tenantNumId, dataSign, regionNumId}, Integer.class) == 0 ? false : true;
    }

    public List<MdmsOCityRegion> queryMarketAreaMaintainceInfo(Long tenantNumId, Long dataSign, MarketAreaMaintenanceQueryCondition condition, Integer start, Integer pageSize) {
        StringBuilder sql = new StringBuilder();
        sql.append("select * from mdms_o_city_region where tenant_num_id = :tenantNumId and data_sign = :dataSign and cancelsign = 'N' ");

        Map<String, Object> params = new HashMap<>();
        params.put("tenantNumId", tenantNumId);
        params.put("dataSign", dataSign);

        joinMdmsOCityRegionQuerySql(sql, params, condition);

        sql.append(" limit :start, :pageSize");
        params.put("start", start);
        params.put("pageSize", pageSize);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(MdmsOCityRegion.class));

    }


    public Integer countMarketAreaMaintainceInfo(Long tenantNumId, Long dataSign, MarketAreaMaintenanceQueryCondition condition) {
        StringBuilder sql = new StringBuilder();
        sql.append("select count(1) from mdms_o_city_region where tenant_num_id = :tenantNumId and data_sign = :dataSign and cancelsign = 'N' ");

        Map<String, Object> params = new HashMap<>();
        params.put("tenantNumId", tenantNumId);
        params.put("dataSign", dataSign);

        joinMdmsOCityRegionQuerySql(sql, params, condition);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);

    }

    public void joinMdmsOCityRegionQuerySql(StringBuilder sql, Map<String, Object> params, MarketAreaMaintenanceQueryCondition condition) {
        if (Objects.nonNull(condition.getRegionNumId())) {
            sql.append(" and region_num_id = :regionNumId");
            params.put("regionNumId", condition.getRegionNumId());
        }
        if (Objects.nonNull(condition.getRegionName())) {
            sql.append(" and locate(:regionName, region_name) > 0 ");
            params.put("regionName", condition.getRegionName());
        }
        if (Objects.nonNull(condition.getRegionSimNo())) {
            sql.append(" and locate(:regionSimNo, region_sim_no) > 0 ");
            params.put("regionSimNo", condition.getRegionSimNo());
        }
    }

    public void insertEntity(MDMS_O_CITY_REGION entity) {
        StringBuilder sql = new StringBuilder();
        sql.append("insert into ");
        sql.append(TABLE_NAME);
        sql.append(" (");
        sql.append(SQL_COLS);
        sql.append(") values (");
        sql.append(WILDCARDS);
        sql.append(")");
        int row = jdbcTemplate.update(sql.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_CITY_REGION.class, entity));
        if (row < 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "插入表MDMS_O_CITY_REGION记录失败!");
        }
    }

    public void updateEntity(MDMS_O_CITY_REGION entity) {
        StringBuilder sql = new StringBuilder();
        sql.append("update ");
        sql.append(TABLE_NAME);
        sql.append("  set REGION_SIM_NO = ?, REGION_NAME = ?, EN_REGION_NAME = ?, " +
                "AREA = ?, LAST_UPDTME = ? , LAST_UPDATE_USER_ID = ? ");
        sql.append(" where tenant_num_id = ? and data_sign = ? and series = ?");
        int row = jdbcTemplate.update(sql.toString(), entity.getREGION_SIM_NO(),
                entity.getREGION_NAME(),
                entity.getEN_REGION_NAME(),
                entity.getAREA(),
                entity.getLAST_UPDTME(),
                entity.getLAST_UPDATE_USER_ID(),
                entity.getTENANT_NUM_ID(),
                entity.getDATA_SIGN(),
                entity.getSERIES());
        if (row < 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "更新表MDMS_O_CITY_REGION记录失败!");
        }
    }

    public void deleteEntity(Long tenantNumId, Long dataSign, String series) {
        StringBuilder sql = new StringBuilder();
        sql.append("update " + TABLE_NAME + " set cancelsign = 'Y' where tenant_num_id = ? and data_sign = ? and series = ?");
        int row = jdbcTemplate.update(sql.toString(), tenantNumId,
                dataSign,
                series);
        if (row < 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "删除表MDMS_O_CITY_REGION记录失败!");
        }
    }
}
