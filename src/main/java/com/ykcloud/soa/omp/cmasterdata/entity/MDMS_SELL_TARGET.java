package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

/**
 * @author tz.x
 * @date 2021/5/24 13:56
 */
@Data
public class MDMS_SELL_TARGET extends BaseEntity {

    private Long SUB_UNIT_NUM_ID;

    private String SUB_UNIT_NAME;

    private String ACCOUNT_YEAR;

    private Double ACCOUNT_MONTH_1;

    private Double ACCOUNT_MONTH_2;

    private Double ACCOUNT_MONTH_3;

    private Double ACCOUNT_MONTH_4;

    private Double ACCOUNT_MONTH_5;

    private Double ACCOUNT_MONTH_6;

    private Double ACCOUNT_MONTH_7;

    private Double ACCOUNT_MONTH_8;

    private Double ACCOUNT_MONTH_9;

    private Double ACCOUNT_MONTH_10;

    private Double ACCOUNT_MONTH_11;

    private Double ACCOUNT_MONTH_12;


}
