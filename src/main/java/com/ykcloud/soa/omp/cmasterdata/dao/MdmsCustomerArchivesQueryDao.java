package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.EntityFieldUtil;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_B2B_SUB_RELATION;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_C_TYPE;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CUSTOMER_SHOP;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_CUSTOMER;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author wangcong
 * @desc 客户档案dao
 * @date 2021/11/2 14:45
 */
@Repository
public class MdmsCustomerArchivesQueryDao {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public List<MDMS_O_SUB_CUSTOMER> selectMdmsOSubCustomerListPage(Long tenantNumId, Long dataSign, String saleOrg, String distChannel, String customerNumId, String validContract, String firstSaleDate, int pageNum, int pageSize) {
        String sql = "select * from MDMS_O_SUB_CUSTOMER where tenant_num_id = ? and data_sign = ? limit ?,?";
        return jdbcTemplate.query(sql, new Object[]{
                tenantNumId,
                dataSign,
                --pageNum * pageSize,
                pageSize
        }, new BeanPropertyRowMapper<MDMS_O_SUB_CUSTOMER>(MDMS_O_SUB_CUSTOMER.class));
    }

    public int selectMdmsOSubCustomerListPageTotal(Long tenantNumId, Long dataSign, String saleOrg, String distChannel, String customerNumId, String validContract, String firstSaleDate) {
        String sql = "select count(*) from MDMS_O_SUB_CUSTOMER where tenant_num_id = ? and data_sign = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign}, Integer.class);
    }

    public MDMS_O_SUB_CUSTOMER selectMdmsOSubCustomer(Long tenantNumId, Long dataSign, Long series) {
        String sql = "select * from MDMS_O_SUB_CUSTOMER where tenant_num_id = ? and data_sign = ? and series = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, new BeanPropertyRowMapper<MDMS_O_SUB_CUSTOMER>(MDMS_O_SUB_CUSTOMER.class));
    }

    public List<MDMS_O_CUSTOMER_SHOP> selectMdmsOCustomerShopListPage(Long tenantNumId, Long dataSign, int pageNum, int pageSize) {
        String sql = "select * from MDMS_O_CUSTOMER_SHOP where tenant_num_id = ? and data_sign = ? limit ?,?";
        return jdbcTemplate.query(sql, new Object[]{
                tenantNumId,
                dataSign,
                --pageNum * pageSize,
                pageSize
        }, new BeanPropertyRowMapper<MDMS_O_CUSTOMER_SHOP>(MDMS_O_CUSTOMER_SHOP.class));
    }

    public int selectMdmsOCustomerShopListPageTotal(Long tenantNumId, Long dataSign) {
        String sql = "select count(*) from MDMS_O_CUSTOMER_SHOP where tenant_num_id = ? and data_sign = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign}, Integer.class);
    }

    public MDMS_O_CUSTOMER_SHOP selectMdmsOCustomerShop(Long tenantNumId, Long dataSign, Long series) {
        String sql = "select * from MDMS_O_CUSTOMER_SHOP where tenant_num_id = ? and data_sign = ? and series = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, new BeanPropertyRowMapper<MDMS_O_CUSTOMER_SHOP>(MDMS_O_CUSTOMER_SHOP.class));
    }

    public List<MDMS_B2B_SUB_RELATION> selectMdmsB2bSubRelationListPage(Long tenantNumId, Long dataSign, String saleOrg, String distChannel, String customerNumId, String effectSign, String subUnitName, int pageNum, int pageSize) {
        String condition = " where tenant_num_id = ? and data_sign = ? ";
        if (!StringUtils.isBlank(effectSign)) {
            condition += " and effect_sign = " + effectSign;
        }
        if (!StringUtils.isBlank(subUnitName)) {
            condition += " and sub_unit_name like '%" + subUnitName + "%' ";
        }
        String sql = String.format("select * from MDMS_B2B_SUB_RELATION %s limit ?,?", condition);

        return jdbcTemplate.query(sql, new Object[]{
                tenantNumId,
                dataSign,
                --pageNum * pageSize,
                pageSize
        }, new BeanPropertyRowMapper<MDMS_B2B_SUB_RELATION>(MDMS_B2B_SUB_RELATION.class));
    }

    public int selectMdmsB2bSubRelationListPageTotal(Long tenantNumId, Long dataSign, String saleOrg, String distChannel, String customerNumId, String effectSign, String subUnitName) {
        String condition = " where tenant_num_id = ? and data_sign = ? ";
        if (!StringUtils.isBlank(effectSign)) {
            condition += " and effect_sign = " + effectSign;
        }
        if (!StringUtils.isBlank(subUnitName)) {
            condition += " and sub_unit_name like '%" + subUnitName + "%' ";
        }
        String sql = String.format("select count(*) from MDMS_B2B_SUB_RELATION %s", condition);
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign}, Integer.class);
    }

    public MDMS_B2B_SUB_RELATION selectMdmsB2bSubRelation(Long tenantNumId, Long dataSign, Long series) {
        String sql = "select * from MDMS_B2B_SUB_RELATION where tenant_num_id = ? and data_sign = ? and series = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, new BeanPropertyRowMapper<MDMS_B2B_SUB_RELATION>(MDMS_B2B_SUB_RELATION.class));
    }

    public int insertMdmsB2bSubRelation(MDMS_B2B_SUB_RELATION entity) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into MDMS_B2B_SUB_RELATION");
        stringBuffer.append(" (");
        stringBuffer.append(EntityFieldUtil.fieldSplit(MDMS_B2B_SUB_RELATION.class, ","));
        stringBuffer.append(") values (");
        stringBuffer.append(EntityFieldUtil.wildcardSplit(MDMS_B2B_SUB_RELATION.class, ","));
        stringBuffer.append(")");
        return jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_B2B_SUB_RELATION.class, entity));
    }

    public int updateMdmsB2bSubRelation(MDMS_B2B_SUB_RELATION entity) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE MDMS_B2B_SUB_RELATION SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(entity, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND series = ?");
        List<Object> objects = new ArrayList(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_B2B_SUB_RELATION.class, entity)));
        objects.add(entity.getDATA_SIGN());
        objects.add(entity.getTENANT_NUM_ID());
        objects.add(entity.getSERIES());
        return jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
    }


}
