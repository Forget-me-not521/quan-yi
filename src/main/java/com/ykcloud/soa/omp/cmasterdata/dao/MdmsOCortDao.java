package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.utils.DaoUtil;
import com.ykcloud.soa.omp.cmasterdata.api.model.*;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CORT;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

@Repository
public class MdmsOCortDao {
    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_CORT.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_CORT.class, ",");
    private static final String TABLE_NAME = "MDMS_O_CORT";
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public boolean checkExistByCortId(String cortId, Long tenantNumId, Long dataSign) {
        return jdbcTemplate.queryForObject(
                "select count(*) from MDMS_O_CORT where tenant_num_id=? and data_sign=? and cort_id=? and cancelsign='N'",
                new Object[]{tenantNumId, dataSign, cortId}, Integer.class) > 0;
    }

    public boolean checkExistByCortNumId(Long cortNumId, Long tenantNumId, Long dataSign) {
        return jdbcTemplate.queryForObject(
                "select count(*) from MDMS_O_CORT where tenant_num_id=? and data_sign=? and cort_num_id=? and "
                        + "cancelsign='N'",
                new Object[]{tenantNumId, dataSign, cortNumId}, Integer.class) > 0;
    }

    public MDMS_O_CORT getMdmsOCort(String cortId, Long tenantNumId, Long dataSign) {
        String sql = "select * from MDMS_O_CORT where tenant_num_id=? and data_sign=? and cort_id=? and cancelsign='N'";
        MDMS_O_CORT cort = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, cortId},
                new BeanPropertyRowMapper<>(MDMS_O_CORT.class));
        return cort;
    }

    public List<MdmsOCort> getMdmsOCortList(Long tenantNumId, Long dataSign) {
        String sql = "select series, cort_num_id as cortNumId, cort_id as cortId, cort_name as cortName from MDMS_O_CORT where tenant_num_id = ? and data_sign = ? and cancelsign='N'";
        List<MdmsOCort> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign}, new BeanPropertyRowMapper<>(MdmsOCort.class));
        return list;
    }

    /***
     *
     * @param entity
     */
    public void insertEntity(MDMS_O_CORT entity) {
        entity.setSERIES(Long.valueOf(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_CORT_SERIES)));
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int row = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_CORT.class, entity));
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "插入MDMS_O_CORT表失败!");
        }
    }

    public void updateEntity(MDMS_O_CORT entity) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE MDMS_O_CORT SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(entity, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND CORT_NUM_ID = ?");
        List<Object> objects = new ArrayList(
                Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_O_CORT.class, entity)));
        objects.add(entity.getDATA_SIGN());
        objects.add(entity.getTENANT_NUM_ID());
        objects.add(entity.getCORT_NUM_ID());
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_O_CORT更新失败!");
        }
    }

    //根据核算简码查询核算公司
    public Long getCortNumIdByCortId(Long tenantNumId, Long dataSign, String cortId) {
        String sql
                = "select cort_num_id from mdms_o_cort where tenant_num_id=? and data_sign=? and cort_id=? and "
                + "cancelsign='N'";
        List<Long> cortNumIds = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, cortId}, Long.class);
        if (cortNumIds.isEmpty()) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据核算供应商简码查询核算编号失败!核算简码:" + cortId);
        }
        if (cortNumIds.size() > 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据核算供应商简码查询多笔核算供应编号!核算简码:" + cortId);
        }
        return cortNumIds.get(0);
    }

    public boolean checkExistCortNumId(Long tenantNumId, Long dataSign, Long cortNumId) {
        String sql = "select count(1) from MDMS_O_CORT where tenant_num_id=? and data_sign=? and cort_num_id=? and cancelsign='N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, cortNumId}, Integer.class) > 0;
    }

    public MDMS_O_CORT getEntity(Long tenantNumId, Long dataSign, Long unitNumId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("select ");
        stringBuilder.append(SQL_COLS);
        stringBuilder.append(" from ");
        stringBuilder.append(TABLE_NAME);
        stringBuilder.append(" where  tenant_num_id=? and data_sign=? and cort_num_id=?");
        MDMS_O_CORT mdms_o_cort = jdbcTemplate.queryForObject(stringBuilder.toString(),
                new Object[]{tenantNumId, dataSign, unitNumId},
                new BeanPropertyRowMapper<>(MDMS_O_CORT.class));
        if (mdms_o_cort == null) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据供应商的编号" + unitNumId + "找不到对应的父供应商信息");
        }
        return mdms_o_cort;
    }

    //查询银行信息
    public MdmsBilling getBilling(Long tenantNumId, Long dataSign, Long unitNumId) {
        String sql = "select   cort1.billing_bank, "
                + "  cort1.billing_bank_account  from  mdms_o_unit cort "
                + " LEFT JOIN mdms_o_cort cort1 ON cort1.data_sign = cort.data_sign"
                + "                                  AND cort1.tenant_num_id = cort.tenant_num_id"
                + "                                  AND cort1.cort_num_id = cort.cort_num_id"
                + " where cort.tenant_num_id=? and cort.data_sign=? and cort.unit_num_id=? and cort1.status_num_id=1";
        List<MdmsBilling> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, unitNumId}, new BeanPropertyRowMapper<>(MdmsBilling.class));
        if (list == null || list.size() == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据供应商的编号" + unitNumId + "找不到对应的银行信息");
        }
        if (list.size() > 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据供应商的编号" + unitNumId + "找到多条银行信息");
        }
//        MdmsBilling cort = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, unitNumId},
//                                                       new BeanPropertyRowMapper<>(MdmsBilling.class));
        return list.get(0);
    }

    public void updateEntity(Long tenantNumId, Long dataSign, Long cortNumId, MDMS_O_CORT mdmsOCort) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE MDMS_O_CORT SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(mdmsOCort, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND CORT_NUM_ID = ?");
        List<Object> objects = new ArrayList(
                Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_O_CORT.class, mdmsOCort)));
        objects.add(dataSign);
        objects.add(tenantNumId);
        objects.add(cortNumId);
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_O_CORT更新失败!");
        }

    }

    public int deleteRecordBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "delete from mdms_o_cort where tenant_num_id=? and data_sign=? and series=? and cancelsign='N'";
        return jdbcTemplate.update(sql, tenantNumId, dataSign, series);
    }

    public void insertEntityNoSeries(MDMS_O_CORT mdmsOCort) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int row = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_CORT.class, mdmsOCort));
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "插入MDMS_O_CORT表失败!");
        }
    }

    public void updateCortIdEntity(Long tenantNumId, Long dataSign, String cortId, MDMS_O_CORT mdmsOCort) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE MDMS_O_CORT SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(mdmsOCort, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND cort_id = ?");
        List<Object> objects = new ArrayList(
                Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_O_CORT.class, mdmsOCort)));
        objects.add(dataSign);
        objects.add(tenantNumId);
        objects.add(cortId);
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_O_CORT更新失败!");
        }

    }

    public void updateCortIdEntityByCortNumId(Long tenantNumId, Long dataSign, Long cortNumId, MDMS_O_CORT mdmsOCort) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE MDMS_O_CORT SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(mdmsOCort, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND cort_num_id = ?");
        List<Object> objects = new ArrayList(
                Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_O_CORT.class, mdmsOCort)));
        objects.add(dataSign);
        objects.add(tenantNumId);
        objects.add(cortNumId);
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_O_CORT更新失败!");
        }

    }



    public void insertMdmsOCort(MDMS_O_CORT mdms_o_cort){
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(TABLE_NAME);
        sb.append("(");
        sb.append(SQL_COLS);
        sb.append(") values (");
        sb.append(WILDCARDS);
        sb.append(")");
        int row = jdbcTemplate.update(sb.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_CORT.class, mdms_o_cort));
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35002, "插入失败！");
        }
    }

    public List<MdmsBlCort> getCortPage(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode, String cortNumId, String cortName, Long createUserId, Date createDateBegin, Date createDateEnd, Integer pageNum, Integer pageSize) {
        String sql = "select * from mdms_o_cort where cancelsign='N' and tenant_num_id =? and data_sign = ? ";
        if(StringUtils.isNotEmpty(unifiedSocialCreditCode)){
            sql+=" and unified_social_credit_code='"+unifiedSocialCreditCode+"' ";
        }
        if(StringUtils.isNotEmpty(cortNumId)){
            sql+=" and cort_num_id='"+cortNumId+"' ";
        }
        if(StringUtils.isNotEmpty(cortName)){
            sql+=" and cort_name='"+cortName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sql+=" and create_user_id='"+createUserId+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateBegin)){
            sql+=" and create_dtme>'"+createDateBegin+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateEnd)){
            sql+=" and create_dtme<'"+createDateEnd+"' ";
        }
        Integer start = (pageNum-1)*pageSize;

        sql+=" limit ?,? ";
        List<MdmsBlCort> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign,start,pageSize}, new BeanPropertyRowMapper<>(MdmsBlCort.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public Integer getCortPageCount(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode, String cortNumId, String cortName, Long createUserId, Date createDateBegin,Date createDateEnd) {
        String sql = "select count(1) from mdms_o_cort where cancelsign='N' and tenant_num_id =? and data_sign = ? ";
        if(StringUtils.isNotEmpty(unifiedSocialCreditCode)){
            sql+=" and unified_social_credit_code='"+unifiedSocialCreditCode+"' ";
        }
        if(StringUtils.isNotEmpty(cortNumId)){
            sql+=" and cort_num_id='"+cortNumId+"' ";
        }
        if(StringUtils.isNotEmpty(cortName)){
            sql+=" and cort_name='"+cortName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sql+=" and create_user_id='"+createUserId+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateBegin)){
            sql+=" and create_dtme>'"+createDateBegin+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateEnd)){
            sql+=" and create_dtme<'"+createDateEnd+"' ";
        }
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign}, Integer.class);
    }


    public List<CortDropDown> getCortDropDown(Long tenantNumId, Long dataSign,int registerType) {
        String sql = "select * from mdms_o_cort where cancelsign='N' and tenant_num_id =? and data_sign = ? ";
        if(registerType==0){
            sql+=" and type_num_id=3 ";
        }else{
            sql+=" and type_num_id!=1 ";
        }

        List<CortDropDown> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign}, new BeanPropertyRowMapper<>(CortDropDown.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public boolean checkCortExist(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode) {
        String sql = "select count(1) from mdms_o_cort where tenant_num_id = ? and data_sign = ? and  unified_social_credit_code = ? AND cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign,
                unifiedSocialCreditCode}, int.class) >0 ? true : false;
    }


    public List<CortInfo> getCortInfo(Long tenantNumId, Long dataSign, List<String> cortNumIds) {
        String sql = "SELECT cort_num_id,cort_name from mdms_o_cort where tenant_num_id =:tenantNumId AND data_sign =:dataSign AND cort_num_id in (:cortNumIds)  and  cancelsign = 'N' "
                + DaoUtil.LIMIT_STR;
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("tenantNumId", tenantNumId);
        parameters.addValue("dataSign", dataSign);
        parameters.addValue("cortNumIds", cortNumIds);
        List<CortInfo> modelList = namedParameterJdbcTemplate.query(sql,
                parameters, new BeanPropertyRowMapper<>(CortInfo.class));
        return modelList;
    }
}
