package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 主数据日志
 *
 * @author wangzhengyan
 */
@Data
public class MDM_OPERATE_LOG {
    private String SERIES;// 行号
    private Long TENANT_NUM_ID;// 租户ID
    private Long DATA_SIGN;// 0: 正式 1：测试
    private Long RESERVED_NO;// 单号
    private Integer BILL_TYPE;// 变更类型
    private String BILL_TYPE_NAME;// 变更类型名称
    private String OPERATE_CONTENT;// 操作内容
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date CREATE_DTME;// 创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date LAST_UPDTME;// 最后更新时间
    private Long CREATE_USER_ID;// 用户
    private Long LAST_UPDATE_USER_ID;// 更新用户
    private String CANCELSIGN = "";//删除
}
