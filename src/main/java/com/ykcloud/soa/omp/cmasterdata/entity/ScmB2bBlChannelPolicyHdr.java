package com.ykcloud.soa.omp.cmasterdata.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 *
 * @date 2021-10-21
 */
@Data
public class ScmB2bBlChannelPolicyHdr implements Serializable {
    private static final long serialVersionUID=1L;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Long series;

    /** 租户 */
    @ApiModelProperty(value = "租户")
    private Long tenant_num_id;

    /** 状态标识，0正式，1测试，3删除，4作废 */
    @ApiModelProperty(value = "状态标识，0正式，1测试，3删除，4作废")
    private Long data_sign;

    /** 公司 */
    @ApiModelProperty(value = "公司")
    private String cort_num_id;

    /** 业务单元 */
    @ApiModelProperty(value = "业务单元")
    private String unit_num_id;

    /** 组织 */
    @ApiModelProperty(value = "组织")
    private String org_unit;

    /** 渠道 */
    @ApiModelProperty(value = "渠道")
    private Byte channel_num_id;

    /** 单据号 */
    @ApiModelProperty(value = "单据号")
    private String reserved_no;

    /** 变更主主题 */
    @ApiModelProperty(value = "变更主主题")
    private String theme;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private Byte status_num_id;

    /** 类型 */
    @ApiModelProperty(value = "类型")
    private Byte type_num_id;

    /** 来源 */
    @ApiModelProperty(value = "来源")
    private Byte so_from_type;

    /** 制单日期 */
    @ApiModelProperty(value = "制单日期")
    private Date order_date;

    /** 生效日期 */
    @ApiModelProperty(value = "生效日期")
    private Date effect_date;

    private Long create_user_id;

    private Date create_dtme;

    private Long update_user_id;

    private Date update_dtme;

    /** 删除 */
    @ApiModelProperty(value = "删除")
    private String cancelsign;
}
