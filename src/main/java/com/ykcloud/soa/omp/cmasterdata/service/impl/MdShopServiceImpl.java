package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccommon.api.exception.BusinessException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.*;
import com.gb.soa.omp.cmessagecenter.util.ExceptionUtils;

import com.ykcloud.cache.spring.boot.starter.RedisService;
import com.ykcloud.soa.erp.common.enums.BlProcessTypeEnum;
import com.ykcloud.soa.erp.common.enums.UnitSubTypeEnum;
import com.ykcloud.soa.erp.common.enums.UnitTypeEnum;
import com.ykcloud.soa.omp.cmasterdata.api.model.BlSubUnit;
import com.ykcloud.soa.omp.cmasterdata.api.model.CustomerShopInfo;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsBlPartner;
import com.ykcloud.soa.omp.cmasterdata.api.model.ProcessHdr;
import com.ykcloud.soa.omp.cmasterdata.api.request.*;
import com.ykcloud.soa.omp.cmasterdata.api.response.*;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdShopService;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdTenantService;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdUnitService;
import com.ykcloud.soa.omp.cmasterdata.dao.*;
import com.ykcloud.soa.omp.cmasterdata.entity.*;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;

import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.bind.annotation.RestController;

@Service("mdShopService")
@RestController
public class MdShopServiceImpl implements MdShopService {
    private static Logger log = LoggerFactory.getLogger(MdShopServiceImpl.class);


    @Resource
    private MdmsOSubUnitDao mdmsOSubUnitDao;
    @Resource
    private MdmsOCustomerShopDao mdmsOCustomerShopDao;
    @Resource
    private MdmsBlCustomerShopDao mdmsBlCustomerShopDao;

    @Resource
    private MdmsBlSubUnitDao mdmsBlSubUnitDao;
    @Resource
    private MdmsBlProcessMasterHdrDao mdmsBlProcessMasterHdrDao;
    @Resource
    private RedisService redisService;
    @Resource(name = "masterDataTransactionManager")
    private PlatformTransactionManager masterDataTransactionManager;

    @Resource
    private MdUnitService mdUnitService;
    @Override
    public SubUnitSaveResponse saveSubUnit(SubUnitSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveSubUnit request:{}", JsonUtil.toJson(request));
        }
        SubUnitSaveResponse response = new SubUnitSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId(),
                    dataSign = request.getDataSign(),
                    userNumId = request.getUserNumId();
            ProcessHdr head=request.getHead();
            BlSubUnit detail=request.getDetail();

            if(BlProcessTypeEnum.SHOP_CREATE.getBillType()==head.getTypeNumId()){
                if(mdmsOSubUnitDao.checkSubUnitExist(tenantNumId,dataSign,detail.getSubUnitId(),head.getCortNumId())){
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"该门店已建档！门店编码：" + detail.getSubUnitId());
                }
            }else{
                if(!mdmsOSubUnitDao.checkSubUnitExist(tenantNumId,dataSign,detail.getSubUnitId(),head.getCortNumId())){
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"该门店不存在，请确认！门店编码：" + detail.getSubUnitId());
                }
            }
            MDMS_BL_PROCESS_MASTER_HDR headHdr = createProcessHdr(tenantNumId, dataSign, userNumId, head);
            String reservedNo=head.getReservedNo();
            if(ValidatorUtils.isNullOrZero(reservedNo)){
                reservedNo=SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_PROCESS_MASTER_HDR_RESERVED_NO);
                headHdr.setRESERVED_NO(reservedNo);
            }
            if(mdmsBlSubUnitDao.checkBlSubUnitExist(tenantNumId,dataSign,reservedNo,detail.getSubUnitId(),detail.getSeries())){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"同一个单据中门店编码不能重复！门店编码：" + detail.getSubUnitId());
            }

            MDMS_BL_SUB_UNIT subUnitEntity=createSubUnit(tenantNumId,dataSign,userNumId,reservedNo,head.getCortNumId(),detail);
            MDMS_BL_SUB_UNIT oldEntity=mdmsBlSubUnitDao.getOldBlSubUnit(tenantNumId,dataSign,reservedNo,detail.getSeries());
            MDMS_BL_PROCESS_MASTER_HDR oldHeadHdr=mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId,dataSign,reservedNo,head.getCortNumId());
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(300));
            try {
                if(!Objects.isNull(oldEntity)){
                    subUnitEntity.setSERIES(oldEntity.getSERIES());
                    subUnitEntity.setCREATE_DTME(oldEntity.getCREATE_DTME());
                    subUnitEntity.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                    mdmsBlSubUnitDao.update(subUnitEntity,tenantNumId,dataSign,subUnitEntity.getSERIES());
                }else{
                    subUnitEntity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_SUB_UNIT_SERIES));
                    mdmsBlSubUnitDao.insert(subUnitEntity);
                }
                if(!Objects.isNull(oldHeadHdr)){
                    headHdr.setSERIES(oldHeadHdr.getSERIES());
                    headHdr.setCREATE_DTME(oldHeadHdr.getCREATE_DTME());
                    headHdr.setCREATE_USER_ID(oldHeadHdr.getCREATE_USER_ID());
                    mdmsBlProcessMasterHdrDao.update(headHdr,tenantNumId,dataSign,headHdr.getSERIES());
                }else{
                    mdmsBlProcessMasterHdrDao.insert(headHdr);
                }
                masterDataTransactionManager.commit(txStatus);
            }catch (Exception e){
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
            response.setReservedNo(reservedNo);
        } catch (Exception e) {
            ExceptionUtils.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveSubUnit response:{}", JsonUtil.toJson(response));
        }
        return response;
    }
    @Override
    public BlSubUnitDetailQueryResponse queryBlSubUnitDetail(BlSubUnitDetailQueryRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin queryBlSubUnitDetail request:{}", JsonUtil.toJson(request));
        }
        BlSubUnitDetailQueryResponse response = new BlSubUnitDetailQueryResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15002);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            ProcessHdr hdr=mdmsBlProcessMasterHdrDao.getBlEntityByReservedNo(tenantNumId,dataSign,request.getReservedNo(),request.getCortNumId());
            if(Objects.isNull(hdr)){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据不存在，单号[%s]...", request.getReservedNo()));
            }
            BlSubUnit detail=mdmsBlSubUnitDao.getBlEntity(tenantNumId,dataSign,request.getReservedNo(),request.getCortNumId());
            if(Objects.isNull(detail)){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据明细不存在，单号[%s]...", request.getReservedNo()));
            }

            response.setDetail(detail);
            response.setHead(hdr);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end queryBlSubUnitDetail response:{}", JsonUtil.toJson(response));
        }
        return response;
    }
    @Override
    public SubUnitCreateAuditResponse auditSubUnitCreate(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin auditSubUnitCreate request:{}", JsonUtil.toJson(request));
        }
        SubUnitCreateAuditResponse response = new SubUnitCreateAuditResponse();
        RedisLock redisLock = null;
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId(),
                    dataSign = request.getDataSign(),
                    userNumId = request.getUserNumId();
            String redisLockKey = String.join("-", "ykcloud.md.bl.shop.audit",
                    request.getCortNumId(),request.getReservedNo());
            redisLock = redisService.getRedisLock(redisLockKey, 600);
            if (!redisLock.lock()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("门店操作审核中，单号[%s]...", request.getReservedNo()));
            }
            MDMS_BL_PROCESS_MASTER_HDR oldHeadHdr=mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId,dataSign,request.getReservedNo(),request.getCortNumId());
            if(Objects.isNull(oldHeadHdr)){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"该流程单头数据不存在，请确认！流程编码：" + request.getReservedNo()+",公司编码："+request.getCortNumId());
            }
            MDMS_BL_SUB_UNIT blEntity=mdmsBlSubUnitDao.getBlSubUnitEntity(tenantNumId,dataSign,request.getReservedNo());
            MDMS_O_SUB_UNIT oldEntity=mdmsOSubUnitDao.getSubUnitEntity(tenantNumId,dataSign,blEntity.getSUB_UNIT_ID(),request.getCortNumId());
            if(BlProcessTypeEnum.SHOP_CREATE.getBillType()==oldHeadHdr.getTYPE_NUM_ID()){
                if(!mdmsOSubUnitDao.checkSubUnitExist(tenantNumId,dataSign,blEntity.getSUB_UNIT_ID(),request.getCortNumId())){
                    MDMS_O_SUB_UNIT entity=new MDMS_O_SUB_UNIT();
                    BeanUtils.copyProperties(blEntity, entity);
                    entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUB_UNIT_SERIES));
                    entity.setCREATE_DTME(new Date());
                    entity.setCREATE_USER_ID(userNumId);
                    entity.setLAST_UPDATE_USER_ID(userNumId);
                    entity.setLAST_UPDTME(new Date());
                    UnitGenerateRequest unitReq=buildUnitReq(tenantNumId,dataSign,userNumId,request.getCortNumId(),entity.getSUB_UNIT_NAME(),entity.getSUB_UNIT_NUM_ID());
                    mdUnitService.generateUnit(unitReq);
                    mdmsOSubUnitDao.insertEntity(entity);
                }
            }else if(BlProcessTypeEnum.SHOP_UPDATE.getBillType()==oldHeadHdr.getTYPE_NUM_ID()){
                if(!Objects.isNull(oldEntity)){
                    MDMS_O_SUB_UNIT entity=new MDMS_O_SUB_UNIT();
                    BeanUtils.copyProperties(blEntity, entity);
                    entity.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                    entity.setCREATE_DTME(oldEntity.getCREATE_DTME());
                    entity.setSERIES(oldEntity.getSERIES());
                    entity.setLAST_UPDATE_USER_ID(userNumId);
                    entity.setLAST_UPDTME(new Date());
                    mdmsOSubUnitDao.updateEntity(tenantNumId,dataSign,request.getCortNumId(),oldEntity.getSUB_UNIT_NUM_ID(),entity);
                }
            }else if(BlProcessTypeEnum.CLOSE_SHOP.getBillType()==oldHeadHdr.getTYPE_NUM_ID()){
                if(!Objects.isNull(oldEntity)){
                    mdmsOSubUnitDao.updateEntity(tenantNumId,dataSign,userNumId,request.getCortNumId(),oldEntity.getSUB_UNIT_NUM_ID(),blEntity.getREMARK(),9);
                }
            }else if(BlProcessTypeEnum.MOVE_SHOP.getBillType()==oldHeadHdr.getTYPE_NUM_ID()){
                if(!Objects.isNull(oldEntity)){
                    mdmsOSubUnitDao.updateEntity(tenantNumId,dataSign,userNumId,request.getCortNumId(),oldEntity.getSUB_UNIT_NUM_ID(),blEntity.getREMARK(),8);
                }
            }
        } catch (Exception e) {
            ExceptionUtils.processException(e, response);
        }finally {
            redisService.unLock(redisLock);
        }
        if (log.isDebugEnabled()) {
            log.debug("end auditSubUnitCreate response:{}", JsonUtil.toJson(response));
        }
        return response;
    }


    @Override
    public SubUnitQueryResponse querySubUnit(SubUnitQueryRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin querySubUnit request:{}", JsonUtil.toJson(request));
        }
        SubUnitQueryResponse response = new SubUnitQueryResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15002);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            List<BlSubUnit> list=mdmsOSubUnitDao.getSubUnitList(tenantNumId,dataSign,request.getSubUnitNumId(),request.getSubUnitName(),request.getSimSubUnitName(),request.getCreateUserId(), request.getCreateDtmeBegin(), request.getCreateDtmeEnd(),request.getCortNumId(),request.getPageNum(),request.getPageSize());
            Integer count =mdmsOSubUnitDao.getSubUnitCount(tenantNumId,dataSign,request.getSubUnitNumId(),request.getSubUnitName(),request.getSimSubUnitName(),request.getCreateUserId(), request.getCreateDtmeBegin(), request.getCreateDtmeEnd(),request.getCortNumId());
            response.setCount(count);
            response.setResults(list);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end querySubUnit response:{}", JsonUtil.toJson(response));
        }
        return response;
    }



    @Override
    public CustomerShopSaveResponse saveCustomerShop(CustomerShopSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveCustomerShop request:{}", JsonUtil.toJson(request));
        }
        CustomerShopSaveResponse response = new CustomerShopSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId(),
                    dataSign = request.getDataSign(),
                    userNumId = request.getUserNumId();
            ProcessHdr head=request.getHead();
            CustomerShopInfo detail=request.getDetail();
            if(BlProcessTypeEnum.SHOP_OUTSIDE_CREATE.getBillType()==head.getTypeNumId()){
                if(mdmsOCustomerShopDao.checkCustomerShopExist(tenantNumId,dataSign,detail.getCustomerNumId(),head.getCortNumId(),detail.getUnifiedSocialCreditCode())){
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"该门店已建档！门店统一信用码：" + detail.getUnifiedSocialCreditCode());
                }
            }else{
                if(!mdmsOCustomerShopDao.checkCustomerShopExist(tenantNumId,dataSign,detail.getCustomerNumId(),head.getCortNumId(),detail.getUnifiedSocialCreditCode())){
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"该门店不存在，请确认！门店统一信用码：" + detail.getUnifiedSocialCreditCode());
                }
            }
            MDMS_BL_PROCESS_MASTER_HDR headHdr = createProcessHdr(tenantNumId, dataSign, userNumId, head);
            String reservedNo=head.getReservedNo();
            if(ValidatorUtils.isNullOrZero(reservedNo)){
                reservedNo=SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_PROCESS_MASTER_HDR_RESERVED_NO);
                headHdr.setRESERVED_NO(reservedNo);
            }
            if(mdmsBlCustomerShopDao.checkBlCustomerShopExist(tenantNumId,dataSign,reservedNo,detail.getUnifiedSocialCreditCode(),detail.getSeries())){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"同一个单据中门店统一信用码不能重复！门店统一信用码：" + detail.getUnifiedSocialCreditCode());
            }
            MDMS_BL_CUSTOMER_SHOP entity=this.createCustomerShop(tenantNumId,dataSign,userNumId,head.getCortNumId(),reservedNo, detail);
            MDMS_BL_CUSTOMER_SHOP oldEntity=mdmsBlCustomerShopDao.getOldCustomerShop(tenantNumId,dataSign,reservedNo,detail.getSeries());
            MDMS_BL_PROCESS_MASTER_HDR oldHeadHdr=mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId,dataSign,reservedNo,head.getCortNumId());
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(300));
            try {
                if(!Objects.isNull(oldEntity)){
                    entity.setSERIES(oldEntity.getSERIES());
                    entity.setCREATE_DTME(oldEntity.getCREATE_DTME());
                    entity.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                    mdmsBlCustomerShopDao.update(entity,tenantNumId,dataSign,entity.getSERIES());
                }else{
                    entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_CUSTOMER_SHOP_SERIES));
                    mdmsBlCustomerShopDao.insert(entity);
                }
                if(!Objects.isNull(oldHeadHdr)){
                    headHdr.setSERIES(oldHeadHdr.getSERIES());
                    headHdr.setCREATE_DTME(oldHeadHdr.getCREATE_DTME());
                    headHdr.setCREATE_USER_ID(oldHeadHdr.getCREATE_USER_ID());
                    mdmsBlProcessMasterHdrDao.update(headHdr,tenantNumId,dataSign,headHdr.getSERIES());
                }else{
                    mdmsBlProcessMasterHdrDao.insert(headHdr);
                }
                masterDataTransactionManager.commit(txStatus);
            }catch (Exception e){
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception e) {
            ExceptionUtils.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveCustomerShop response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public CustomerShopAuditResponse auditCustomerShop(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin auditCustomerShop request:{}", JsonUtil.toJson(request));
        }
        CustomerShopAuditResponse response = new CustomerShopAuditResponse();
        RedisLock redisLock = null;
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId(),
                    dataSign = request.getDataSign(),
                    userNumId = request.getUserNumId();
            String cortNumId=request.getCortNumId();
            String reservedNo=request.getReservedNo();
            String redisLockKey = String.join("-", "ykcloud.md.bl.customer.shop.audit", cortNumId,reservedNo);
            redisLock = redisService.getRedisLock(redisLockKey, 600);
            if (!redisLock.lock()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("客户门店审核中，单号[%s]...", reservedNo));
            }
            MDMS_BL_PROCESS_MASTER_HDR oldHeadHdr=mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId,dataSign,request.getReservedNo(),request.getCortNumId());
            if(Objects.isNull(oldHeadHdr)){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"该流程单头数据不存在，请确认！流程编码：" + request.getReservedNo()+",公司编码："+request.getCortNumId());
            }
            MDMS_BL_CUSTOMER_SHOP entity=mdmsBlCustomerShopDao.getCustomerShopByReservedNo(tenantNumId,dataSign,reservedNo,cortNumId);
            MDMS_O_CUSTOMER_SHOP oldEntity=mdmsOCustomerShopDao.getCustomerShopEntity(tenantNumId,dataSign,entity.getCUSTOMER_NUM_ID(),request.getCortNumId(),entity.getUNIFIED_SOCIAL_CREDIT_CODE());
            if(BlProcessTypeEnum.SHOP_OUTSIDE_CREATE.getBillType()==oldHeadHdr.getTYPE_NUM_ID()){
                if(!mdmsOCustomerShopDao.checkCustomerShopExist(tenantNumId,dataSign,entity.getCUSTOMER_NUM_ID(),request.getCortNumId(),entity.getUNIFIED_SOCIAL_CREDIT_CODE())){
                    MDMS_O_CUSTOMER_SHOP inEntity=new MDMS_O_CUSTOMER_SHOP();
                    BeanUtils.copyProperties(inEntity, entity);
                    inEntity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_CUSTOMER_SHOP_SERIES));
                    inEntity.setSHOP_NUM_ID(SeqUtil.getSeqAutoNextValue(tenantNumId,dataSign,SeqUtil.AUTO_MDMS_O_CUSTOMER_SHOP_NUM_ID));
                    inEntity.setCREATE_DTME(new Date());
                    inEntity.setCREATE_USER_ID(userNumId);
                    inEntity.setLAST_UPDATE_USER_ID(userNumId);
                    inEntity.setLAST_UPDTME(new Date());
                    mdmsOCustomerShopDao.insert(inEntity);
                }
            }else if(BlProcessTypeEnum.SHOP_OUTSIDE_UPDATE.getBillType()==oldHeadHdr.getTYPE_NUM_ID()){
                if(!Objects.isNull(oldEntity)){
                    MDMS_O_CUSTOMER_SHOP upEntity=new MDMS_O_CUSTOMER_SHOP();
                    BeanUtils.copyProperties(upEntity, entity);
                    upEntity.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                    upEntity.setCREATE_DTME(oldEntity.getCREATE_DTME());
                    upEntity.setSERIES(oldEntity.getSERIES());
                    upEntity.setLAST_UPDATE_USER_ID(userNumId);
                    upEntity.setLAST_UPDTME(new Date());
                    mdmsOCustomerShopDao.updateEntity(tenantNumId,dataSign,upEntity);
                }
            }
        } catch (Exception e) {
            ExceptionUtils.processException(e, response);
        }finally {
            redisService.unLock(redisLock);
        }
        if (log.isDebugEnabled()) {
            log.debug("end auditCustomerShop response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public CustomerShopQueryResponse queryCustomerShop(CustomerShopQueryRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin queryCustomerShop request:{}", JsonUtil.toJson(request));
        }
        CustomerShopQueryResponse response = new CustomerShopQueryResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId(),
                    dataSign = request.getDataSign();
            String cortNumId=request.getCortNumId();
            String customerNumId=request.getCustomerNumId();

            List<CustomerShopInfo> list=mdmsOCustomerShopDao.getCustomerShopList(tenantNumId,dataSign,cortNumId,customerNumId);
            if(Objects.isNull(list)){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"该客户门店数据不存在，请确认！公司编码：" + cortNumId+",客户编码："+customerNumId);
            }
            response.setList(list);
        } catch (Exception e) {
            ExceptionUtils.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end queryCustomerShop response:{}", JsonUtil.toJson(response));
        }
        return response;
    }


    private MDMS_BL_PROCESS_MASTER_HDR createProcessHdr(Long tenantNumId, Long dataSign, Long usrNumId, ProcessHdr head) {
        MDMS_BL_PROCESS_MASTER_HDR entity = new MDMS_BL_PROCESS_MASTER_HDR();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_PROCESS_MASTER_HDR_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        if(head.getSourceType()==1){
            entity.setBILL_STATUS(1);
        }
        entity.setDATA_SIGN(dataSign);
        entity.setCORT_NUM_ID(head.getCortNumId());
        entity.setPROCESS_TITLE(head.getProcessTitle());
        entity.setTYPE_NUM_ID(head.getTypeNumId());
        entity.setREMARK(head.getRemark());
        entity.setDEGREE_URGENCY(head.getDegreeUrgency());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        entity.setSOURCE_TYPE(head.getSourceType());
        return entity;
    }

    private MDMS_BL_SUB_UNIT createSubUnit(Long tenantNumId, Long dataSign, Long usrNumId, String reservedNo, String cortNumId,BlSubUnit detail) {
        MDMS_BL_SUB_UNIT entity = new MDMS_BL_SUB_UNIT();
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        entity.setRESERVED_NO(reservedNo);
        entity.setSUB_UNIT_NUM_ID(detail.getSubUnitId());
        entity.setSUB_UNIT_ID(detail.getSubUnitId());
        entity.setSUB_UNIT_NAME(detail.getSubUnitName());
        entity.setSIM_SUB_UNIT_NAME(detail.getSimSubUnitName());
        entity.setIS_B2C(detail.getIsB2c());
        entity.setIS_O2O(detail.getIsO2o());
        entity.setIS_OFFLINE(detail.getIsOffline());
        entity.setCORT_NUM_ID(cortNumId);
        entity.setDIVISION_NUM_ID(detail.getDivisionNumId());
        entity.setMANAGE_AREA(detail.getManageArea());
        entity.setSHOP_STAGE(detail.getShopStage());
        entity.setSHOP_CATEGORY(detail.getShopCategory());
        entity.setPRV_NUM_ID(detail.getPrvNumId());
        entity.setCITY_NUM_ID(detail.getCityNumId());
        entity.setCITY_AREA_NUM_ID(detail.getCityAreaNumId());
        entity.setTOWN_NUM_ID(detail.getTownNumId());
        entity.setMAPLOCATION_X(detail.getMaplocationX());
        entity.setMAPLOCATION_Y(detail.getMaplocationY());
        entity.setADR(detail.getAdr());
        entity.setSIGNING_DATE(detail.getSigningDate());
        entity.setBEGIN_DATE(detail.getBeginDate());
        entity.setBEGIN_YEAR(detail.getBeginYear());
        entity.setACQUISITION_DATE(detail.getAcquisitionDate());
        entity.setACQUISITION_YEAR(detail.getAcquisitionYear());
        entity.setORIGINAL_SUBJECT(detail.getOriginalSubject());
        entity.setCLOSE_DATE(detail.getCloseDate());
        entity.setCLOSE_YEAR(detail.getCloseYear());
        entity.setLAST_BUSINESS_DATE(detail.getLastBusinessDate());
        entity.setRENT_STOP_DATE(detail.getRentStopDate());
        entity.setLEASE_CONTRACT_AREA(detail.getLeaseContractArea());
        entity.setINSIDE_AREA(detail.getInsideArea());
        entity.setOPERATING_AREA(detail.getOperatingArea());
        entity.setSUBLEASE_AREA(detail.getSubleaseArea());
        entity.setIDLE_AREA(detail.getIdleArea());
        entity.setSTAFF_DORMITORY_AREA(detail.getStaffDormitoryArea());
        entity.setCONTRACT_SIGNING_DATE(detail.getContractSigningDate());
        entity.setRENT_FREE(detail.getRentFree());
        entity.setLEASE_START_DATE(detail.getLeaseStartDate());
        entity.setSTORE_HEAD_LEADER(detail.getStoreHeadLeader());
        entity.setLEASE_END_DATE(detail.getLeaseEndDate());
        entity.setRENTAL_EXPENSES(detail.getRentalExpenses());
        entity.setSUB_UNIT_NATURE(detail.getSubUnitNature());
        entity.setGROUP_SCALE_STORE_TYPE(detail.getGroupScaleStoreType());
        entity.setSUBSECTION_STORE_TYPE(detail.getSubsectionStoreType());
        entity.setMAIN_TRADE_STORE_TYPE(detail.getMainTradeStoreType());
        entity.setSUB_TRADE_STORE_TYPE(detail.getSubTradeStoreType());
        entity.setCOMPETITIVE_HIERARCHY(detail.getCompetitiveHierarchy());
        entity.setBUSINESS_LICENSE_TIME(detail.getBusinessLicenseTime());
        entity.setDRUG_BUSINESS_LICENSE_TIME(detail.getDrugBusinessLicenseTime());
        entity.setIS_MEDICAL_INSURANCE(detail.getIsMedicalInsurance());
        entity.setRETAIL_PHARMACY_CODE(detail.getRetailPharmacyCode());
        entity.setMEDICAL_INSURANCE_TYPE(detail.getMedicalInsuranceType());
        entity.setCARD_TYPE(detail.getCardType());
        entity.setMEDICAL_INSURANCE_FIRST_OPEN_TIME(detail.getMedicalInsuranceFirstOpenTime());
        entity.setMEDICAL_INSURANCE_CANCEL_TIME(detail.getMedicalInsuranceCancelTime());
        entity.setIS_DUAL_CHANNEL(detail.getIsDualChannel());
        entity.setIS_CHINESE_MEDICINE_APTITUDE(detail.getIsChineseMedicineAptitude());
        entity.setIS_COLD_CHAIN_APTITUDE(detail.getIsColdChainAptitude());
        entity.setSTORE_DISTRIBUTION_CYCLE(detail.getStoreDistributionCycle());
        entity.setSMALL_SCALE_START_MONTH(detail.getSmallScaleStartMonth());
        entity.setSMALL_SCALE_END_MONTH(detail.getSmallScaleEndMonth());
        entity.setTAX_ATTRIBUTE(detail.getTaxAttribute());
        entity.setIS_RELOCATION(detail.getIsRelocation());
        entity.setRELOCATION_TYPE(detail.getRelocationType());
        entity.setRELOCATION_DATE(detail.getRelocationDate());
        entity.setLAST_DECORATION_BUSINESS_TIME(detail.getLastDecorationBusinessTime());
        entity.setFIXED_NUMBER(detail.getFixedNumber());
        entity.setON_DUTY_PEOPLE_NUM(detail.getOnDutyPeopleNum());
        entity.setFIXED_TELEPHONE(detail.getFixedTelephone());
        entity.setSTORE_MANAGER(detail.getStoreManager());
        entity.setMOBILE_PHONE(detail.getMobilePhone());
        entity.setSTORE_ARRIVAL_VALIDITY(detail.getStoreArrivalValidity());
        entity.setOLD_SUB_UNIT_ID(detail.getOldSubUnitId());
        entity.setREPLENISHMENT_MODE(detail.getReplenishmentMode());
        entity.setSALES_GROUP(detail.getSalesGroup());
        entity.setSALE_CODE(detail.getSaleCode());
        entity.setREMARK(detail.getRemark());
        entity.setQUALITY_LEADER(detail.getQualityLeader());
        entity.setBUSINESS_LICENSE(detail.getBusinessLicense());
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(detail.getUnifiedSocialCreditCode());
        entity.setDRUG_BUSINESS_LICENSE(detail.getDrugBusinessLicense());
        entity.setDRUG_BUSINESS_SCOPE(detail.getDrugBusinessScope());
        entity.setMEDICAL_DEVICE_BUS_LINCENSE(detail.getMedicalDeviceBusLincense());
        entity.setMEDICAL_DEVICE_BUS_SCOPE(detail.getMedicalDeviceBusScope());
        entity.setMEDICAL_DEVICE_BUS_BEGIN(detail.getMedicalDeviceBusBegin());
        entity.setMEDICAL_DEVICE_BUS_END(detail.getMedicalDeviceBusEnd());
        entity.setMEDICAL_DEVICE_PRO_REC2(detail.getMedicalDeviceProRec2());
        entity.setMEDICAL_DEVICE_PRO_REC2_SCOPE(detail.getMedicalDeviceProRec2Scope());
        entity.setFOOD_BUS_LICENSE(detail.getFoodBusLicense());
        entity.setFOOD_BUS_LICENSE_SCOPE(detail.getFoodBusLicenseScope());
        entity.setFOOD_BUS_LICENSE_BEGIN(detail.getFoodBusLicenseBegin());
        entity.setFOOD_BUS_LICENSE_END(detail.getFoodBusLicenseEnd());
        entity.setSALE_LIMIT_DAYS(detail.getSaleLimitDays());
        entity.setSUB_UNIT_CLASSIFICATION(detail.getSubUnitClassification());
        entity.setSMALL_INVEST_BEGIN(detail.getSmallInvestBegin());
        entity.setSMALL_INVEST_END(detail.getSmallInvestEnd());
        return entity;
    }
    private MDMS_BL_CUSTOMER_SHOP createCustomerShop(Long tenantNumId, Long dataSign, Long usrNumId, String cortNumId,String reservedNo, CustomerShopInfo detail) {
        MDMS_BL_CUSTOMER_SHOP entity = new MDMS_BL_CUSTOMER_SHOP();
        entity.setRESERVED_NO(reservedNo);
        entity.setCORT_NUM_ID(cortNumId);
        entity.setCUSTOMER_NUM_ID(detail.getCustomerNumId());
        entity.setSHOP_NAME(detail.getShopName());
        entity.setSIM_SHOP_NAME(detail.getSimShopName());
        entity.setSHOP_NUM_ID(detail.getShopNumId());
        entity.setSHOP_CATEGORY(detail.getShopCategory());
        entity.setPRV_NUM_ID(detail.getPrvNumId());
        entity.setCITY_NUM_ID(detail.getCityNumId());
        entity.setCITY_AREA_NUM_ID(detail.getCityAreaNumId());
        entity.setTOWN_NUM_ID(detail.getTownNumId());
        entity.setMAPLOCATION_X(detail.getMaplocationX());
        entity.setMAPLOCATION_Y(detail.getMaplocationY());
        entity.setADR(detail.getAdr());
        entity.setSIGNING_DATE(detail.getSigningDate());
        entity.setIS_DUAL_CHANNEL(detail.getIsDualChannel());
        entity.setIS_CHINESE_MEDICINE_APTITUDE(detail.getIsChineseMedicineAptitude());
        entity.setIS_COLD_CHAIN_APTITUDE(detail.getIsColdChainAptitude());
        entity.setSTORE_DISTRIBUTION_CYCLE(detail.getStoreDistributionCycle());
        entity.setSALE_CODE(detail.getSaleCode());
        entity.setSTORE_HEAD_LEADER(detail.getStoreHeadLeader());
        entity.setQUALITY_LEADER(detail.getQualityLeader());
        entity.setBUSINESS_LICENSE(detail.getBusinessLicense());
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(detail.getUnifiedSocialCreditCode());
        entity.setDRUG_BUSINESS_LICENSE(detail.getDrugBusinessLicense());
        entity.setDRUG_BUSINESS_SCOPE(detail.getDrugBusinessScope());
        entity.setMEDICAL_DEVICE_BUS_LINCENSE(detail.getMedicalDeviceBusLincense());
        entity.setMEDICAL_DEVICE_BUS_SCOPE(detail.getMedicalDeviceBusScope());
        entity.setMEDICAL_DEVICE_BUS_BEGIN(detail.getMedicalDeviceBusBegin());
        entity.setMEDICAL_DEVICE_BUS_END(detail.getMedicalDeviceBusEnd());
        entity.setMEDICAL_DEVICE_PRO_REC2(detail.getMedicalDeviceProRec2());
        entity.setMEDICAL_DEVICE_PRO_REC2_SCOPE(detail.getMedicalDeviceProRec2Scope());
        entity.setFOOD_BUS_LICENSE(detail.getFoodBusLicense());
        entity.setFOOD_BUS_LICENSE_SCOPE(detail.getFoodBusLicenseScope());
        entity.setFOOD_BUS_LICENSE_BEGIN(detail.getFoodBusLicenseBegin());
        entity.setFOOD_BUS_LICENSE_END(detail.getFoodBusLicenseEnd());
        entity.setSALE_LIMIT_DAYS(detail.getSaleLimitDays());
        entity.setREMARK(detail.getRemark());
        entity.setSERIES(detail.getSeries());
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private UnitGenerateRequest buildUnitReq(Long tenantNumId, Long dataSign, Long usrNumId, String cortNumId,String subUnitName,String subUnitNumId){
        UnitGenerateRequest unitRequest = new UnitGenerateRequest();
        unitRequest.setCortNumId(cortNumId);
        unitRequest.setUnitName(subUnitName);
        unitRequest.setUnitType(UnitTypeEnum.UNIT_ORG.getTypeNumId());
        unitRequest.setSubType(UnitSubTypeEnum.SHOP.getSubType());
        unitRequest.setUnitNumId(subUnitNumId);
        unitRequest.setUserNumId(usrNumId);
        unitRequest.setTenantNumId(tenantNumId);
        unitRequest.setDataSign(dataSign);
        return unitRequest;
    }
}
