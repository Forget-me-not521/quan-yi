package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class CANAL_KAFKA_LISTENER_LOG {

    private String SERIES;
    private Long TENANT_NUM_ID;
    private Long DATA_SIGN;
    private Date CREATE_DTME;
    private Date LAST_UPDTME;
    private String topics;
    private String group_id;
    private String msg_content;
    private String request;
    private String response;
    private String error;
    private Long item_num_id;
    private Long sub_unit_num_id;
    private Integer msg_status;
    private Integer send_count;

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String sERIES) {
        SERIES = sERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long tENANT_NUM_ID) {
        TENANT_NUM_ID = tENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long dATA_SIGN) {
        DATA_SIGN = dATA_SIGN;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date cREATE_DTME) {
        CREATE_DTME = cREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date lAST_UPDTME) {
        LAST_UPDTME = lAST_UPDTME;
    }

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getMsg_content() {
        return msg_content;
    }

    public void setMsg_content(String msg_content) {
        this.msg_content = msg_content;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Long getItem_num_id() {
        return item_num_id;
    }

    public void setItem_num_id(Long item_num_id) {
        this.item_num_id = item_num_id;
    }

    public Long getSub_unit_num_id() {
        return sub_unit_num_id;
    }

    public void setSub_unit_num_id(Long sub_unit_num_id) {
        this.sub_unit_num_id = sub_unit_num_id;
    }

    public Integer getMsg_status() {
        return msg_status;
    }

    public void setMsg_status(Integer msg_status) {
        this.msg_status = msg_status;
    }

    public Integer getSend_count() {
        return send_count;
    }

    public void setSend_count(Integer send_count) {
        this.send_count = send_count;
    }
}
