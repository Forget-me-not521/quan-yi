package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.erp.common.exception.ErpExceptionType;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_LICENSE_ALLOW;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;


@Repository
public class MdmsOLicenseAllowDao extends Dao<MDMS_O_LICENSE_ALLOW> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public List<MDMS_O_LICENSE_ALLOW> getBySecondNumId(Long tenantNumId, Long dataSign, String secondNumId) {
        String sql = "select * from mdms_o_license_allow where tenant_num_id = ? and data_sign = ? " +
                " and second_num_id = ? and cancelsign='N'";
        List<MDMS_O_LICENSE_ALLOW> list = jdbcTemplate.query(sql,
                new Object[]{tenantNumId, dataSign, secondNumId}, new BeanPropertyRowMapper<>(MDMS_O_LICENSE_ALLOW.class));
        return list;
    }

    public void updateBySeries(Long tenantNumId, Long dataSign, List<String> seriesList) {
        String sql = "update mdms_o_license_allow set cancelsign = 'Y' where tenant_num_id = ? and data_sign = ?  " +
                "and cancelsign='N' and series in (" + StringUtils.join(seriesList, ",") + ")";
        int rows = jdbcTemplate.update(sql, tenantNumId, dataSign);
        if (rows != seriesList.size()) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ErpExceptionType.DOE33003,
                    "更新失败!");
        }
    }

}
