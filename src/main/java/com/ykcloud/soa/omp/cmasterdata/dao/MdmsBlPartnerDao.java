package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.utils.DaoUtil;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsBlPartner;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_PARTNER;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
//import com.ykcloud.soa.omp.cmasterdata.util.DaoUtil;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MdmsBlPartnerDao extends Dao<MDMS_BL_PARTNER> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_BL_PARTNER.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_BL_PARTNER.class, ",");
    private static final String TABLE_NAME = "MDMS_BL_PARTNER";

    public List<MDMS_BL_PARTNER> checkItemExist(Long tenantNumId, Long dataSign, String unifiedCreditCode, String reservedNo) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_partner ");
        sb.append(" where tenant_Num_id = ? and data_sign = ? and unified_credit_code = ? and reserved_no = ? and cancelsign = 'N'");
        List<MDMS_BL_PARTNER> mdmsBlPartnerList = jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, unifiedCreditCode, reservedNo}, new BeanPropertyRowMapper<>(MDMS_BL_PARTNER.class));
        return mdmsBlPartnerList;
    }

    public MDMS_BL_PARTNER selectByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_partner ");
        sb.append(" where tenant_Num_id = ? and data_sign = ? and reserved_no = ? and cancelsign = 'N'");
        List<MDMS_BL_PARTNER> list = jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(MDMS_BL_PARTNER.class));
        if(CollectionUtils.isEmpty(list)){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据详情不存在!");
        }
        if(list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据详情有多条!");
        }
        return list.get(0);
    }

    public void batchUpdateEntity(List<MDMS_BL_PARTNER> entityList) {
        String sql = "update mdms_bl_partner set " +
                "partner_id=?,partner_name=?,unified_credit_code=?,partner_classify=?,business_scope=?,contacts_tel=?,contacts=?," +
                "sale_allow=?,partner_adr=?,partner_type=?,legal_person=?,register_adr=?," +
                "register_date=?,register_capital=?,last_updtme=?,last_update_user_id=?" +
                " where tenant_num_id=? and data_sign =? and series=? ";
        List<Object[]> argsList =new ArrayList<Object[]>();
        for(MDMS_BL_PARTNER entity : entityList) {
            Object[] args = new Object[]{
                    entity.getPARTNER_ID(), entity.getPARTNER_NAME(), entity.getUNIFIED_CREDIT_CODE(), entity.getPARTNER_CLASSIFY(), entity.getBUSINESS_SCOPE(), entity.getCONTACTS_TEL(), entity.getCONTACTS(),
                    entity.getSALE_ALLOW(), entity.getPARTNER_ADR(), entity.getPARTNER_TYPE(), entity.getLEGAL_PERSON(), entity.getREGISTER_ADR(),
                    entity.getREGISTER_DATE(), entity.getREGISTER_CAPITAL(), entity.getLAST_UPDTME(), entity.getLAST_UPDATE_USER_ID(),
                    entity.getTENANT_NUM_ID(),entity.getDATA_SIGN(),entity.getSERIES()
            };
            argsList.add(args);
        }

        int[] rows = jdbcTemplate.batchUpdate(sql, argsList);
        int sum = DaoUtil.sum(rows);
        if (sum != argsList.size()) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                    "批量更新合作商详情失败!");
        }
    }

    public List<MdmsBlPartner> selectInfoByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_partner ");
        sb.append(" where tenant_Num_id = ? and data_sign = ? and reserved_no = ? and cancelsign = 'N'");
        List<MdmsBlPartner> list = jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(MdmsBlPartner.class));
        if(CollectionUtils.isEmpty(list)){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据详情不存在!");
        }
        if(list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据详情有多条!");
        }
        return list;
    }


    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }


}